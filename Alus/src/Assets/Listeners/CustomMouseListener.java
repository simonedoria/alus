package Assets.Listeners;

import java.awt.Canvas;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;

import javax.swing.JTextField;

import Alus.*;
import States.*;
import System.MemoryCard;

/**
 * Questa classe gestisce tutti gli eventi provenienti dal Mouse e dalla tastiera.<br>
 * Implementa {@code MouseListener}, {@code MouseMotionListener} e {@code KeyListener}.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */

public class CustomMouseListener implements MouseListener, MouseMotionListener, KeyListener {
	
	private JTextField textbox;
	
	public static int x, y;
	public static MouseEvent me, mc, cp;
	
	public CustomMouseListener(Frame frame, Canvas canvas, JTextField textbox) {
		this.textbox = textbox;
	}

	@Override
	public void mouseClicked(MouseEvent e) {}
	
	@Override
	public void mousePressed(MouseEvent e) {
		mc = e;
		if(State.getState() instanceof MenuState) {
			cp = e;
			if(!State.getState().isActiveCarica && !State.getState().isActiveCredits) {
				//DA CANCELLARE!!
				if(Assets.indietro.isDetected(e)) {
					Game.scenaFinale = new ScenaFinale();
					Game.menuState.setState(Game.scenaFinale);
					Game.menuState = null;
					Assets.mute();
					Assets.dialogoFinale.start();
				}
				//pulsante nuova partita
				if(Assets.newG.isDetected(e)) {
					Game.scenaIniziale = new ScenaIniziale();
					State.getState().setState(Game.scenaIniziale);
					Game.menuState = null;
					Assets.mute();
					Assets.mainTitle.start();
				}
				//pulsante load
				if(Assets.load.isDetected(e)) {
					if(!State.getState().isActiveCarica) {
						State.getState().isActiveCarica = true;
						State.getState().page = 0;
						State.fading = 220;
					}
				}
				//pulsante esci
				if(Assets.esci.isDetected(e)) {
					System.exit(0);
				} 
			} else {
				if(State.getState().isActiveCarica) {
					if(Assets.frecciaGiu.isDetected(e) && State.getState().isActiveDownArrow) {
						State.getState().page++;
					}
					if(Assets.frecciaSu.isDetected(e) && State.getState().isActiveUpArrow) {
						State.getState().page--;
					}
					//controllo sui pulsanti
					//pulsante 1
					if(Assets.list1.isDetected(e)) {
						if(State.getState().numToCont-State.getState().cont >= 1) {
							State.getState().numSalv = State.getState().cont;
							try {
								MemoryCard.carica(State.getState().listaSalvataggi.get(Game.menuState.numSalv));
							} catch (IOException er) {
								er.printStackTrace();
							}
							Game.gameState = new GameState(MemoryCard.getMap());
							State.getState().setState(Game.gameState);
							Game.display.panel.setVisible(true);
							State.getState().isActiveCarica = false;
							Game.menuState = null;
							Assets.mute();
							Assets.prigioneTheme.start();
						}
					}
					//pulsante 2
					else if(Assets.list2.isDetected(e)) {
						if(State.getState().numToCont-State.getState().cont >= 2) {
							State.getState().numSalv = State.getState().cont + 1;
							try {
								MemoryCard.carica(State.getState().listaSalvataggi.get(Game.menuState.numSalv));
							} catch (IOException er) {
								er.printStackTrace();
							}
							Game.gameState = new GameState(MemoryCard.getMap());
							State.getState().setState(Game.gameState);
							Game.display.panel.setVisible(true);
							State.getState().isActiveCarica = false;
							Game.menuState = null;
							Assets.mute();
							Assets.prigioneTheme.start();
						}
					}
					//pulsante 3
					else if(Assets.list3.isDetected(e)) {
						if(State.getState().numToCont-State.getState().cont >= 3) {
							State.getState().numSalv = State.getState().cont + 2;
							try {
								MemoryCard.carica(State.getState().listaSalvataggi.get(State.getState().numSalv));
							} catch (IOException er) {
								er.printStackTrace();
							}
							Game.gameState = new GameState(MemoryCard.getMap());
							State.getState().setState(Game.gameState);
							Game.display.panel.setVisible(true);
							State.getState().isActiveCarica = false;
							Game.menuState = null;
							Assets.mute();
							Assets.prigioneTheme.start();
						}
					}
					//pulsante 4
					else if(Assets.list4.isDetected(e)) {
						if(State.getState().numToCont-State.getState().cont >= 4) {
							State.getState().numSalv = State.getState().cont + 3;	
							try {
								MemoryCard.carica(State.getState().listaSalvataggi.get(State.getState().numSalv));
							} catch (IOException er) {
								er.printStackTrace();
							}
							Game.gameState = new GameState(MemoryCard.getMap());
							State.getState().setState(Game.gameState);
							Game.display.panel.setVisible(true);
							State.getState().isActiveCarica = false;
							Game.menuState = null;
							Assets.mute();
							Assets.prigioneTheme.start();
						}
					}
				}
				if(Assets.esciCarica.isDetected(e) && State.getState().isActiveCarica) {
					State.getState().isActiveCarica = false;
					State.fading = 0;
				}
			}
			if(!State.getState().isActiveCredits) {
				if(Assets.credits.isDetected(e)) {
					State.getState().isActiveCredits = true;
				}
			} else {
				if(Assets.indietro.isDetected(e)) {
					State.getState().isActiveCredits = false;
				}
			}
		} else if(State.getState() instanceof GameState || State.getState() instanceof BattleState) {
			//attivo inventario
			if(State.getState().isActive1) {
				if(Assets.iconaEsci.isDetected(e)) {
					Game.display.panel.setVisible(true);
					State.getState().isActive1 = false;
					State.fading = 0;
					State.getState().incrementoDesc = 0;
				}
			} else {
				if(Assets.iconaInv.isDetected(e) && !State.getState().isActive2) {
					Game.display.panel.setVisible(false);
					State.getState().isActive1 = true;
					State.getState().page = 0;
					State.getState().numPage = 0;
					State.fading = 220;
				}
			}
			if(State.getState().isActive2) {
				if(Assets.iconaEsci.isDetected(e)) {
					Game.display.panel.setVisible(true);
					State.getState().isActive2 = false;
					State.fading = 0;
					State.getState().incrementoDesc = 0;
				}
			} else {
				if(Assets.iconaAb.isDetected(e) && !State.getState().isActive1) {
					Game.display.panel.setVisible(false);
					State.getState().isActive2 = true;
					State.getState().page = 0;
					State.getState().numPage = 0;
					State.fading = 220;
				}
			}
		} 
		if(State.getState() instanceof GameState) {
			if(Assets.plus.isDetected(e) && !State.getState().isActive3 && !State.getState().isActive1 && !State.getState().isActive2) {
				State.getState().isActive3 = true;
			}
			if(State.getState().isActive3) {
				if(Assets.esciBoxStats.isDetected(e) && !State.getState().isActive1 && !State.getState().isActive2) {
					State.getState().isActive3 = false;
				}
			}
		}
		//controlli paginazione inventario oggetti
		if(State.getState().isActive1 || State.getState().isActive2) {
			if(Assets.frecciaDX.isDetected(e) && State.getState().isActiveRightArrow) {
				State.getState().page++;
			}
			if(Assets.frecciaSX.isDetected(e) && State.getState().isActiveLeftArrow) {
				State.getState().page--;
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseDragged(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {
		me = e;
		x = e.getX();
		y = e.getY();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		System.out.print(e.getKeyChar());
		if ( e.getKeyChar() == 'c' ) {
			textbox.setText("");
        }
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}
 }