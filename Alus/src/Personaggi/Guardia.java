package Personaggi;

import Abilita.Attacca;
import Abilita.AttaccoPoderoso;
import Stuff.*;
import System.RoutineSystem;

/**
 * 
 * Questa classe d' la specializzazione di una classe {@code Personaggio}.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga 
 *
 */

public class Guardia extends Personaggio {
	/**
	 * Setta i valori di base ed aggiunge oggetti di tipo {@code Stuff} all'inventario e oggetti di tipo {@code Abilita} alla lista apposita.
	 */
	public Guardia() {
		super("Guardia", 50, 50, 8, 8, 3, 2, 1, 2, 2, 0, 1, 0, 1, 0, 1, 4, 0, 1.5, 10, 5);
		addStuff(new PozioneCurativa());
		addAbilita(new Attacca());
		addAbilita(new AttaccoPoderoso());
	}
	/**
	 * Metodo che genera l'intelligenza artificiale della guardia.
	 */
	public void richiamaAbilita(Personaggio p2) {
		
		if(this.getPv()>(this.getPvMax()/3)) {
			if(RoutineSystem.prob(40)){
				if(!abilita.get(1).attiva(this,p2)){
					abilita.get(0).attiva(this, p2);
				}
				return;
			}
			else abilita.get(0).attiva(this, p2);     
			return;
			
		} else {
			if(RoutineSystem.prob(70)){
				if(!abilita.get(1).attiva(this,p2)) {
					abilita.get(0).attiva(this, p2);
				}
				return;
			} else abilita.get(0).attiva(this, p2);
			return;
		}
	}

}
