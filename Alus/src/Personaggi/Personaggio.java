package Personaggi;
import java.util.*;

import Abilita.Abilita;
import Status.*;
import Stuff.*;

/**
 * 
 * Questa classe contiene tutte le statistiche appartenenti al protagonista e ad ogni personaggio che lui incontri nel suo cammino.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga 
 *
 */

public class Personaggio {
	//attibuti: per comodità setto tutto a public così mi risparmio l'utilizzo di get e set (per ora)
	/**
	 * Questi sono gli attrinuti che caratterizzano il personaggio.<br>
	 * Ognuno di essi e' di tipo protected e sono di tipo {@code String}, {@code double}, {@code List} e {@code Stuff}.
	 */
	protected String nome;
	protected double pv;
	protected double pvMax;
	protected double pm;
	protected double pmMax;
	protected double forz;
	protected double bonusforz;
	protected double dif;
	protected double bonusdif;
	protected double des;
	protected double bonusdes;
	protected double amag;
	protected double bonusamag;
	protected double dmag;
	protected double bonusdmag;
	protected double fort;
	protected double crit;
	protected double bonuscrit;
	protected double dcrit;
	protected double exp;
	protected double gold;
	protected List<Status> statusInapplicabili;
	protected List<Abilita> abilita;
	protected List<Stuff> inventario; 
	protected List<Status> statusAttivi;
	protected Stuff arma=null, armaScudo=null, armatura=null, accessorio=null;
	//#########################################################################################
	public Personaggio(String nome, double pv, double pvMax, double pm, double pmMax, double forz, double bonusforz, double dif, double bonusdif, double des, double bonusdes, double amag, double bonusamag, double bonusdmag, double dmag, double fort, double crit, double bonuscrit, double dcrit, double exp, double gold) {
		this.nome=nome; this.pv=pv; this.pvMax=pvMax; this.pm=pm; this.pmMax=pmMax; this.forz=forz; this.bonusforz=bonusforz; this.dif=dif; this.des=des; this.bonusdes=bonusdes; this.bonusdif=bonusdif; this.amag=amag; this.bonusamag=bonusamag; this.dmag=dmag; this.bonusdmag=bonusdmag; this.fort=fort; this.crit=crit; this.bonuscrit=bonuscrit; this.dcrit=dcrit; this.exp=exp; this.gold=gold;
		inventario= new LinkedList<>();
		abilita=new ArrayList<>();
		statusAttivi=new LinkedList<>();
		statusInapplicabili=new ArrayList<>();
	}
	
	//METODI ACCESSORI
	/**
	 * @return L'arma equipaggiata nello slot destro.
	 */
	public Stuff getArma(){return arma;}
	/**
	 * Setta l'arma, slot destro.
	 */
	public void setArma(Stuff arma) {this.arma=arma;}
	/**
	 * @return L'arma o lo scudo equipaggiato nello slot sinistro.
	 */
	public Stuff getArmaScudo() {return armaScudo;}
	/**
	 * Setta l'arma o lo scudo, slot sinistro.
	 */
	public void setArmaScudo(Stuff as){armaScudo=as;}
	/**
	 * @return L'armatura equipaggiata.
	 */
	public Stuff getArmatura() {return armatura;}
	/**
	 * Setta l'armatura.
	 */
	public void setArmatura(Stuff arm) {armatura=arm;}
	/**
	 * @return L'accessorio equipaggiato.
	 */
	public Stuff getAccessorio() {return accessorio;}
	/**
	 * Setta l'accessorio.
	 */
	public void setAccessorio(Stuff acc) {accessorio=acc;}
	
	//###########################################################################################################
	/**
	 * @return Il nome del personaggio.
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Setta il nome del personaggio.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return I punti vita.
	 */
	public double getPv() {
		return pv;
	}
	/**
	 * Setta i pv del personaggio.
	 */
	public void setPv(double pv) {
		if(pv > this.pvMax) this.pv = this.pvMax;
		else if(pv<0) this.pv = 0;
		else this.pv = pv;
		
	}
	/**
	 * @return I punti vita massimi.
	 */
	public double getPvMax() {
		return pvMax;
	}
	/**
	 * Setta i pv max del personaggio.
	 */
	public void setPvMax(double pvMax) {
		this.pvMax = pvMax;
	}
	/**
	 * @return Il mana.
	 */
	public double getPm() {
		return pm;
	}
	/**
	 * Setta il mana del personaggio.
	 */
	public void setPm(double pm) {
		this.pm = pm;
		if(this.pm > this.pmMax)
		this.pm = this.pmMax;
	}
	/**
	 * @return Il mana massimo.
	 */
	public double getPmMax() {
		return pmMax;
	}
	/**
	 * Setta il mana massimo del personaggio.
	 */
	public void setPmMax(double pmMax) {
		this.pmMax = pmMax;
	}
	/**
	 * @return Il valore della forza.
	 */
	public double getForz() {
		return forz;
	}
	/**
	 * Setta la forza del personaggio.
	 */
	public void setForz(double forz) {
		this.forz = forz;
	}
	/**
	 * @return Il bonus della forza.
	 */
	public double getBonusforz() {
		return bonusforz;
	}
	/**
	 * Setta il bonus forza del personaggio.
	 */
	public void setBonusforz(double bonusforz) {
		this.bonusforz = bonusforz;
	}
	/**
	 * @return Il valore della difesa.
	 */
	public double getDif() {
		return dif;
	}
	/**
	 * Setta la difesa del personaggio.
	 */
	public void setDif(double dif) {
		this.dif = dif;
	}
	/**
	 * @return Il bonus della difesa.
	 */
	public double getBonusdif() {
		return bonusdif;
	}
	/**
	 * Setta il bonus difesa del personaggio.
	 */
	public void setBonusdif(double bonusdif) {
		this.bonusdif = bonusdif;
	}
	/**
	 * @return La destrezza.
	 */
	public double getDes() {
		return des;
	}
	/**
	 * Setta la destrzza del personaggio.
	 */
	public void setDes(double des) {
		this.des = des;
	}
	/**
	 * @return Il bonus della destrezza.
	 */
	public double getBonusdes() {
		return bonusdes;
	}
	/**
	 * Setta il bonus destrezza del personaggio.
	 */
	public void setBonusdes(double bonusdes) {
		this.bonusdes = bonusdes;
	}
	/**
	 * @return L'attacco magico.
	 */
	public double getAmag() {
		return amag;
	}
	/**
	 * Setta l'attacco magico del personaggio.
	 */
	public void setAmag(double amag) {
		this.amag = amag;
	}
	/**
	 * @return Il bonus attacco magico.
	 */
	public double getBonusamag() {
		return bonusamag;
	}
	/**
	 * Setta il bonus attacco magico del personaggio.
	 */
	public void setBonusamag(double bonusamag) {
		this.bonusamag = bonusamag;
	}
	/**
	 * @return L'attacco magico.
	 */
	public double getDmag() {
		return dmag;
	}
	/**
	 * Setta l'attacco magico 2 del personaggio.
	 */
	public void setDmag(double dmag) {
		this.dmag = dmag;
	}
	/**
	 * @return Il bonus attacco magico.
	 */
	public double getBonusdmag() {
		return bonusdmag;
	}
	/**
	 * Setta il bonus attacco magico 2 del personaggio.
	 */
	public void setBonusdmag(double bonusdmag) {
		this.bonusdmag = bonusdmag;
	}
	/**
	 * @return La fortuna del personaggio.
	 */
	public double getFort() {
		return fort;
	}
	/**
	 * Setta la fortuna del personaggio.
	 */
	public void setFort(double fort) {
		this.fort = fort;
	}
	/**
	 * @return Il critico del personaggio.
	 */
	public double getCrit() {
		return crit;
	}
	/**
	 * Setta il critico del personaggio.
	 */
	public void setCrit(double crit) {
		this.crit = crit;
	}
	/**
	 * @return Il bonus del critico.
	 */
	public double getBonuscrit() {
		return bonuscrit;
	}
	/**
	 * Setta il bonus critico del personaggio.
	 */
	public void setBonuscrit(double bonuscrit) {
		this.bonuscrit = bonuscrit;
	}
	/**
	 * @return Il critico del persionaggio.
	 */
	public double getDcrit() {
		return dcrit;
	}
	/**
	 * Setta il d critico del personaggio.
	 */
	public void setDcrit(double dcrit) {
		this.dcrit = dcrit;
	}
	/**
	 * @return I punti esperienza del personaggio.
	 */
	public double getExp() {
		return exp;
	}
	/**
	 * Setta i punti esperienza del personaggio.
	 */
	public void setExp(double exp) {
		this.exp = exp;
	}
	/**
	 * @return L'oro del personaggio.
	 */
	public double getGold() {
		return gold;
	}
	/**
	 * Setta l'oro del personaggio.
	 */
	public void setGold(double gold) {
		this.gold = gold;
	}
	
	//######################################################################################
	/**
	 * Aggiunge uno {@code Status} alla lista degli status del personaggio.<br>
	 * Verifica se fa parte della lista degli status inapplicabili, in tal caso non l'aggiunge.<br>
	 * Se viene aggiunto uno status in corso, viene resettato il timing del suddetto.
	 */
	public boolean addStatus(Status s){ 
		if(hasStatusInapplicabile(s)) return false; 
		Status status=hasStatus(s); 
 		if(status!=null){ 
 			status.resetTiming(); 
 			return true; 
 		} 
		statusAttivi.add(s); return true; 
	} 
	/**
	 * Rimuove lo {@code Status} dalla lista degli status attivi.
	 */
	public void removeStatus(Status s){ 
				statusAttivi.remove(s); 
	 	} 
	/**
	 * @return L'inventario di tutti gli {@code Stuff} senza distinzione.
	 */
	public List<Stuff> getInventario() {
		return inventario;
	}
	/**
	 * Setta L'inventario con un'altra lista di inventari.
	 */
	public void setInventario(List<Stuff> inventario) {
		this.inventario = inventario;
	}
	/**
	 * Aggiunge una abilita' alla lista personale delle {@code Abilita} imparate.
	 */
	public void addAbilita(Abilita a) {
		for(int i = 0; i < abilita.size(); i++) {
			if(abilita.get(i).getNome().equals(a.getNome())) {
				return;
			}
		}
		abilita.add(a);
	}
	/**
	 * Rimuove una {@code Abilita} dalla lista.
	 */
	public void removeAbilita(Abilita a){
		abilita.remove(a);
	}
	/**
	 * Aggiunge uno stuff all'inventario. Ne aumenta la quantita' solo se si tratta di uno stuffConsumabile,<br>
	 * nel caso in cui sia già presente e non è uno {@code StuffConsumabile} allora non fa nulla;
	 */
	public void addStuff(Stuff s){
		for(int i = 0; i < inventario.size(); i++) {
			//se si tratta di uno stuff consumabile già presente, ne aumenta le quantità
			if(inventario.get(i).getNome().equals(s.getNome()) && inventario.get(i) instanceof StuffConsumabile) {
				inventario.get(i).setQnt(inventario.get(i).getQnt() + 1);
				return;
			//se si tratta di uno stuff non consumabile già presente, non fa nulla
			} else if(inventario.get(i).getNome().equals(s.getNome()) && inventario.get(i) instanceof StuffNonConsumabile) {
				return;
			}
		}
		//se non si possiede allora inserisci come nuovo
		inventario.add(s);
	}
	/**
	 * Rimuove uno {@code Stuff} dalla lista inventario.
	 */
	public void removestuff(Stuff s){
		inventario.remove(s);
	}
	/**
	 * @return una {@code List} di {@code Status} di cui il suddetto personaggio non puo' subirne gli effetti.
	 */
	public List<Status> getStatusInapplicabili() {
		return statusInapplicabili;
	}
	/**
	 * Aggiunge uno {@code Status} alla lista di quelli inappicabili.
	 */
	public void addStatusInapplicabili(Status s) {
		statusInapplicabili.add(s);
	}
	/**
	 * Rimuove lo {@code Status} dalla lista di quelli inapplicabili. Se presente ovviamente.
	 */
	public void removeStatusInapplicabili(Status s) {
		statusInapplicabili.remove(s);
	}
	/**
	 * @return La lista di tutte le {@code Abilita} imparate.
	 */
	public List<Abilita> getAbilita() {
		return abilita;
	}
	/**
	 * Setta la lista delle {@code Abilita}.
	 */
	public void setAbilita(List<Abilita> abilita) {
		
		this.abilita = abilita;
	}
	/**
	 * @return La lista degli {@code Status} attivi.
	 */
	public List<Status> getStatusAttivi() {
		return statusAttivi;
	}
	/**
	 * Setta la {@code List} degli {@code Status} attivi.
	 */
	public void setStatusAttivi(List<Status> statusAttivi) {
		this.statusAttivi = statusAttivi;
	}
	
	//##############################################################################
	
	private Status hasStatus(Status status){
		String s=status.getNome();
		for(Status sts : statusAttivi)
			if(s.equals(sts.getNome())) return sts;
		return null;
	}
	
	private boolean hasStatusInapplicabile(Status status){
		String s=status.getNome();
		for(Status sts : statusInapplicabili)
			if(s.equals(sts.getNome())) return true;
		return false;
	}
	
	/**
	 * @return La stringa con tutti i valori del personaggio che poi dovro' analizzare per il salvataggio del file.
	 * @see MemoryCard
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.nome);
		sb.append(" ");
		sb.append(this.pv);
		sb.append(" ");
		sb.append(this.pvMax);
		sb.append(" ");
		sb.append(this.pm);
		sb.append(" ");
		sb.append(this.pmMax);
		sb.append(" ");
		sb.append(this.forz);
		sb.append(" ");
		sb.append(this.bonusforz);
		sb.append(" ");
		sb.append(this.dif);
		sb.append(" ");
		sb.append(this.bonusdif);
		sb.append(" ");
		sb.append(this.des);
		sb.append(" ");
		sb.append(this.bonusdes);
		sb.append(" ");
		sb.append(this.amag);
		sb.append(" ");
		sb.append(this.bonusamag);
		sb.append(" ");
		sb.append(this.dmag);
		sb.append(" ");
		sb.append(this.bonusdmag);
		sb.append(" ");
		sb.append(this.fort);
		sb.append(" ");
		sb.append(this.crit);
		sb.append(" ");
		sb.append(this.bonuscrit);
		sb.append(" ");
		sb.append(this.dcrit);
		sb.append(" ");
		sb.append(this.exp);
		sb.append(" ");
		sb.append(this.gold);
		sb.append("|");
		if(abilita.size()!=0) {
			for(int i = 0; i<abilita.size(); i++) {
				sb.append(abilita.get(i).getSlug());
				if(i!=abilita.size()-1) sb.append(" ");
			}
		}
		sb.append("|");
		if(inventario.size()!=0) {
			for(int i = 0; i<inventario.size(); i++) {
				sb.append(inventario.get(i).getSlug());
				if(i!=inventario.size()-1) sb.append(" ");
			}
		}
		sb.append("|");
		if(statusInapplicabili.size()!=0) {
			for(int i = 0; i<statusInapplicabili.size(); i++) {
				sb.append(statusInapplicabili.get(i).getSlug());
				if(i!=statusInapplicabili.size()-1) sb.append(" ");
			}
		}
		sb.append("|");
		if(arma!=null) {
			sb.append(arma.getSlug());
		}
		sb.append("|");
		if(armaScudo!=null) {
			sb.append(armaScudo.getSlug());
		}
		sb.append("|");
		if(armatura!=null) {
			sb.append(armatura.getSlug());
		}
		sb.append("|");
		if(accessorio!=null) {
			sb.append(accessorio.getSlug());
		}
		return sb.toString();
	}
	
	
}