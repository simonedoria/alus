package Dialoghi;
/**
 * Classe astratta nella quale ci sono i metodi per muoversi all'interno dei dialoghi.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public abstract class MappaDialoghi {
	
	/**
	 * Dichiaro un array di {@code String} quadrimensionale.
	 */
	public static String[][][][] mp = new String[50][50][50][3];
	/**
	 * Setto le risposte dei dialoghi.
	 */
	public abstract void setRisposteDialoghi();
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @return Passandogli l'evento, lo statement, e la risposta da prendere, lui mi restituisce cio' che viene detto dopo in ordine cronologico.
	 */
	public String getRisposta(int x, int y, int z) {
		return mp[x][y][z][0];
	}
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @return Restituisce l'evento successivo.
	 */
	public int getNextEvent(int x, int y, int z) {
		return Integer.parseInt(mp[x][y][z][1]);
	}
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @return Restituisce lo statement successivo.
	 */
	public int getNextStatement(int x, int y, int z) {
		return Integer.parseInt(mp[x][y][z][2]);
	}
	/**
	 * @param x
	 * @param y
	 * @return Restituisce il numero di possibili risposte che può avere un evento di tipo dialogo.
	 */
	public int numRisposte(int x, int y) {
		int cont = 0;
		for(int i = 0; i<mp[x][y].length; i++) {
			if(!mp[x][y][i][0].equals("")) {
				cont++;
			}
		}
		return cont;
	}
	protected void inizializzaArray() {
		for(int i = 0; i<50; i++)
			for(int j = 0; j<50; j++)
				for(int k = 0; k<50; k++)
					for(int z = 0; z<3; z++)
						mp[i][j][k][z] = "";
	}
}
