package Dialoghi;
/**
 * In questa classe si raccolgono tutte le {@code String} di tipo narrazione.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 * @see Shax
 *
 */
public class Narrazione {
	public static String[] n0 = {"Prigione di Tyrell","E' quasi sera, e tu, Carl, sei stato appena arrestato per omicidio.","L'unica cosa che sei riuscito a portarti, noscondendolo dentro una scarpa, e' un cristallo che tu conosci molto bene.","Vieni portato di peso nella tua cella e scaraventato a terra; dietro di te, senti la porta in ferro chiudersi pesantemente.\nLe voci delle guardie svaniscono pian piano allontanandosi.","L'unica cosa davanti a te e' il pavimento.\nIl tuo pensiero, per un momento, ti riporta al passato: il perche' sei qui, quali sono stati i tuoi errori, e le ingiustizie che hai subito...","Finche' una voce non rompe il silenzio nella cella...", Shax.shax0};
	//###########################################################################################################################################
	public static String[] n1 = {"Ti guardi intorno...\nLa cella, e' molto spoglia, c'e' solo il minimo indispensabile: un tavolino, due letti... se cosi' si possono definire, un secchio e ovviamente Shax.","A nord si trova la porta della cella, a ovest una parete con evidenti tracce d'umidita'."};
	//###########################################################################################################################################
	public static String n2 = "E' un secchio normalissimo, serve a...insomma...";
	public static String n3 = "Non proprio la cosa piu' comoda del mondo...";
	public static String n4 = "Cade a pezzi...il legno e' marcio.";
	public static String n5 = "La parete a Ovest ha evidenti tracce d'umidita'.";
	public static String n6 = "A nord si trova la porta della cella; e' chiusa e la serratura sembra essere molto solida. Da qui pero', riesci a scorgere nuovi particolari: a Ovest, proprio accanto della tua cella, c'e' un'altra stanza, nella quale sembrano esserci dei prigionieri. Inoltre, sembri trovarti alla fine di un lungo corridoio, con svariate stanze a destra e a sinistra.";
	public static String n7 = "Il corridoio a Nord, si perde a vista d'occhio, probabilmente a causa della poca luce, difatti e' quasi buio; anche se in fondo, scorgi le torcie delle guardie che pattugliano l'area.";
	public static String n8 = "Oltre la parete a Ovest ci deve essere un'altra cella.\nOsservando bene noti un mattone leggermente smosso.";
	public static String n9 = "Appena poggi la testa su quello che dovrebbe essere il cuscino, noti uno scarafaggio passarti sulla pancia.\n\n\nA) (Ti alzi disgustato)\nB) (Fai finta di niente)";
	public static String n10 = "Ti stendi sul letto.\nNon e' affatto comodo.";
	//###########################################################################################################################################
	public static String[] n11 = {"Appena Shax finisce di parlare si avvicina alla parete a Ovest; toglie un mattone dal muro e lo poggia vicino al tavolino. Poi dice sarcastico...","Shax: \"O no Gyll? Non vorresti evadere pure tu? Ahahah!\"","Pochi secondi dopo, da quell'apertura nella parete improvvisamente appare un occhio, sbarrato; l'uomo dall'altra parte comincia a parlare.","Gyll: \"Si'! Si'! Evadere evadere evadere! Ma come? Come?\nGyll non lo sa, e tu? Tu lo sai?\"","Shax: \"Vedi? Anche Gyll vuole uscire di qua', ma lui non puo', ha bisogno d'aiuto, tutti abbiamo bisogno d'aiuto; povero Gyll...\"","A) E adesso lui cosa c'entra?\nB) Siete tutti pazzi.\nC) Quindi non mi vuoi aiutare?"};
	//###########################################################################################################################################
	public static String[] n12 = {"Come detto da Shax ti stendi a letto.","Dopo circa un'ora, la prigione e' ormai nel buio piu' totale, solo le torcie delle gardie illuminano i corridoi di tanto in tanto.","Fino a che Shax non si avvicina alla porta a Nord.","Shax: \"Guardia! Guardia! Dov'e' la nostra acqua? Qui abbiamo sete!\"","Dopo qualche secondo una guardia si fa avanti e allungando la mano attraverso le sbarre di ferro, porge a Shax una borraccia d'acqua.","Guardia: \"Ecco, e adesso smettila di rompere...\"","Improvvisamente Shax afferra la mano della guardia ed estrae una Daga dai suoi vestiti; la pugnala freddamente piu' e piu' volte fino a quando non cade a terra. Poi si china a prendere le chiavi e apre la cella.","Shax si gira verso di te.\nShax: \"Visto non era cosi' difficile. Ahahah!\nAdesso faresti meglio a prendere i suoi vestiti se vuoi sperare di non essere beccato subito.\""};
	//###########################################################################################################################################
	public static String n13 = "Hai trovato un'armatura, una spada corta e una pozione curativa!\n\n\n(Suggerimento: equipaggia gli oggetti trovati con il comando \"equipaggia\".\nEs. \"equipaggia Armatura Da Guardia Carceraria\")";
	public static String n14 = "Cominci a camminare fuori dalla cella.\n\n\nA) (Continua)";
	public static String n15 = "Sarebbe meglio che tu indossassi l'armatura";
	//#############################################################################################################################################
	public static String[] n16 = {Shax.shax6,"Prosegui per il corridoio della prigione.\nTutto sembra essere calmo, non si vede nessuna guardia.","Arrivi fino ad un incrocio. Qui vedi uno sgabello e una torcia; sembra un posto di guardia.","Ad un certo punto senti dei passi molto veloci a Est, inoltre e' inconfondibile il suono del metallo che sbatte sul metallo.","E' una guardia che correndo si sta avvicinando verso di te!\n\n\nA) Sguaino l'arma e l'attacco\nB) Mantengo la calma"}; 
	//#############################################################################################################################################
	public static String[] n17 = {"La guardia cade a terra.", "Sei riuscito a ucciderla e nessuno sembra averti notato. Ti avvi verso Est, da dove la guardia era arrivata.","Davanti a te si apre un arco che da' all'esterno. Prosegui.","Una volta uscito ti trovi su quello che sembra essere una terrazza; e' buio e l'unica cosa visibile e' la piazza centrale della prigione che si trova proprio sotto di te a Est.","Cio' che vedi ti lascia sconcertato...","I corpi delle guardie sono accatastati l'uno sull'altro, la piazza e' attraversata da fiumi di sangue.","Per quanto questo evento sia traumatizzante decidi di sfruttarlo per scappare.", " "};
}





