package Dialoghi;
/**
 * In questa classe si raccolgono tutte le {@code String} provenienti dal Shax.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class Shax {
	
	public static String shax0 = "?: \"Guarda chi c'e'...\nUn nuovo arrivato... l'ultimo compagno di cella che ho avuto e' stato appena giustiziato.\"\n\n\nA) Chi sei?\nB) ...";
	public static String shax1 = "Shax sembra essere un individuo molto subdolo e probabilemente poco raccomandabile.\nHa i capelli neri e occhi chiarissimi, la cosa che ti e' rimasta impressa di lui e' il suo ghigno.";
	public static String shax2 = "Shax: \"Dimmi, cosa vuoi?\"\n\n\nA) ...\nB) Da cosa?";
	public static String shax3 = "Shax: \"...\"\n\n\nA) Chi sei? Come hai fatto finire qui?\nB) Chi era il tuo compagno di cella?\nC) Hai mai provato ad evadere?\nD) Uhm no, niente...";
	public static String shax4 = "Shax: \"Allora? Qualche novita'?\n\n\nA) Non ho niente di valore.\nB) Ho solo questo. (Dai Cristallo)";
	public static String shax5 = "Shax: \"Allora siamo pronti?\"\n\n\nA) Si'\nB) Aspetta un secondo";
	public static String shax6 = "Shax: \"Ora le nostre strade si dividono, vedi di non farti ammazzare! Ahahah!"; 
}
