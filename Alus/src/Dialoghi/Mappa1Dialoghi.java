package Dialoghi;
/**
 * La raccolta di tutti i dialoghi della Mappa1
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class Mappa1Dialoghi extends MappaDialoghi {
	
	public void setRisposteDialoghi() {
		inizializzaArray();
		//evento0 sentenza0 risposte A B C + prossimo event + prossimo statement
		mp[0][0][0][0] = "Shax: \"Il mio nome e' Shax\".\n\n\nA) Il mio nome e' Carl.\nB) ...\nC) Cosa hai fatto per finire qui?";
		mp[0][0][0][1] = "0";
		mp[0][0][0][2] = "1";
		mp[0][0][1][0] = "Shax: \"Ah, facciamo i misteriosi.\n\nIl mio nome e' Shax comunque...\nE in effetti, il tuo nome non e' importante.\n\nSara' interessante vedere quanto resisterai senza impazzire.\nAhahah!\"\n\nA) (Continua)";
		mp[0][0][1][1] = "0";
		mp[0][0][1][2] = "2";
		//evento0 sentenza1 risposte A B
		mp[0][1][0][0] = "Shax: \"Il tuo nome non e' importante.\n\nSara' interessante vedere quanto resisterai senza impazzire.\nAhahah!\"\n\nA) (Continua)";
		mp[0][1][0][1] = "0";
		mp[0][1][0][2] = "2";
		mp[0][1][1][0] = "Shax: \"Ah, facciamo i misteriosi.\nVa bene, tanto non mi interessa quale sia il tuo nome.\n\nSara' interessante vedere quanto resisterai senza impazzire.\nAhahah!\"\n\nA) (Continua)";
		mp[0][1][1][1] = "0";
		mp[0][1][1][2] = "2";
		mp[0][1][2][0] = "Shax: \"Uhm. Cio' non ha importanza.\n\nTutti siamo finiti qui per i piu' disparati motivi, alcuni di noi anche ingiustamente...\n\nMa cio' non interessa a nessuno\".\n\n\nA) (Continua)";
		mp[0][1][2][1] = "0";
		mp[0][1][2][2] = "2";
		//evento1 sentenza0 risposte A B C D
		mp[0][2][0][0] = "Hai guadagnato 10 exp!\n\n\nA) (Continua)";
		mp[0][2][0][1] = "0";
		mp[0][2][0][2] = "3";
		//
		mp[0][3][0][0] = "";
		mp[0][3][0][1] = "0";
		mp[0][3][0][2] = "0";
	//########################################################################################################################
		//evento1
		mp[1][0][0][0] = "Shax: \"Dalla vita intendo...\"\n\n\nA) ...\nB)Che vuoi?";
		mp[1][0][0][1] = "1";
		mp[1][0][0][2] = "1";
		mp[1][0][1][0] = "Shax: \"Dalla vita.\"\n\n\nA) (Continua)";
		mp[1][0][1][1] = "1";
		mp[1][0][1][2] = "2";
		//
		mp[1][1][0][0] = "Shax: \"Cos'e' facciamo i misteriosi? O il gatto ti ha morso la lingua?\nAhahah!\"\n\n\nA) ...\nB) Che vuoi?";
		mp[1][1][0][1] = "1";
		mp[1][1][0][2] = "3";
		mp[1][1][1][0] = "Shax: \"Perche' sei qui?\nNon sai quante ne ho sentite da quando sono qui.\nUna storia nuova forse potra' farmi fuggire dalla noia.\"\n\n\nA) Non sono affari tuoi.\nB) (Di' la verita')\nC) (Menti)";
		mp[1][1][1][1] = "1";
		mp[1][1][1][2] = "4";
		//
		mp[1][2][0][0]= "Shax: \"Perche' sei qui?\nNon sai quante ne ho sentite da quando sono qui.\nUna storia nuova forse potra' farmi fuggire dalla noia.\"\n\n\nA) Non sono affari tuoi.\nB) (Di' la verita')\nC) (Menti)";
		mp[1][2][0][1]= "1";
		mp[1][2][0][2]= "4";
		//
		mp[1][3][0][0]= "Shax: \"Uhm...\"\n\n\nA) (Continua)";
		mp[1][3][0][1]= "1";
		mp[1][3][0][2]= "12";
		mp[1][3][1][0]= "Shax: \"Perche' sei qui?\nNon sai quante ne ho sentite da quando sono qui.\nUna storia nuova forse potra' farmi fuggire dalla noia.\"\n\n\nA) Non sono affari tuoi.\nB) (Di' la verita')\nC) (Menti)";
		mp[1][3][1][1]= "1";
		mp[1][3][1][2]= "4";
		//
		mp[1][4][0][0]= "Shax: \"Capisco...ma non preoccuparti, non c'e' nessun problema, abbiamo tutto il tempo del mondo.\"\n\n\nA)...\nB) (Di'la verita')\nC) (Menti)";
		mp[1][4][0][1]= "1";
		mp[1][4][0][2]= "5";
		mp[1][4][1][0]= "Tu: \"...Qualche anno fa avevo una vita tranquilla, abitavo alla pacifica cittadella di Foren.\n Avevo una moglie, amici, una famiglia...\"\n\n\nA) (Continua)";
		mp[1][4][1][1]= "1";
		mp[1][4][1][2]= "6";
		mp[1][4][2][0]= "Tu: \"...Io sono innocente, vedrai tra poche ore massimo un giorno mi scarcereranno...\"\n\n\nA) (Continua)";
		mp[1][4][2][1]= "1";
		mp[1][4][2][2]= "7";
		//
		mp[1][5][0][0]= "Shax: \"Ah...certo che sei un tipo noioso...\"\n\n\nA) (Continua)";
		mp[1][5][0][1]= "1";
		mp[1][5][0][2]= "12";
		mp[1][5][1][0]= "Tu: \"...Qualche anno fa avevo una vita tranquilla, abitavo nella pacifica cittdella di Foren.\n Avevo una moglie, amici, una famiglia...\"\n\n\nA) (Continua)";
		mp[1][5][1][1]= "1";
		mp[1][5][1][2]= "6";
		mp[1][5][2][0]= "Tu: \"...Io sono innocente, vedrai tra poche ore massimo un giorno mi scarcereranno...\"\n\n\nA) (Continua)";
		mp[1][5][2][1]= "1";
		mp[1][5][2][2]= "7";
		//
		mp[1][6][0][0]= "Shax: \"Oh, che quadretto commovente...e poi?\"\n\n\nA) (Continua)";
		mp[1][6][0][1]= "1";
		mp[1][6][0][2]= "8";
		//
		mp[1][7][0][0]= "Shax: \"Sicuramente amico, anche il mio vecchio compagno di cella, Lyr, era innocente, chi qua' dentro non e' innocente?\"\n\n\nA) (Continua)";
		mp[1][7][0][1]= "1";
		mp[1][7][0][2]= "12";
		//
		mp[1][8][0][0]= "Tu: \"Dopo aver perso mia moglie, non ci ho visto piu'...\nMi sono dato all'alcool e sono diventato molto violento.\"\n\n\nA) (Continua)";
		mp[1][8][0][1]= "1";
		mp[1][8][0][2]= "9";
		//
		mp[1][9][0][0]= "Shax: \"Ah certo!\nOmicidio a sangue freddo!\"\n\n\nA) (Continua)";
		mp[1][9][0][1]= "1";
		mp[1][9][0][2]= "10";
		//
		mp[1][10][0][0]= "Tu: \"...Io non volevo...\nPoi mi hanno portato qui\"\n\n\nA) (Continua)";
		mp[1][10][0][1]= "1";
		mp[1][10][0][2]= "11";
		//
		mp[1][11][0][0]= "Shax: \"Una delle prigioni piu' amabili di tutta Tyrell...\nAhahah!\"\n\n\nA) (Continua)";
		mp[1][11][0][1]= "1";
		mp[1][11][0][2]= "12";
		//
		mp[1][12][0][0]= "";
		mp[1][12][0][1]= "0";
		mp[1][12][0][2]= "0";
		
		//###############################################################################################################################################################
		mp[2][0][0][0]= "Ti alzi.\n\n\nA) (Continua)";
		mp[2][0][0][1]= "2";
		mp[2][0][0][2]= "2";
		mp[2][0][1][0]= "Dopo un po' senti la voce di Shax...\n\n\nA) (Continua)";
		mp[2][0][1][1]= "2";
		mp[2][0][1][2]= "1";
		//
		mp[2][1][0][0]= "Shax: \"Dimmi, cosa vuoi?\"\n\n\nA) ...\nB) Da cosa?";
		mp[2][1][0][1]= "1";
		mp[2][1][0][2]= "0";
		//
		mp[2][2][0][0]= "";
		mp[2][2][0][1]= "0";
		mp[2][2][0][2]= "0";
		//###############################################################################################################################################################
		mp[3][0][0][0]= "Shax: \"Oh il mio nome e' Shax, te l'ho gia' detto.\nSono stato arrestato circa sei mesi fa, per un disguido tra me, un tizio che parlava troppo e un coltello da macellaio...\"\n\n\nA) Che parlava troppo?\nB) Capito, ne riparliamo dopo.";
		mp[3][0][0][1]= "3";
		mp[3][0][0][2]= "1";
		mp[3][0][1][0]= "Shax: \"Si chiamava Lyr, non ho mai capito cosa ha fatto per essere catturato, cio' che so e' che e' arrivato qui circa un mese dopo di me, ed e' stato giustiziato una settimana fa.\"\n\n\nA) Tu invece non devi essere giustiziato?\nB) Cosa puo' aver fatto per avere quella pena?\nC) Capito, ne riparliamo dopo.";
		mp[3][0][1][1]= "3";
		mp[3][0][1][2]= "3";
		mp[3][0][2][0]= "Shax: \"Ah, questa si' che e' una bella domanda. Comunque no, mi conviene restare qui ancora per un po', ci sono persone la' fuori che aspettano solo che io mi rifaccia vivo per uccidermi.\"\n\n\nA) Ne parli come se potessi decidere di evadere quando vuoi tu.\nB) Voglio evadere.\nC) Capito, ne riparliamo dopo.";
		mp[3][0][2][1]= "3";
		mp[3][0][2][2]= "4";
		mp[3][0][3][0]= "Shax: \"Non c'e' problema. Sai dove trovarmi...Eheheh!\"\n\n\nA) (Continua)";
		mp[3][0][3][1]= "3";
		mp[3][0][3][2]= "5";
		//
		mp[3][1][0][0]= "Shax: \"Ero un assassino su commissione, e il mio lavoro lo facevo bene; pero' mi hanno beccato e adesso sono qui.\"\n\n\nA) Per chi lavoravi?\nB) Capito, ne riparliamo dopo.";
		mp[3][1][0][1]= "3";
		mp[3][1][0][2]= "2";
		mp[3][1][1][0]= "Shax: \"Non c'e' problema. Sai dove trovarmi...Eheheh!\"\n\n\nA) (Continua)";
		mp[3][1][1][1]= "3";
		mp[3][1][1][2]= "5";
		//
		mp[3][2][0][0]= "Shax: \"Me lo stai chiedendo davvero?\nTe la faccio io una domanda: secondo te, se mai usciro' da qui, non mi uccideranno? Specialmente se dico al primo arrivato il nome del mio committente.\"\n\n\nA) (Continua)";
		mp[3][2][0][1]= "3";
		mp[3][2][0][2]= "5";
		mp[3][2][1][0]= "Shax: \"Non c'e' problema. Sai dove trovarmi...Eheheh!\"\n\n\nA) (Continua)";
		mp[3][2][1][1]= "3";
		mp[3][2][1][2]= "5";
		//
		mp[3][3][0][0]= "Shax: \"Cos'e' ti sei gia' scocciato di me!? Ahahah!\nNon credo, mi hanno dato solo l'ergastolo.\"\n\n\nA) (Continua)";
		mp[3][3][0][1]= "3";
		mp[3][3][0][2]= "5";
		mp[3][3][1][0]= "Shax: \"Come ti ho gia' detto non lo so.\nForse qualche pezzo grosso lo voleva morto e l'ha incastrato per bene.\nMeglio cosi', non era un amante della conversazione come te.\"\n\n\nA) (Continua)";
		mp[3][3][1][1]= "3";
		mp[3][3][1][2]= "5";
		mp[3][3][2][0]= "Shax: \"Non c'e' problema. Sai dove trovarmi...Eheheh!\"\n\n\nA) (Continua)";
		mp[3][3][2][1]= "3";
		mp[3][3][2][2]= "5";
		//
		mp[3][4][0][0]= "Shax: \"Infatti e' cosi', ma non e' ancora arrivato il momento.\"\n\n\nA) (Continua)";
		mp[3][4][0][1]= "3";
		mp[3][4][0][2]= "5";
		mp[3][4][1][0]= "Shax: \"Ahahah! Chi non vorrebbe evadere?\"\n\n\nA) (Continua)";
		mp[3][4][1][1]= "3";
		mp[3][4][1][2]= "5";
		mp[3][4][2][0]= "Shax: \"Non c'e' problema. Sai dove trovarmi...Eheheh!\"\n\n\nA) (Continua)";
		mp[3][4][2][1]= "3";
		mp[3][4][2][2]= "5";
		//
		mp[3][5][0][0]= "";
		mp[3][5][0][1]= "0";
		mp[3][5][0][2]= "0";
		//##########################################################################################################################################################################
		mp[4][0][0][0]= "Shax: \"Il suo nome e' Gyll, sta qui da molto, molto tempo. Vero Gyll?\"\n\n\nA) (Continua)";
		mp[4][0][0][1]= "4";
		mp[4][0][0][2]= "1";
		mp[4][0][1][0]= "Gyll: \"No! Gyll non e' pazzo! Lui uscira', li trovera'...i uomini vestiti di bianco. Devono ridare a Gyll cio' che e' suo!\"\n\n\nA) (Continua)";
		mp[4][0][1][1]= "4";
		mp[4][0][1][2]= "4";
		mp[4][0][2][0]= "Shax: \"Non ho detto questo, ma ti costera' e non credo che tu abbia qualcosa di valore\"\n\n\nA) Qualcosa di valore?\nB) Ho solo questo. (Dai Cristallo)";
		mp[4][0][2][1]= "4";
		mp[4][0][2][2]= "5";
		//
		mp[4][1][0][0]= "Gyll: \"Uhm...si,si! Da molto tempo, ma usciro' trovero' gli uomini vestiti di bianco! ...Come...come Ly...\"\n\n\nA) (Continua)";
		mp[4][1][0][1]= "4";
		mp[4][1][0][2]= "2";
		//
		mp[4][2][0][0]= "(Interrompendolo)\nShax: \"Ah, Gyll, Gyll, quanto sei pazzo? Eheheh!\n\n\nA) (Continua)";
		mp[4][2][0][1]= "4";
		mp[4][2][0][2]= "3";
		//
		mp[4][3][0][0]= "Gyll: \"No! Gyll non e' pazzo! Lui uscira', li trovera'...i uomini vestiti di bianco. Devono ridare a Gyll cio' che e' suo!\"\n\n\nA) (Continua)";
		mp[4][3][0][1]= "4";
		mp[4][3][0][2]= "4";
		//
		mp[4][4][0][0]= "Shax: \"Quindi tu vuoi uscire, bene, ma ti costera' e non credo che tu abbia qualcosa di valore\"\n\n\nA) Qualcosa di valore?\nB) Ho solo questo. (Dai Cristallo)";
		mp[4][4][0][1]= "4";
		mp[4][4][0][2]= "5";
		//
		mp[4][5][0][0]= "Shax: \"Come vedi non e' cosi' facile, devo avere qualcosa in cambio per correre certi rischi.\"\n\n\nA) (Continua)";
		mp[4][5][0][1]= "4";
		mp[4][5][0][2]= "8";
		mp[4][5][1][0]= "Shax: \"Uhm, interessante...e' un cristallo dal valore mediocre, dove l'hai trovato?\"\n\n\nA) Ha importanza?\nB) (Di' la verita')";
		mp[4][5][1][1]= "4";
		mp[4][5][1][2]= "6";
		//
		mp[4][6][0][0]= "Shax: \"A dir la verita' no, bene, dammelo e fammi sapere quando sei pronto.\"\n\n\nA) (Continua)";
		mp[4][6][0][1]= "4";
		mp[4][6][0][2]= "8";
		mp[4][6][1][0]= "Tu: \"Questo mi e' stato regalato da mia madre, non so da dove provenga, e' un portafortuna.\"\n\n\nA) (Continua)";
		mp[4][6][1][1]= "4";
		mp[4][6][1][2]= "7";
		//
		mp[4][7][0][0]= "Shax: \"Non deve averti portato cosi' tanta fortuna, visto che sei finito qui. Ahahah!\nComunque, dammelo e fammi sapere quando sei pronto.\"\n\n\nA) (Continua)";
		mp[4][7][0][1]= "4";
		mp[4][7][0][2]= "8";
		//
		mp[4][8][0][0]= "";
		mp[4][8][0][1]= "0";
		mp[4][8][0][2]= "0";
		//###########################################################################################################################################################
		mp[5][0][0][0]= "Shax: \"Bene, allora si comincia; stenditi a letto quando sara' il momento lo capirai da solo.\"\n\n\nA) (Continua)";
		mp[5][0][0][1]= "5";
		mp[5][0][0][2]= "1";
		mp[5][0][1][0]= "Shax: \"Si', si' tanto non ci corre dietro nessuno.\"\n\n\nA) (Continua)";
		mp[5][0][1][1]= "5";
		mp[5][0][1][2]= "1";
		//
		mp[5][1][0][0]= "";
		mp[5][1][0][1]= "0";
		mp[5][1][0][2]= "0";
		//###########################################################################################################################################################
		mp[6][0][0][0]= "Guardia: \"Hey! Cosa fai!?\n\n\nA) (Continua)";
		mp[6][0][0][1]= "6";
		mp[6][0][0][2]= "3";
		mp[6][0][1][0]= "Guardia: \"Scappa! Scappa! Sono tutti morti! Tutti!\n\n\nA) (Continua)";
		mp[6][0][1][1]= "6";
		mp[6][0][1][2]= "1";
		//
		mp[6][1][0][0]= "Per un momento lo sguardo della guardia cade sulla tua armatura sporca di sangue.\n\n\nA) (Continua)";
		mp[6][1][0][1]= "6";
		mp[6][1][0][2]= "2";
		//
		mp[6][2][0][0]= "Guardia: \"Tu...tu...ma come hai fatto? (Sguaina l'arma)\n\n\nA) (Continua)";
		mp[6][2][0][1]= "6";
		mp[6][2][0][2]= "3";
		//
		mp[6][3][0][0]= "";
		mp[6][3][0][1]= "0";
		mp[6][3][0][2]= "0";
		//#########################################################################################################################################
		mp[7][0][0][0] = "";
		mp[7][0][0][1] = "0";
		mp[7][0][0][2] = "0";
	}
}
