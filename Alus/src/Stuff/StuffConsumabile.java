package Stuff;
/**
 * Questa classe astratta estende e specializza la classe {@link Stuff}.
 * Essa non implementa alcun metodo, infatti e' utilizzata solo ai fini della tipizzazzione:
 * uno {@code StuffConsumabile} ha la carettistica di poter essere una sola volta, dopo
 * il suo utilizzo, infatti, viene eliminato dal {@code Personaggio} che lo possiede
 * tramite il metodo opportuno. Inoltre, da un'ulteriore tipizazzione tramite 
 * l'implementazione di alcune interfaccie tra {@link Accessorio}, {@link Arma}, {@link Armatura},
 * {@link Scudo}, {@link StuffChiave} ed {@link UsabileInCombattimento}.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public abstract class StuffConsumabile extends Stuff {
	
	

}
