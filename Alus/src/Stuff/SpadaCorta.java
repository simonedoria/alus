package Stuff;

import Personaggi.Personaggio;

/**
 * Questa classe estende concretizza {@link StuffEquipaggiabile} ed implementa l'interfaccia
 * {@link Arma}. Essa e' dichiarata {@code final} in quanto non puo' essere ulteriormente
 * specializzata.
 * <br>Implementa un metodo Costruttore e concretizza i metodi {@link #attiva(Personaggio)}
 * e {@link #disattiva(Personaggio)}.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 */
public final class SpadaCorta extends StuffEquipaggiabile implements Arma {
	
	/**
	 * Costruttore.
	 * <br> Inizializza gli attributi di this con dei valori preimpostati.
	 */
	public SpadaCorta() {
		nome="Spada Corta";
		sigla="Spada Corta";
		descrizione="Una normale spada d'ordinanza, servita in dotazione alle guardie della prigione di Tyrell.";
		slug="spadacorta";
	}
	
	
	@Override
	/**
	 * 
	 * 
	 */
	public void disattiva(Personaggio p) {
		p.setBonusforz(p.getBonusforz()-2);
		p.setBonusdif(p.getBonusdif()-1);
	}

	@Override
	/**
	 * 
	 */
	public void attiva(Personaggio p) {
		p.setBonusforz(p.getBonusforz()+2);
		p.setBonusdif(p.getBonusdif()+1);
	}

}
