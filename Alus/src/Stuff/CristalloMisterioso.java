package Stuff;

import Personaggi.Personaggio;

/**
 * Questa classe estende concretizza {@link StuffNonConsumabile} ed implementa l'interfaccia
 * {@link StuffChiave}. Essa e' dichiarata {@code final} in quanto non puo' essere ulteriormente
 * specializzata.
 * <br>Implementa un metodo Costruttore e concretizza i metodi {@link #attiva(Personaggio)}
 * e {@link #disattiva(Personaggio)}.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 */
public final class CristalloMisterioso extends StuffNonConsumabile implements StuffChiave{
	
	/**
	 * Costruttore.
	 * <br> Inizializza gli attributi di this con dei valori preimpostati.
	 */
	public CristalloMisterioso(){
		sigla="Crist. Mist.";
		nome="Cristallo Misterioso";
		slug="cristmisterioso";
		descrizione="Una strana pietra, trovata da Carl nella sua citta' natale, Foren.\nHa un grande valore affettivo per lui.";
	}

	@Override
	/**
	 * 
	 */
	public void attiva(Personaggio p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	/**
	 * 
	 */
	public void disattiva(Personaggio p) {
		// TODO Auto-generated method stub
		
	}

}
