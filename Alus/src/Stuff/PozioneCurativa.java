package Stuff;

import Personaggi.Personaggio;

/**
 * Questa classe estende concretizza {@link StuffConsumabile} ed implementa l'interfaccia
 * {@link UsabileInCombattimento}. Essa e' dichiarata {@code final} in quanto non puo' essere ulteriormente
 * specializzata.
 * <br>Implementa un metodo Costruttore e concretizza i metodi {@link #attiva(Personaggio)}
 * e {@link #disattiva(Personaggio)}.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 */
public final class PozioneCurativa extends StuffConsumabile implements UsabileInCombattimento {
	
	/**
	 * Costruttore.
	 * <br> Inizializza gli attributi di this con dei valori preimpostati.
	 */
	public PozioneCurativa(){
		nome = "Pozione Curativa";
		descrizione = "Rimarginare le ferite, curare le malattie... Molte cose sono possibili ora, grazie alla magia.";
		sigla = "Pozione Curativa";
		slug = "pcurativa";
	}
	
	@Override
	/**
	 * 
	 */
	public void attiva(Personaggio p){
		p.setPv(p.getPv() + 20);
		if(qnt==1) p.getInventario().remove(this);
		else if(qnt>1) qnt--;
	}

	@Override
	/**
	 * 
	 */
	public void disattiva(Personaggio p) {}
}
