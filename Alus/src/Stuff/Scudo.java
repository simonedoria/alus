package Stuff;


/**
 * Questa intefaccia ha la funzione di rafforzare la tipizazzione di un oggetto 
 * di un <b>sottotipo concreto</b>  di {@code Stuff}, essa infatti non dichiara alcun metodo.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 * @see Stuff.
 */
public interface Scudo {

}
