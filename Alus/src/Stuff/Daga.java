package Stuff;

import Personaggi.Personaggio;

/**
 * Questa classe estende concretizza {@link StuffEquipaggiabile} ed implementa l'interfaccia
 * {@link Arma}. Essa e' dichiarata {@code final} in quanto non puo' essere ulteriormente
 * specializzata.
 * <br>Implementa un metodo Costruttore e concretizza i metodi {@link #attiva(Personaggio)}
 * e {@link #disattiva(Personaggio)}.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 */
public final class Daga extends StuffEquipaggiabile implements Arma {
		
	/**
	 * Costruttore.
	 * <br> Inizializza gli attributi di this con dei valori preimpostati.
	 */
	public Daga(){
		sigla="Daga";
		nome="Daga";
		slug="daga";
		descrizione="Una semplice daga." 
				+"\nLeggera e con dimensioni contenute, ideale per essere nascosta tra i vestiti.";
	}
	@Override
	/**
	 * 
	 */
	public void attiva(Personaggio p){
		p.setBonusforz(p.getBonusforz()+1);
		p.setBonusdes(p.getBonusdes()+3);
	}
	@Override
	/**
	 * 
	 */
	public void disattiva(Personaggio p){
		p.setBonusforz(p.getBonusforz()-1);
		p.setBonusdes(p.getBonusdes()-3);
	}
	
}
