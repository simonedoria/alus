package Stuff;

import Personaggi.Personaggio;

/**
 * Questa classe definisce un oggetto di tipo {@code Stuff}. E' dichiarata astratta
 * perche' e' alla base della struttura di un oggetto di questo tipo; essa infatti viene 
 * estesa da altre classi astratte che ne specializzano la tipizazzione.
 * <p>
 * Un oggetto <b>{@code Stuff}</b> e' composto da:
 * <ul>
 * <li>sigla;
 * <li>nome;
 * <li>descrizione;
 * <li>slug.
 * </ul>
 * Inoltre implementa due metodi ({@link #attiva(Personaggio)} e {@link #disattiva(Personaggio)})
 * che descrivono la logica di ogni singolo {@code Stuff}.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public abstract class Stuff {
	
protected String sigla, nome, descrizione, slug;
protected int qnt = 1;
	
	/**
	 * Descrive la logica di {@code this}.
	 * Applica le opportune modifiche all'oggetto {@code Personaggio} passato come parametro.
	 * Esso viene richiamato quando l'utente decide di utilizzare questo stuff.
	 * @param p {@code Personaggio} che possiede lo {@code Stuff this} ed al quale vengono applicate le modifiche.
	 */
	public abstract void attiva(Personaggio p);
	
	/**
	 * Esegue l'operazione contraria di {@link #attiva(Personaggio)}:
	 * cancella le modifiche applicate da quest'ultimo sul {@code Personaggio} 
	 * passato come parametro.
	 * @param p {@code Personaggio} che possiede lo {@code Stuff this} ed al quale vengono 
	 * cancellate le modifiche fatte dallo {@code this}.
	 */
	public abstract void disattiva(Personaggio p);
	
	//METODI ACCESSORI 
	/**
	 * Ritona il nome di {@code this} come stringa.
	 * @return il nome di {@code this}l
	 */
	public String getNome() {return nome;}
	
	/**
	 * Modifica il nome di {@code this} con la strtinga passatogli come parametro.
	 * @param nome Nuovo nome di {@code this}.
	 */
	public void setNome(String nome) {this.nome = nome;}
	
	/**
	 * Ritorna lo "slug" di {@code this} come stringa.
	 * @return lo "slug" di {@code this}.
	 */
	public String getSlug() {return slug;}
	
	/**
	 * Modifica lo "slug" di {@code this} con la stringa passata come parametro. 
	 * @param slug Il nuovo "slug" di {@code this}.
	 */
	public void setSlug(String slug) {this.slug = slug;}
	
	/**
	 * Ritorna la descrizione di {@code this} come stringa.
	 * @return La descrizione di {@code this}.
	 */
	public String getDescrizione(){return descrizione;}
	
	/**
	 * Modifica la descrizine di {@code this} con quella passatogli come parametro.
	 * @param des La nuova descrizione di {@code this}.
	 */
	public void setDescrizione(String des){descrizione=des;}
	
	/**
	 * Ritorna la sigla di {@code this} come stringa.
	 * @return La sigla di {@code this}.
	 */
	public String getSigla() { return sigla; }
	
	/**
	 * Ritorna il numero di oggetti dello stesso tipo di {@code this} che un personaggio puo'
	 * avere.
	 * @return La quantita' di oggetti di tipo {@code this} che un personaggio puo' avere.
	 */
	public int getQnt() { return qnt; }
	
	/**
	 * Modifica il numero di oggetti di tipo {@code this} che un {@code Personaggio} puo' avere.
	 * @param qnt Nuova quantita' di oggetti {@code this} ch eun {@code Personaggio} pu' avere.
	 */
	public void setQnt(int qnt) { this.qnt = qnt; }

}
