package Stuff;

import Personaggi.Personaggio;

/**
 * Questa classe estende concretizza {@link StuffEquipaggiabile} ed implementa l'interfaccia
 * {@link Armatura}. Essa e' dichiarata {@code final} in quanto non puo' essere ulteriormente
 * specializzata.
 * <br>Implementa un metodo Costruttore e concretizza i metodi {@link #attiva(Personaggio)}
 * e {@link #disattiva(Personaggio)}.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 */
public final class ArmaturaDaGuardiaCarceraria extends StuffEquipaggiabile implements Armatura {
	
	/**
	 * Costruttore.
	 * <br> Inizializza gli attributi di this con dei valori preimpostati.
	 */
	public ArmaturaDaGuardiaCarceraria(){
		sigla="Arm. da guardia";
		nome = "Armatura da guardia carceraria";
		descrizione = "Un armatura semplice presa in \"prestito\" da una guardia.\n Non penso che le servira' piu'.";
		slug="armguacar";
	}
	@Override
	/**
	 * 
	 */
	public void attiva(Personaggio p){
		p.setBonusdif(p.getBonusdif()+2);
	}
	
	@Override
	/**
	 * 
	 */
	public void disattiva(Personaggio p){
		p.setBonusdif(p.getBonusdif()-2);
	}
	
	

}
