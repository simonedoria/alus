package Stuff;

/**
 * Questa classe astratta estende e specializza la classe {@link Stuff}.
 * Essa non implementa alcun metodo, infatti e' utilizzata solo ai fini della tipizzazzione:
 * uno {@code StuffNonConsumabile} ha la carettistica di poter essere usata infinite volte
 * fin quando il {@code Personaggio} la possiede.  Inoltre, da un'ulteriore tipizazzione tramite 
 * l'implementazione di alcune interfaccie tra {@link Accessorio}, {@link Arma}, {@link Armatura},
 * {@link Scudo}, {@link StuffChiave} ed {@link UsabileInCombattimento}.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public abstract class StuffNonConsumabile extends Stuff {
	
}
