package Stuff;

/**
 * Questa classe astratta estende e specializza la classe {@link StuffNonConsumabile}.
 * Essa non implementa alcun metodo, infatti e' utilizzata solo ai fini della tipizzazzione:
 * uno {@code StuffConsumabile} e' caratterizzato, inoltre, da un'ulteriore tipizazzione tramite 
 * l'implementazione di alcune interfaccie tra {@link Accessorio}, {@link Arma}, {@link Armatura},
 * {@link Scudo}, {@link StuffChiave} ed {@link UsabileInCombattimento}.
 * Inoltre, uno {@code Stuff} di questo tipo per essere utilizzato deve occupare uno degli <b>slot</b>,
 * presenti in un {@code Personaggio}, ed infine, fin quando e' equipaggiato in uno slot puo'
 * essere utilizzato infinite volte in quanto estende {@code StuffNonConsumabile}.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public abstract class StuffEquipaggiabile extends StuffNonConsumabile {
	
	//public abstract void disattiva(Personaggio p);

}
