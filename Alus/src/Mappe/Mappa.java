package Mappe;

import java.io.IOException;

import Abilita.*;
import Alus.Game;
import Combattimento.Lotta;
import Personaggi.Personaggio;
import States.State;
import Stuff.*;
import System.*;

/**
 * Questa classe di tipo abstract contiene tutti i settaggi ed i metodi che sono necessari per far funzionare la Mappa<br>
 * ma soprattutto l'insieme delle dinamiche del gioco.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public abstract class Mappa {
	
	protected String nomeMappa;
	
	protected TransparentTextField textbox = Game.display.textbox;
	protected String cmdLotta = " ";
	/**
	 * Un {@code Array} di {@code String} che contiene l'elenco dei comandi eseguibili.
	 */
	public String [] comandi;
	/**
	 * Una {@code String} che registra l'ultimo comando inserito.
	 */
	public String cmdCorrente;
	/**
	 * Un {@code Array} di {@code String} che contiene SOLO l'elenco dei comandi verbali.
	 */
	public String [] comandiVerbali;
	/**
	 * Una {@code String} che contiene l'help.
	 */
	public String help;
	/**
	 * Una {@code String} che l'ultimo testo stampato nel box centrale.
	 */
	public String printaCorrente;
	/**
	 * Un attributo di tipo {@code StatoDelGioco} che contiene tutti i valori di tipo {@code Boolean} che andranno a condizionare l'andamento della storia.
	 */
	public StatoDelGioco st;
	/**
	 * Un attributo di tipo {@code Personaggio} che contiene l'indirizzo del protagonista.
	 */
	public Personaggio protagonista;
	/**
	 * Un attributo di tipo {@code Personaggio} che contiene l'indirizzo dell'avversario.
	 */
	public Personaggio avversario;
	/**
	 * Un attributo di tipo {@code String} che contiene il nome del protagonista.
	 */
	public String nomeP;
	/**
	 * Un attributo di tipo {@code Lotta} che contiene l'indirizzo alla lotta.
	 */
	public Lotta lotta;
	
	//paramentri per gestire il multistringa
	protected StringBuilder multiStringa = new StringBuilder();
	protected boolean multiStringhe = false;
	protected int con;
	protected boolean done = false;
	protected String obj = "";
	protected boolean ambiente = false;

	//parametri di gestione risposte agli statement
	protected int event = 0, statement = 0;
	
	protected String output = " ";
	
	/**
	 * Il play della mappa, fondamentale.
	 */
	public abstract void play();
	/**
	 * COSTRUTTORE senza paramentri
	 * Setta un nuovo {@code Protagonista}, un nuovo {@code StatoDelGioco} ed aggiunge al protagonista gli oggetti iniziali di base e le abilita'.
	 */
	public Mappa() {
		protagonista = new Personaggio("Carl", 40, 40, 16, 16, 3, 0, 1, 0, 5, 0, 1, 0, 1, 0, 1, 6, 0, 1.5, 0, 0);
		st = new StatoDelGioco();
		protagonista.addStuff(new CristalloMisterioso());
		protagonista.addAbilita(new Attacca());
		protagonista.addAbilita(new Difendi());
		protagonista.addAbilita(new AttaccoFurtivo());
	}
	/**
	 * @return Una stringa con l'help del gioco.
	 */
	public String generaHelp() {
		StringBuilder helpp = new StringBuilder();
		helpp.append("");
		for(int i=0; i<comandi.length; i++) {
			helpp.append(comandi[i]);
			if(i<comandi.length-1) helpp.append(", ");
		}
		help = helpp.toString();
		return help;
	}
	/**
	 * @param str è un vararg che riceve 1 o più {@code String}
	 * Setta i comandi che e' possibile eseguire dall'inputbox.
	 */
	public void allow(String ... str) {
		//nel caso in cui si stiano dando degli allow su statement, salva i comandi verbali in un array specifico
		if(str[0].equals("A") && !comandi[0].equals("A")) {
			comandiVerbali = new String[comandi.length];
			for(int i = 0; i < comandi.length; i++) {
				comandiVerbali[i] = comandi[i];
			}
		}
		//setto normalmente gli array
		cmdCorrente = new String("");
		comandi = new String[str.length];
		for(int i = 0; i < str.length; i++) {
			comandi[i] = str[i];
		}
	}
	/**
	 * @return L'output che viene mostrato a video.
	 */
	public String getOutput() {
		return output;
	}
	/**
	 * @return Il comando dato nella lotta.
	 */
	public String getCmdLotta() {
		return cmdLotta;
	}
	/**
	 * @param cmd
	 * Riceve un parametro di tipo {@code String}.<br>Setta il comando della lotta.
	 */
	public void setCmdLotta(String cmd) {
		cmdLotta = cmd;
	}
	/**
	 * @param out
	 * Riceve un vararg di stringhe, setta il printaCorrente.
	 */
	public void printa(String ... out) {
		if(out.length==1) {
			State.getState().fadingTesto = 0;
			if(printaCorrente==null) printaCorrente = out[0]; //nel caso in cui si tratta del primissimo print
			output = out[0];
			return;
		} 
		if(out[1].equals("avviso")) {
			State.getState().fadingTesto = 0;
			printaCorrente = output;
			output = out[0];
			return;
		} if(out.length>1) {
			State.getState().fadingTesto = 0;
			StringBuilder sb = new StringBuilder();
			for(int i=0; i<out.length; i++) {
				sb.append(out[i]);
				if(printaCorrente==null) printaCorrente = sb.toString();
				output = sb.toString();
				State.getState().fadingTesto = 0;
			}
			return;
		}
	}

	/**
	 * @param p
	 * Setta l'avversario ricevendo un parametro di tipo {@code Personaggio}
	 */
	public void setAvversario(Personaggio p) {
		avversario = p;
	}
	/**
	 * Metodo astratto che viene eseguito quando ritorni dalla battaglia.
	 */
	public abstract void returnBattle();
	
	/**
	 * @param nx
	 * @return Ritorna False se il vararg di {@code String} che si e' passato come paramentro non e' ancora finito di essere printato.<br>
	 * Ritorna True quando ha finito e si puo' proseguire con in play.
	 */
	protected boolean narrazione(String ... nx) {
		if(nx.length==1) {
			printa(nx[0]);
			return true;
		}
		//se e' stata gia' printata una volta, stampa tutto l'array e basta
		if(done) return true;
		else {
			//se si tratta della prima volta
			printa(nx[con]);
			con++;
			if(con==nx.length) {
				done = true;
				con = 0;
				return true;
			}
			allow("");
			return false;
		}
	}
	/**
	 * @param cmd
	 * Attiva lo {@code Stuff} che si e' passato come parametro {@code String} preceduto dlla parola "attiva".
	 */
	public void attivaStuff(String cmd) {
		//controlli sugli stuff consumabili, usabili in entrambi gli states
		for(int i = 0; i<protagonista.getInventario().size(); i++) {
			if(protagonista.getInventario().get(i) instanceof StuffConsumabile) {
				if(cmd.toUpperCase().equals("ATTIVA "+protagonista.getInventario().get(i).getNome().toUpperCase())) {
					protagonista.getInventario().get(i).attiva(protagonista);
				}
			}
		}
	}
	/**
	 * @param cmd
	 * Disequipaggia lo {@code Stuff} che si e' passato come parametro {@code String} preceduto dlla parola "disequipaggia".
	 */
	public void disequipaggiaStuff(String cmd) {
		//controlli sugli equipaggiamenti
		for(int i = 0; i<protagonista.getInventario().size(); i++) {
			if(protagonista.getArma()!=null && protagonista.getInventario().get(i).getNome().equals(protagonista.getArma().getNome())) {
				if(cmd.toUpperCase().equals("DISEQUIPAGGIA "+protagonista.getInventario().get(i).getNome().toUpperCase())) {
					protagonista.getArma().disattiva(protagonista);
					protagonista.setArma(null);
					printa("Hai disequipaggiato: " + protagonista.getInventario().get(i).getNome(), "avviso");
					RoutineSystem.dormi(1000);
					printa(printaCorrente);
				}
			}
			if(protagonista.getArmaScudo()!=null && protagonista.getInventario().get(i).getNome().equals(protagonista.getArmaScudo().getNome())) {
				if(cmd.toUpperCase().equals("DISEQUIPAGGIA "+protagonista.getInventario().get(i).getNome().toUpperCase())) {
					protagonista.getArmaScudo().disattiva(protagonista);
					protagonista.setArmaScudo(null);
					printa("Hai disequipaggiato: " + protagonista.getInventario().get(i).getNome(), "avviso");
					RoutineSystem.dormi(1000);
					printa(printaCorrente);
				}
			}
			if(protagonista.getArmatura()!=null && protagonista.getInventario().get(i).getNome().equals(protagonista.getArmatura().getNome())) {
				if(cmd.toUpperCase().equals("DISEQUIPAGGIA "+protagonista.getInventario().get(i).getNome().toUpperCase())) {
					protagonista.getArmatura().disattiva(protagonista);
					protagonista.setArmatura(null);
					printa("Hai disequipaggiato: " + protagonista.getInventario().get(i).getNome(), "avviso");
					RoutineSystem.dormi(1000);
					printa(printaCorrente);
				}
			}
			if(protagonista.getAccessorio()!=null && protagonista.getInventario().get(i).getNome().equals(protagonista.getAccessorio().getNome())) {
				if(cmd.toUpperCase().equals("DISEQUIPAGGIA "+protagonista.getInventario().get(i).getNome().toUpperCase())) {
					protagonista.getAccessorio().disattiva(protagonista);
					protagonista.setAccessorio(null);
					printa("Hai disequipaggiato: " + protagonista.getInventario().get(i).getNome(), "avviso");
					RoutineSystem.dormi(1000);
					printa(printaCorrente);
				}
			}
		}
	}
	
	public void equipaggiaStuff(String cmd) {
		//controlli sugli equipaggiamenti
		for(int i = 0; i<protagonista.getInventario().size(); i++) {
			//se si tratta di un'arma
			if(protagonista.getInventario().get(i) instanceof Arma) {
				if(cmd.toUpperCase().equals("EQUIPAGGIA "+protagonista.getInventario().get(i).getNome().toUpperCase())) {
					if(protagonista.getArma() == null) {
						protagonista.setArma(protagonista.getInventario().get(i));
						protagonista.getArma().attiva(protagonista);
					} else {
						if(protagonista.getArmaScudo() != null) protagonista.getArmaScudo().disattiva(protagonista);
						protagonista.setArmaScudo(protagonista.getInventario().get(i));
						protagonista.getArmaScudo().attiva(protagonista);
					}
					printa("Hai equipaggiato: " + protagonista.getInventario().get(i).getNome(), "avviso");
					RoutineSystem.dormi(1000);
					printa(printaCorrente);
				}
			}
			//se si tratta di un armaScudo
			if(protagonista.getInventario().get(i) instanceof Scudo) {
				if(cmd.toUpperCase().equals("EQUIPAGGIA "+protagonista.getInventario().get(i).getNome().toUpperCase())) {
					if(protagonista.getArmaScudo() == null) {
						protagonista.setArmaScudo(protagonista.getInventario().get(i));
						protagonista.getArmaScudo().attiva(protagonista);
					} else {
						protagonista.getArmaScudo().disattiva(protagonista);
						protagonista.setArmaScudo(protagonista.getInventario().get(i));
						protagonista.getArmaScudo().attiva(protagonista);
					}
					printa("Hai equipaggiato: " + protagonista.getInventario().get(i).getNome(), "avviso");
					RoutineSystem.dormi(1000);
					printa(printaCorrente);
				}
			}
			//se si tratta di un'armatura
			if(protagonista.getInventario().get(i) instanceof Armatura) {
				if(cmd.toUpperCase().equals("EQUIPAGGIA "+protagonista.getInventario().get(i).getNome().toUpperCase())) {
					if(protagonista.getArmatura() == null) {
						protagonista.setArmatura(protagonista.getInventario().get(i));
						protagonista.getArmatura().attiva(protagonista);
					} else {
						protagonista.getArmatura().disattiva(protagonista);
						protagonista.setArmatura(protagonista.getInventario().get(i));
						protagonista.getArmatura().attiva(protagonista);
					}
					printa("Hai equipaggiato: " + protagonista.getInventario().get(i).getNome(), "avviso");
					RoutineSystem.dormi(1000);
					printa(printaCorrente);
				}
			}
			//se si tratta di un accessorio
			if(protagonista.getInventario().get(i) instanceof Accessorio) {
				if(cmd.toUpperCase().equals("EQUIPAGGIA "+protagonista.getInventario().get(i).getNome().toUpperCase())) {
					if(protagonista.getAccessorio() == null) {
						protagonista.setAccessorio(protagonista.getInventario().get(i));
						protagonista.getAccessorio().attiva(protagonista);
					} else {
						protagonista.getAccessorio().disattiva(protagonista);
						protagonista.setAccessorio(protagonista.getInventario().get(i));
						protagonista.getAccessorio().attiva(protagonista);
					}
					printa("Hai equipaggiato: " + protagonista.getInventario().get(i).getNome(), "avviso");
					RoutineSystem.dormi(1000);
					printa(printaCorrente);
				}
			}
		}
	}
	/**
	 * @param cmd
	 * Equipaggia lo {@code Stuff} che si e' passato come parametro {@code String} preceduto dlla parola "equipaggia".
	 */
	public void salvaPartita(String str) {
		String[] cmd = str.split("\\s");
		String pattern = "^[a-zA-Z0-9]*$"; //espressione regolare che controlla se si tratta di una stringa alfanumerica
		if(cmd.length != 2) return;
		if(cmd[0].toUpperCase().equals("SALVA")) {
			if(!cmd[1].matches(pattern)) {
				printa("Perfavore inserisci solo caratteri alfanumerici per il nomefile.", "avviso");
				RoutineSystem.dormi(2000);
				printa(printaCorrente);
				return;
			}
			if(cmd[1].length() > 15) {
				printa("La lunghezza dei caratteri deve essere massimo di 15.", "avviso");
				RoutineSystem.dormi(2000);
				printa(printaCorrente);
				return;
			}
			try {
				MemoryCard.salva(protagonista.toString()+"@"+st.toString()+"@"+nomeMappa, cmd[1]);
			} catch (IOException e) {
				e.printStackTrace();
			}
			printa("Il file "+cmd[1]+".alus e' stato salvato con successo.", "avviso");
			RoutineSystem.dormi(2000);
			printa(printaCorrente);
		}
	}
}
