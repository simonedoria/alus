package Mappe;

/**
 * La classe StatoDelGioco e' una serie di booleani la cui diversa combinazione da appunto l'esatto stato in cui si trova il gioco.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class StatoDelGioco {
	public boolean m1v;
	public boolean m1misterioso;
	public boolean m1shax;
	public boolean m1cellaovest;
	public boolean m1verita;
	public boolean m1evasione;
	public boolean m1evaso;
	public boolean m1cristallodato;
	public boolean m1pronto;
	public boolean m1fuori;
	public boolean m1guardiauccisa;
	public boolean m1scontro;
	public boolean m1shax2;
	/**
	 * COSTRUTTORE<br>
	 * Senza argomenti nel caso in cui si tratta di una nuova partita.
	 */
	public StatoDelGioco() {
		m1v = false;         //0
		m1misterioso = false;	 //1
		m1shax = false;    //2
		m1cellaovest = false;    //3
		m1evasione = false; // 4
		m1evaso = false; // 5
		m1cristallodato = false;  //6
		m1pronto = false; //7
		m1fuori = false; //8
		m1guardiauccisa = false; //9
		m1scontro=false; //10
		m1shax2=false; //11
	}
	/**
	 * COSTRUTTORE<br>
	 * Con argomenti nel caso in cui si tratta di una partita salvata.
	 */
	public StatoDelGioco(boolean m1v, boolean m1misterioso, boolean m1shax, boolean m1cellaovest, boolean m1evasione, boolean m1evaso, boolean m1cristallodato, boolean m1pronto, boolean m1fuori, boolean m1guardiauccisa, boolean m1scontro, boolean m1shax2) {
		this.m1v = m1v;						//0
		this.m1misterioso = m1misterioso;		//1
		this.m1shax = m1shax;			//2
		this.m1cellaovest= m1cellaovest;     //3
		this.m1evasione =m1evasione; //4
		this.m1evaso= m1evaso; //5
		this.m1cristallodato=m1cristallodato; //6
		this.m1pronto=m1pronto; //7
		this.m1fuori=m1fuori; //8
		this.m1guardiauccisa=m1guardiauccisa; //9
		this.m1scontro=m1scontro; //10
		this.m1shax2=m1shax2; //11
	} 
	
	/*
	 * Nel metodo toString viene fatto il controllo per ogni booleano e convertito a 0 se l'attuale valore è falseo, altrimento a true
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		//blocco 0
		if(!this.m1v) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 1
		if(!this.m1misterioso) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 2
		if(!this.m1shax) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 3
		if(!this.m1cellaovest) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 4
		if(!this.m1evasione) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 5
		if(!this.m1evaso) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 6
		if(!this.m1cristallodato) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 7
		if(!this.m1pronto) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 8
		if(!this.m1fuori) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 9
		if(!this.m1guardiauccisa) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 10
		if(!this.m1scontro) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		//blocco 11
		if(!this.m1shax2) sb.append("0");
		else sb.append("1");
		sb.append(" ");
		
		return sb.toString();
	}
}
