package Mappe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Alus.*;
import Combattimento.Lotta;
import Dialoghi.Mappa1Dialoghi;
import Dialoghi.Narrazione;
import Dialoghi.Shax;
import Personaggi.*;
import States.*;
import Stuff.*;
import System.RoutineSystem;

/**
 * In questa classe si sviluppa la storia della prima {@code Mappa}
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class Mappa1 extends Mappa {
	
	private Mappa1Dialoghi dialoghi;
	private int temp;
	
	/**
	 * COSTRUTTORE
	 * Predispone il gioco ad una nuova partita.
	 */
	public Mappa1() {
		super();
		nomeMappa = "mappa1";  //serve per il salvataggio !!! IMPORTANTE!!!
		//inizializzo i dialoghi della mappa1
		dialoghi = new Mappa1Dialoghi();
		dialoghi.setRisposteDialoghi();
		//inizializzo il listener sul textbox
		textbox.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	if(textbox.getText().equals("")) textbox.setText("");
		        submit(textbox.getText()); //da modificare
		        textbox.setText("");
		    }
		});
		
	}
	/**
	 * @param p
	 * @param stt
	 * Riceve come paramentri un {@code Personaggio} e un {@code StatoDelGioco} provenienti da {@code MemoryCard}.
	 */
	public Mappa1(Personaggio p, StatoDelGioco stt) {
		protagonista = p;
		st = stt;
		nomeMappa = "mappa1";  //serve per il salvataggio !!! IMPORTANTE!!!
		con = 0;
		//inizializzo il listener sul textbox
		textbox.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	if(textbox.getText().equals("")) textbox.setText("");
		        submit(textbox.getText()); //da modificare
		        textbox.setText("");
		    }
		});
		//inizializzo i dialoghi della mappa1
		dialoghi = new Mappa1Dialoghi();
		dialoghi.setRisposteDialoghi();
	}
	/**
	 * @param cmd
	 * Questo metodo analizza tutto cio' che viene inviato dal textbox.
	 */
	public void submit(String cmd) {
		cmd.trim(); //rimuove blankspaces a sinistra e a destra
		if(State.getState() instanceof GameState) salvaPartita(cmd); //metodo che effettua il controllo se si è mandata la richiesta di salvataggio
		attivaStuff(cmd); //metodo che effettua i controlli sugli stuff da attivare
		equipaggiaStuff(cmd); //metodo che effettua i controlli sull'equipaggiamento da attivare
		disequipaggiaStuff(cmd); //metodo che effettua i controlli sull'equipaggiamento da disattivare
		if(State.getState() instanceof BattleState && avversario.getPv()<=0) {
			if(cmd.toUpperCase().equals("ESCI LOTTA")) {
				Game.gameState.setState(Game.gameState); //cambio scenario grafico
				Assets.mute();
				Assets.prigioneTheme.start(); //start audio prigione
				play();
				return;
			}
			return;
		}
		if(State.getState() instanceof BattleState) {
			cmdLotta = cmd;
		}
		if(cmd.equals("") && !done) {
			play();
			return;
		}
		
		if(State.getState() instanceof GameState && cmd.toUpperCase().equals("ESCI")) {
			Assets.mute();
			Game.menuState = new MenuState();
			Game.gameState.setState(Game.menuState);
			Game.display.panel.setVisible(false);
			Game.gameState = null;
		}
		if(cmd.toUpperCase().equals("HELP")) {
			printaCorrente = output;
			printa(generaHelp());
			return;
		}
		if(cmd.toUpperCase().equals("OSSERVARE AMBIENTE") && ambiente) {
			play();
			printaCorrente = output;
			return;
		}
		if(output.equals(help) && cmd.toUpperCase().equals("INDIETRO")) {
			printa(printaCorrente);
		}
		cmdCorrente = cmd;
		printaCorrente = output;
		if(RoutineSystem.parseString(cmdCorrente, comandi)) eseguiComandi();
		Game.display.textbox.setText("");
	}
	
	// METODO CENTRALE DI PLAY. DOVE ACCADE TUTTO
	/**
	 * In questo metodo si sviluppano tutte le dinamiche del gioco.
	 */
	@Override
	public void play() {
		allow("OSSERVARE SECCHIO", "OSSERVARE SHAX", "OSSERVARE LETTI", "USARE LETTI", "OSSERVARE TAVOLINO", "OSSERVARE PARETE", "PARLARE SHAX", "ANDARE NORD", "OSSERVARE CORRIDOIO");
		if(!st.m1v){
			ambiente = false;
			if(!narrazione(Narrazione.n0)) return;
			event = 0; // evento 0
			statement = 0;
			allow("A", "B");
			ambiente=true;
		} else if(st.m1evasione && !st.m1cristallodato && !st.m1shax2){
			ambiente = false;
			done=false;
			if(!narrazione(Narrazione.n11)) return;
			
			event=4; //evento 4
			statement=0;
			allow("A", "B", "C");
			ambiente=true;
			return;
		} else if(st.m1pronto && !st.m1evaso){
			ambiente=false;
			done=false;
			if(!narrazione(Narrazione.n12)) return;
			printa(Narrazione.n13);
			protagonista.addStuff(new ArmaturaDaGuardiaCarceraria());
			protagonista.addStuff(new SpadaCorta());
			protagonista.addStuff(new PozioneCurativa());
			st.m1evaso=true;
			ambiente=true;
			return;
		} else if(st.m1fuori && !st.m1scontro){
			ambiente=false;
			done=false;
			if(!narrazione(Narrazione.n16)) return;
			event=6; //evento 6
			statement=0;
			allow("A","B");	
			ambiente=true;
		} else if(st.m1scontro && !st.m1guardiauccisa){	
			setAvversario(new Guardia());
			lotta = new Lotta(true); //inizializzo la classe lotta
			Game.battleState = new BattleState();
			Game.gameState.setState(Game.battleState); //cambio scenario grafico
			lotta.start(); //start della lotta
			Assets.mute();
			Assets.battleTheme.start(); //start audio battaglia
			st.m1guardiauccisa=true;
			return;
		} else if(st.m1guardiauccisa){
			ambiente=false;
			done=false;
			if(!narrazione(Narrazione.n17)) return;
			Game.scenaFinale = new ScenaFinale();
			State.getState().setState(Game.scenaFinale);
			Assets.mute();
			Assets.dialogoFinale.start();
			Game.display.panel.setVisible(false);
			Game.gameState = null;
		} else {
			done=false;
			if(!narrazione(Narrazione.n1)) return;
		}
		if(st.m1misterioso) Mappa1Dialoghi.mp[1][1][0][0] = "Shax: \"Ho capito...\nSai la prigione e' un luogo che ti fa apprezzare la compagnia.\"\n\n\nA) ...\nB) Che vuoi?";
		 //per ogni mappa, puoi aggiornare l'avversario, ad ogni punto della storia
	}
	/**
	 * In base all'ultimo comando che hai submittato, tramite uno switch viene eseguita una determinata azione.
	 */
	public void eseguiComandi() {
		switch(cmdCorrente.toUpperCase()) {
			case "PARLARE SHAX":
				parlareShax();
				break;
			case "OSSERVARE SHAX":
				osservareShax();
				break;
			case "OSSERVARE SECCHIO":
				osservareSecchio();
				break;
			case "OSSERVARE LETTI":
				osservareLetti();
				break;
			case "USARE LETTI":
				usareLetti();
				break;
			case "OSSERVARE TAVOLINO":
				osservareTavolino();
				break;
			case "OSSERVARE PARETE":
				osservareParete();
				break;
			case "ANDARE NORD":
				andareNord();
				break;
			case "OSSERVARE CORRIDOIO":
				osservareCorridoio();
				break;
			case "A":
				setBoolean(event, statement, 0);
				printa(dialoghi.getRisposta(event, statement, 0));
				temp = event;
				event = dialoghi.getNextEvent(event, statement, 0);
				statement = dialoghi.getNextStatement(temp, statement, 0);
				setAllow(event, statement);
				break;
			case "B":
				setBoolean(event, statement, 1);
				printa(dialoghi.getRisposta(event, statement, 1));
				temp = event;
				event = dialoghi.getNextEvent(event, statement, 1);
				statement = dialoghi.getNextStatement(temp, statement, 1);
				setAllow(event, statement);
				break;
			case "C":
				setBoolean(event, statement, 2);
				printa(dialoghi.getRisposta(event, statement, 2));
				temp = event;
				event = dialoghi.getNextEvent(event, statement, 2);
				statement = dialoghi.getNextStatement(temp, statement, 2);
				setAllow(event, statement);
				break;
			case "D":
				setBoolean(event, statement, 3);
				printa(dialoghi.getRisposta(event, statement, 3));
				temp = event;
				event = dialoghi.getNextEvent(event, statement, 3);
				statement = dialoghi.getNextStatement(temp, statement, 3);
				setAllow(event, statement);
				break;
			case "E":
				setBoolean(event, statement, 4);
				printa(dialoghi.getRisposta(event, statement, 4));
				temp = event;
				event = dialoghi.getNextEvent(event, statement, 4);
				statement = dialoghi.getNextStatement(temp, statement, 4);
				setAllow(event, statement);
				break;
		}
	}
	

	private void osservareCorridoio() {
		if(!st.m1fuori){
			if(st.m1cellaovest){
				printa(Narrazione.n7);
		
			}
		}
	}
	private void andareNord() {
		if(!st.m1fuori){
			if(st.m1evaso){
				if(protagonista.getArmatura() instanceof ArmaturaDaGuardiaCarceraria){
					printa(Narrazione.n14);
					event=7;
					statement=0;
					allow("A");
					st.m1fuori=true;
				}
				else{
					printa(Narrazione.n15);
				}
			}
			else{
				printa(Narrazione.n6);
				st.m1cellaovest=true;
			}
		}
	}
	private void osservareParete() {
		if(!st.m1fuori){
			printa(Narrazione.n5);
		}
	}
	private void osservareTavolino() {
		if(!st.m1fuori){
			printa(Narrazione.n4);
		}
	}
	private void usareLetti() {
		if(!st.m1fuori){
			if(!st.m1shax){
			printa(Narrazione.n9);
			event=2;
			statement=0;
			allow("A", "B"); //evento 2
			}
			else{
			printa(Narrazione.n10);
			}
		}
	}
	private void osservareLetti() {
		if(!st.m1fuori){
			printa(Narrazione.n3);
		}
	}	
	private void osservareSecchio() {
		if(!st.m1fuori){
			printa(Narrazione.n2);
		}
	}
	private void parlareShax() {
		if(!st.m1fuori){
			if(!st.m1shax){
			//evento 1
			printa(Shax.shax2);
			event = 1;
			statement = 0;
			allow("A", "B");
			return;
			}
			if(!st.m1evasione){
				//evento 3
				printa(Shax.shax3);
				event = 3;
				statement= 0;
				allow("A", "B", "C", "D");
				return;
			}
			if(!st.m1cristallodato){
				printa(Shax.shax4);
				event= 4;
				statement=5;
				allow("A","B");
				return;
			}
			if(!st.m1pronto){
				printa(Shax.shax5);
				event=5; //evento 5
				statement=0;
				allow("A", "B");
				return;
			}
		}
	}
	
	private void osservareShax() {
		if(!st.m1fuori){
			printa(Shax.shax1);
		}
	}
	/**
	 * Cio' che accade quando ritorni dalla {@code Lotta}, che tu sia vittorioso o meno.
	 */
	public void returnBattle() {
		if(lotta.getGameover()) {
			Assets.mute();
			Game.menuState = new MenuState();
			Game.gameState.setState(Game.menuState);
			Game.display.panel.setVisible(false);
			Game.gameState = null;
		} else {
			lotta = null;
		}
	}
	
	//setta gli allow in base all'evento e lo statement successivo
	private void setAllow(int x, int y) {
		if(x==0 && y==0) {
			allow(comandiVerbali);
			play();
			return;
		}
		int num = dialoghi.numRisposte(x, y);
		switch(num) {
			case 1:
				allow("A");
				break;
			case 2:
				allow("A", "B");
				break;
			case 3:
				allow("A", "B", "C");
				break;
			case 4:
				allow("A", "B", "C", "D");
				break;
			case 5:
				allow("A", "B", "C", "D", "E");
				break;
		}
	}
	
	private void setBoolean(int event, int statement, int answer){
		if(event==0 && statement==0 && answer==1) {st.m1misterioso=true;} 
		if(event==0 && statement==1 &&answer==1) {st.m1misterioso=true;}
		if(event==0 && statement==2 &&answer==0) {st.m1v=true; protagonista.setExp(protagonista.getExp()+10);}
		if(event==1 && statement==3 &&answer==0) {st.m1shax=true;}
		if(event==1 && statement==5 &&answer==0) {st.m1shax=true;}
		if(event==1 && statement==7 &&answer==0) {st.m1shax=true;}
		if(event==1 && statement==11 &&answer==0) {st.m1shax=true;}
		if(event==3 && statement==4 && answer==1) {st.m1evasione=true;}
		if(event==4 && statement==5 && answer==0) {st.m1shax2=true;}
		if(event==4 && statement==6 && answer==0) {st.m1cristallodato=true; protagonista.getInventario().remove(0);}
		if(event==4 && statement==7 && answer==0) {st.m1cristallodato=true; protagonista.getInventario().remove(0);}
		if(event==5 && statement==0 && answer==0) {st.m1pronto=true;}
		if(event==6 && statement==0 && answer==0) {st.m1scontro=true;}
		if(event==6 && statement==2 && answer==0) {st.m1scontro=true;}
	}
}
