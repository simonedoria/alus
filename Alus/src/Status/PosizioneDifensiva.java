package Status;

import Alus.Game;
import Personaggi.Personaggio;
import System.RoutineSystem;

/**
 * Questa classe definisce lo Status {@code Posizione Difensiva}. Eredita e ne concretizza
 * metodi e attributi dichiarati nella classe {@code Status}.
 *
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 */
public final class PosizioneDifensiva extends Status{

	/**
	 * Costruttore.
	 * Implementa il valore delle variabili d'istanza dell'oggetto {@code this}.
	 * Alcune di esse utilizzano alcuni attributi della {@code Personaggio} passato come parametro.
	 * @param p Vengono utilizzati i suoi attributi per implementare gli attributi di {@code this}.
	 */
	
	public PosizioneDifensiva(Personaggio p){
		nome="Posizione difensiva";
		sigla="DEF";
		inizio= p.getNome()+" si mette in posizione difensiva!";
		inCorso= "";
		fine= p.getNome()+" non e' piu' in posizione difensiva!";
		timingMax=1;
		timing=timingMax;
	}
	
	/**
	 * {@inheritDoc} 
	 * 
	 * Esegue un controllo sul timing attualle di {@code this}:
	 * <ul>
	 * <li>se timing=timingMax stampa a video la strtinga {@code inizio}, esegue l'effetto di {@code this}
	 * sottrae 1 a {@code timing}.
	 * <li> Annulla l'effetto di {@code this} e si auto-rimuove da {@code p}.
	 * </ul>
	 * @param p {@code Personaggio} sul quale evengono eseguiti gli effetti di {@code this}.
	 * 
	 * 
	 */
	public void esegui(Personaggio p){
		if(timing==1){ 
			p.setBonusdif(p.getBonusdif()+3);
			p.setBonusdes(p.getBonusdes()+3);
			Game.m.lotta.setTestoCentrale(inizio);
			RoutineSystem.dormi(1000);
			timing--;
		}	
		else{
			p.setBonusdif(p.getBonusdif()-3);
			p.setBonusdes(p.getBonusdes()-3);
			Game.m.lotta.setTestoCentrale(fine);
			RoutineSystem.dormi(1000);
			p.removeStatus(this);
		}
	}
	
	/**
	 * Resetta il timing di {@code this}. DI fatto pone timing=timingMax.
	 */
	public void resetTiming(){
		timing=timingMax;
	}

}
