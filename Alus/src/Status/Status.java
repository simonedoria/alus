package Status;

import Personaggi.Personaggio;

/**
 * Questa classe astratta definisce la stuttura e le caratteristiche che un oggetto di tipo
 * {@code Status} deve avere.
 * <p>
 * Uno <b>{@code Status}</b> e' un oggetto che aggiunto tramite metodi opportuni ad un generico {@code Personaggio p}
 * modifica opportunamente i valori delle variabili d'istanza di {@code p}.
 * <p>
 * Un oggetto {@code Status} possiede le relative caratteristiche:
 * <ul>
 * <li> {@code nome};
 * <li> {@code sigla};
 * <li> tre stringhe che vengono utilizzate come messaggio per l'utente {@code inizio}, {@code inCorso} e {@code fine};
 * <li> due valori interi che ne definiscono il "timing" che sono appunto {@code timing} e {@code timingMax}.
 * </ul>
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 * @see Personaggi.Personaggio
 * @see Personaggi.Personaggio#addStatus(Status)
 *
 */
public abstract class Status {
	
	
	protected String nome;
	
	
	protected String sigla;
	
	
	protected String inizio;
	
	
	protected String inCorso;
	
	
	protected String fine; 
	
	
	protected String slug;
	
	
	
	protected int timing;
	
	
	protected int timingMax;

	/**
	 * Ritorna sotto forma di stringa il valore della variabile {@code inizio} .
	 * @return Il valore della variabile {@code inizio}.
	 * @see #inizio
	 */
	public String getInizio() {
		return inizio;
	}
	
	/**
	 * Modifica la variabile {@code inizio} con la stringa passatogli come parametro.
	 * @param Inizio nuovo valore della variabili {@code inizio}.
	 * @see #inizio
	 */
	public void setInizio(String inizio) {
		this.inizio = inizio;
	}
	
	

	/**
	 * Ritorna sotto forma di stringa il valore della variabile {@code inCorso}.
	 * @return Il valore della variabile {@code in Corso}.
	 * @see #inCorso.
	 */
	public String getInCorso() {
		return inCorso;
	}
	
	/**
	 * Modifica il valore della variabile {@code inCorso} con quello passatogli come parametro.
	 * @param inCorso Nuovo valore della variabile {@code inCorso}.
	 * @see #inCorso
	 */
	public void setInCorso(String inCorso) {
		this.inCorso = inCorso;
	}
	
	/**
	 * Ritorna sotto forma di stringa il valore della variabile {@code fine}.
	 * @return Il valore della variabile {@code fine}.
	 * @see #fine.
	 */
	public String getFine() {
		return fine;
	}
	
	/**
	 * Modifica il valore della variabile {@code fine} col valore passatogli come parametro. 
	 * @param fine Nuovo valore della variabile {@code fine}.
	 * @see #fine
	 */

	public void setFine(String fine) {
		this.fine = fine;
	}
	
	/**
	 * Ritorna l'intero corrispsondente al valore della variabile {@code timingMax}.
	 * @return Il valore della variabile {@code timingMax}.
	 * @see #timingMax
	 */
	public int getTimingMax() {
		return timingMax;
	}
	
	/**
	 * Ritorna sottofoma di stringa il valore della variabile {@code sigla} con i caratteri tutti in <br>maiuscolo</br>
	 * sfruttando il metodo della classe {@code String} {@linkplain String#toUpperCase()}
	 * @return Il valore della variabile {@code sigla}.
	 * @see #sigla
	 * @see
	 */
	public String getSigla(){
		return sigla.toUpperCase();
	}
	
	/**
	 * Ritorna sottoforma di stringa il valore della variabila {@code slug}.
	 * @return Il valore della variabile {@code slug}
	 */
	public String getSlug() {
		return slug;
	}
	
	/**
	 * Ritorna come stringa il valore della variabile {@code nome}.
	 * @return Il valore della variabile {@code nome}.
	 * @see #nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Modifica il valore della variabile {@code nome} col valore passatogli come parametro. 
	 * @param fine Nuovo valore della variabile {@code nome}.
	 * @see #nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Modifica il valore della variabile {@code slug} col nuovo valore passato
	 * come parametro
	 * @param slug Il nuovo valore della variabile {@code slug}
	 */
	public void setSlug(String slug) {
		this.slug = slug;
	}
	
	/**
	 * Ritorna l'intero relativo al vaore della variabile {@code timing}.
	 * @return Il valore della variabile {@code timing}.
	 * @see #timing
	 */
	public int getTiming() {
		return timing;
	}
	
	/**
	 * Modifica il valore della variabile {@code timing} col nuovo valore passato come parametro.
	 * @param timing Nuovo valore della variabile {@code timing}.
	 * @see #timing
	 */
	public void setTiming(int timing) {
		this.timing = timing;
	}
	
	/**
	 * Applica le modifiche che caratterizzano {@code this} al {@code Personaggio}
	 * passato come parametro.
	 * E' dichiarato astratto, ed è astratto, perche' deve essere sovrasvritto da tutte le 
	 * classi concrete che estendono la classe {@link Status.Status}.
	 * In questo metodo e' racchiusa la logica che veramente caratterizza uno {@code Status}
	 * e lo diversifica da qualunque altro.
	 * @param p {@code Personggio} sul quale verra' applicato l'effetto dell'oggetto {@code this}.
	 */
	public abstract void esegui(Personaggio p);
	
	
	/**
	 * Resetta il {@link #timing} dell'oggetto {@code this} al timing di partenza. 
	 * Questo metodo chiamato ogni qual volta che uno {@code Status} gia attivo su un
	 * certo {@code Personaggio p} viene riattivato. Evita di creare un oggetto {@code Status}
	 * identico semplicemente resettandone il timing.
	 */
	public abstract void resetTiming();
	
}
