package Status;

import Alus.Game;
import Personaggi.Personaggio;
import System.RoutineSystem;

/**
 *
 * Questa classe definisce lo Status {@code Sanguinante}. Eredita e ne concretizza
 *  metodi e attributi dichiarati nella classe {@code Status}.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class Sanguinante extends Status {
	
	
	
	/**
	 * Costruttore.
	 * Implementa il valore delle variabili d'istanza dell'oggetto {@code this}.
	 * Alcune di esse utilizzano alcuni attributi della {@code Personaggio} passato come parametro.
	 * @param p Vengono utilizzati i suoi attributi per implementare gli attributi di {@code this}.
	 */
	public Sanguinante(Personaggio p){
		nome="Sanguinante";
		sigla="SNG";
		inizio= p.getNome()+" inizia a sanguinare!";
		inCorso= p.getNome()+" sanguina!";
		fine= p.getNome()+" non sanguina piu'!";
		timingMax=4;
		timing=timingMax;
	}
	
	/**
	 * {@inheritDoc} 
	 * 
	 * Esegue un controllo sul timing attualle di {@code this}:
	 * <ul>
	 * <li>se timing=timingMax stampa a video la strtinga {@code inizio};
	 * <li>se timing<=timingMax e timing>=1 esegue l'effetto di {@code this} e sottrae 1 a {@code timing}.
	 * <li>se timing=1 stampa a video la stinga {@code fine} e si auto-rimuove dalla lista degli {@code Status} attivi su {@code p}.
	 * </ul>
	 * @param p {@code Personaggio} sul quale evengono eseguiti gli effetti di {@code this}.
	 * 
	 * 
	 */
	public void esegui(Personaggio p){
		if(timing==4) Game.m.lotta.setTestoCentrale(inizio);	
		if(timing<=4 && timing>=1){
			long dannos = Math.round(p.getPvMax()*0.05);
			Game.m.lotta.setTestoCentrale(p.getNome() + " subisce " + dannos + " danni da sanguinamento!");
			RoutineSystem.dormi(1000);
			p.setPv(p.getPv()-dannos);
			Game.m.lotta.setTestoCentrale(inCorso);
			RoutineSystem.dormi(1000);
			timing--;
		}
		else{
			Game.m.lotta.setTestoCentrale(fine);
			RoutineSystem.dormi(1000);
			p.removeStatus(this);
		}
	}
	
	/**
	 * Resetta il timing di {@code this}. DI fatto pone timing=timingMax.
	 */
	public void resetTiming(){
		timing=timingMax;
	}


}
