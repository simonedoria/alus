package Abilita;

import Personaggi.Personaggio;

/** Definisce la struttura che un oggetto {@code Abilita} deve avere.
 * <p>
 * 
 * Ogni {@code Abilita} creata verra' aggiunta nella lista delle abilita' della classe {@code Personaggio}. 
 *
 * Essa definisce tutti gli attributi che un oggetto {@code Abilita} deve avere.
 * <br>
 * Gli attributi sono:
 * <ul>
 * <li>il nome dell'abilita';
 * <li>la descrizione dell'abilita';
 * <li>il costo di Punti Mana.
 * </ul>
 * <p>
 * Ogni oggetto {@code Abilita} deve essere aggiunto ad un personaggio tramite il metodo 
 * {@code addAbilita(Abilita)} della classe {@link Personaggi.Personaggio}.
 * L'effetto di un'abilita' puo' prevedere che venga aggiunto uno {@code Status} al {@codePersonaggio}
 * sul quale essa viene eseguita.
 * <p>
 * La logica del gioco prevede che le istanze di questa classe vengano utilizzate 
 * sono nella maniera descritta sopra.
 * 
 * 
 *
 *   
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 * @see Personaggi.Personaggio
 * @see Personaggi.Personaggio#addAbilita(Abilita)
 *
 */
public abstract class Abilita {
	
	protected String nome;
	
	
	protected String sigla;
	
	
	protected String slug;
	
	
	protected String descrizione;
	
	
	protected int costoPm;
	
	/**
	 * Ritorna iil nome dell'abilita' come stringa.
	 * 
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Ritorna la sigla dell'abilita' come stringa.
	 */
	public String getSigla() {
		return sigla;
	}
	
	/**
	 * Ritorna lo "slug" dell'abilita.
	 */
	public String getSlug() {
		return slug;
	}
	
	/**
	 * Modifica il nome di {@code this} con la stringa passata come parametro.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Modifica la sigla di {@code this} con la stringa passata come parametro.
	 */
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	/**
	 * Modifica lo "slug" di {@code this} con la stringa passata come parametro.
	 */
	public void setSlug(String slug) {
		this.slug = slug;
	}
	
	/**
	 * Ritorna la stringa corrispondente alla descrizione dell'abilita.
	 */
	public String getDescrizione() {
		return descrizione;
	} 
	
	/**
	 * Modifica la descrizione dell'abilita con la stringa passata come parametro.
	 */
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	/**
	 * Ritorna un intero il cui valore corrisponde al costo in <b>Punti Mana</b> per 
	 * il {@code Personaggio} che segue l'abilita {@code this}. 
	 */
	public int getCostoPm() {
		return costoPm;
	}
	
	/**
	 * Modifica il valore corripsondente  al costo in <b>Punti Mana</b> che il 
	 * {@code Personaggio} deve pagare per eseguire l'abilita.
	 */
	public void setCostoPm(int costoPm) {
		this.costoPm = costoPm;
	}
	
	/**
	 * Esegue l'effetto dell'istanza {@code Abilita} sulla quale � invocato.
	 * <br>L'effetto viene eseguito <b>solo</b> se il {@code Personaggio} che possiede ed esegue l'abilita ha
	 * una quantita' di <b>Punti Mana</b> maggiore o uguale al valore di {@link #getCostoPm()} dell'oggetto {@code this}.
	 * @param p1 Oggetto {@code Personaggio} che possiede ed esegue l'abilita' sulla quale viene incocato il metodo.
	 * @param p2 Oggetto {@code Personaggio} che subisce l'effetto dell'abilita sulla quale viene invocato il metodo.
	 * @return {@code true} se l'effetto dell'abilita e' eseguito, {@code false} altrimenti.
	 */
	public abstract boolean attiva(Personaggio p1, Personaggio p2);
}
