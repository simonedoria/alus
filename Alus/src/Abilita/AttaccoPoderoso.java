package Abilita;

import Alus.Game;
import Combattimento.RoutineCombattimento;
import Personaggi.Personaggio;
import System.RoutineSystem;

/**
 * La classe {@code AttaccoPoderoso} estende la classe {@code Abilita} e ne concretizza il metodo 
 * {@code attiva(Personaggio, Personaggio)}
 * 
 * Questa classa non puo' essere estesa.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 */
public final class AttaccoPoderoso extends Abilita{
	
	/**
	 * Costruttore.
	 * <br> Inizializza il nuovo oggetto {@code AttaccoPoderoso} con valori predefiniti e <b>NON MODIFICABILI</b>.
	 */
	public AttaccoPoderoso() {
		
		sigla="Att. Poderoso";
		nome="Attacco Poderoso";
		descrizione="Sacrificando precisione e stabilita', il guerriero si lancia in una carica travolgente.";
		costoPm=6;
		slug="apoderoso";
	}
	
	@Override
	/**
	 * 
	 */
	public boolean attiva(Personaggio p1, Personaggio p2){
		if(p1.getPm()<costoPm) return false;
		p1.setPm(p1.getPm() - costoPm);
		RoutineCombattimento.waitDots();
		Game.m.lotta.setTestoCentrale(p1.getNome() + " usa " + nome);
		RoutineSystem.dormi(1000);
		if(precisione(p1,p2)){
			double danno= danno(p1,p2);
			danno = RoutineCombattimento.correzioneDanno(danno, 25);
			Game.m.lotta.setTestoCentrale(p1.getNome() + " ha inflitto " + RoutineSystem.rimuoviZero(danno) + " danni a " + p2.getNome() );
			p2.setPv(p2.getPv()-danno);
			RoutineSystem.dormi(1000);
		}
		return true;
	}
	
	private boolean precisione(Personaggio p1, Personaggio p2){
		if(RoutineSystem.prob(20-(p1.getForz()+p1.getBonusforz())/2+(p2.getDes()+p2.getBonusdes()))) {
			RoutineCombattimento.waitDots();
			Game.m.lotta.setTestoCentrale(p2.getNome()+" ha schivato l'attacco!");
			RoutineSystem.dormi(1000);
			return false;
		}
		return true;
	}
	
	private double danno(Personaggio p1, Personaggio p2){
		double danno;
		if(RoutineCombattimento.critico2(p1, (p1.getCrit()+4))) {
			danno=Math.round(((p1.getForz()+p1.getBonusforz())*3-p2.getDif()-p2.getBonusdif())*p1.getDcrit());
		}
		else danno=Math.round((p1.getForz()+(p1.getBonusforz())*3)-p2.getDif()-p2.getBonusdif());
		return danno;
	}

}

