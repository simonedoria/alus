package Abilita;

import Alus.Game;
import Combattimento.RoutineCombattimento;
import Personaggi.Personaggio;
import Status.Sanguinante;
import System.RoutineSystem;

/**
 * La classe <tt>AttaccoFurtivo</tt> estende la classe {@link Abilita} e ne concretizza il metodo {@link #attiva(Personaggio, Personaggio)}.
 *
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga 
 * @version 1.0
 */
public final class AttaccoFurtivo extends Abilita {
	
	/**
	 *
	 *Costruttore.
	 * <br> Inizializza il nuovo oggetto {@code AttaccoFurtivo} con valori predefiniti e <b>NON MODIFICABILI</b>.
	 *
	 */
	public AttaccoFurtivo() {
		
		nome = "Attacco furtivo";
		descrizione = "Nascosto nell'ombra, l'assassino colpisce i suoi avversari alle spalle."
					+"\nChi usa questa abilita' trae molto vanaggio dalla propria DES e puo' aprire profonde ferite ai propri nemici.";
		costoPm=10;
		sigla="A. Furtivo";
		slug="afurtivo";
		
	}
	
	/**
	 * {@inheritDoc}
	 
	 * 
	 *  
	 *@see Sanguinante
	 *@see RoutineSystem#prob(double)
	 */
	public boolean attiva(Personaggio p1, Personaggio p2){
		if(p1.getPm()<costoPm) return false;
		p1.setPm(p1.getPm() - costoPm);
		RoutineCombattimento.waitDots();
		Game.m.lotta.setTestoCentrale(p1.getNome() + " usa " + nome);
		RoutineSystem.dormi(1000);
		if(precisione(p1,p2)){
			if(RoutineSystem.prob(60)){
				p2.addStatus(new Sanguinante(p2));
			}
			double danno= danno(p1,p2);
			danno = RoutineCombattimento.correzioneDanno(danno, 10);
			Game.m.lotta.setTestoCentrale(p1.getNome() + " ha inflitto " + RoutineSystem.rimuoviZero(danno) + " danni a " + p2.getNome() );
			p2.setPv(p2.getPv()-danno);
			RoutineSystem.dormi(1000);
		}
		return true;
	}
	
	
	private boolean precisione(Personaggio p1, Personaggio p2){
		if(RoutineSystem.prob(30-(p1.getDes()+p1.getBonusdes())/2+(p2.getDes()+p2.getBonusdes()))) {
			RoutineCombattimento.waitDots();
			Game.m.lotta.setTestoCentrale(p2.getNome()+" ha schivato l'attacco!");
			RoutineSystem.dormi(1000);
			return false;
		}
		return true;
	}
	
	
	private double danno(Personaggio p1, Personaggio p2){
		double danno;
		if(RoutineCombattimento.critico2(p1, (p1.getCrit()+10))) {
			danno=Math.round(((p1.getDes()+p1.getBonusdes())*2-p2.getBonusdif()-p2.getDif())*p1.getDcrit());
		}
		else danno=Math.round((p1.getForz()+p1.getBonusforz())*2-p2.getDif()-p2.getBonusdif());
		return danno;
	}

}

//if(RoutineCombattimento.critico2(p1, (p1.getCrit()+10)))