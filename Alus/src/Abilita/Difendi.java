package Abilita;

import Alus.Game;
import Combattimento.RoutineCombattimento;
import Personaggi.Personaggio;
import Status.PosizioneDifensiva;
import System.RoutineSystem;

/**
 * La classe {@code Difendi} estende la classe {@code Abilita} e ne concretizza il metodo 
 * {@code attiva(Personaggio, Personaggio)}
 * 
 * Questa classa non puo' essere estesa.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 */
public class Difendi extends Abilita{
	
	/**
	 * Costruttore.
	 * <br> Inizializza il nuovo oggetto {@code AttaccoPoderoso} con valori predefiniti e <b>NON MODIFICABILI</b>.
	 */
	public Difendi() {
		
		sigla="Difendi";
		nome="Difendi";
		descrizione="Arretri leggermente non attaccando, in modo da prepararti a shivare il prossimo attacco nemico, o per lo meno limitarne gli effetti.";
		slug="difendi";
	}

	@Override
	/**
	 * 
	 */
	public boolean attiva(Personaggio p1, Personaggio p2) {
		
		if(p1.getPm()<costoPm) return false;
		p1.setPm(p1.getPm() - costoPm);
		RoutineCombattimento.waitDots();
		Game.m.lotta.setTestoCentrale(p1.getNome() + " usa " + nome);
		RoutineSystem.dormi(1000);
		p1.addStatus(new PosizioneDifensiva(p1));
		return true;
	}

}
