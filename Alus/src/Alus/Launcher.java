package Alus;

/**
 * Contiene il metodo che fa partire il {@code Thread} del gioco.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */

public class Launcher {
	public static Game game;
	public static void main(String [] args) {
		game = new Game();
		game.start();
	}
}
