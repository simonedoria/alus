package Alus;

import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class Element {
	private int posXa;
	private int posXb;
	private int posYa;
	private int posYb;
	private int width;
	private int height;
	private BufferedImage image;
	public Element(BufferedImage image, int posXa, int posXb, int posYa, int posYb, int width, int height) {
		this.image = image;
		this.posXa = posXa;
		this.posXb = posXb;
		this.posYa = posYa;
		this.posYb = posYb;
		this.width = width;
		this.height = height;
	}
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	public int getPosXa() {
		return posXa;
	}
	public int getPosXb() {
		return posXb;
	}
	public int getPosYa() {
		return posYa;
	}
	public int getPosYb() {
		return posYb;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	
	public boolean isDetected(MouseEvent e) {
		if(e.getX()>posXa && e.getX()<posXb && e.getY()>posYa && e.getY()<posYb) return true;
		else return false;
	}
}
