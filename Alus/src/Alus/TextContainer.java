package Alus;

import java.awt.*;
import java.awt.font.*;
import java.text.*;

/**
 * Questa classe permette di creare dei box di testo con coordinate, dimensioni, {@code Font} e {@code Color} personalizzabili.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */

public class TextContainer extends Panel {
    
	private static final long serialVersionUID = 1L;
	private int m_width;
    private String m_text;
    private float x, y;
    private Color color;
    private Font font;
    
    public TextContainer(String text, float xx, float yy, int width, int height, Font fontt, Color colorr) {
    	m_text = text;
        m_width = width;
        x = (int) xx;
        y = (int) yy;
        color = colorr;
        font = fontt;    
    }
    
    private int larg(int larg) {
    	int width = (larg*980)/587;
    	return width;
    }
    /**
	 * Passando al metodo paint un oggetto di tipo {@code Graphics} esso disegna la stringa richiamando il metodo privato paintLines.<br>
	 * Quest'ultimo prende come parametri: la conversione di Graphics in {@code Graphics2D} una {@code String} che rappresenta il testo da printare,<br>
	 * la larghezza ipotetica della linea, e le coordinate. Infine il {@code Font} già compreso del colore.<br>
	 * Attraverso lo split del testo in multistringhe nel caso in cui ci fossero "line breaker", viene gestito anche l'andare a capo.
	 */
    public void paint(Graphics g) {
        super.paintComponents(g);

        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(color);
        paintLines(g2, m_text, larg(m_width), x, y, font);
    }
    
    private void paintLines(Graphics2D g, String text, int lineWidth, float x2, float y2, Font font) {

        String[] lines = text.split("\\n");

        Point cursor = new Point((int)x2, (int)y2);
        
        final FontMetrics metrics = g.getFontMetrics();
        int lineHeight = metrics.getAscent() + metrics.getDescent() + metrics.getLeading();

        FontRenderContext frc = g.getFontRenderContext();

        for (int i = 0; i < lines.length; i++) {
            final String line = lines[i];

            if ("".equals(line)) {
                cursor.y += lineHeight;
            } else {
                AttributedString styledText = new AttributedString(line);
                styledText.addAttribute(TextAttribute.FONT, font);
                LineBreakMeasurer measurer = new LineBreakMeasurer(styledText.getIterator(), frc);

                while (measurer.getPosition() < line.length()) {
                	
                	int layoutW;
                	if(lineWidth < x2) layoutW = lineWidth;
                	else layoutW = (int) (lineWidth - x2);
                    TextLayout layout = measurer.nextLayout(layoutW);

                    cursor.y += (layout.getAscent());
                    float dx = layout.isLeftToRight() ? 0 : (lineWidth - layout.getAdvance());

                    layout.draw(g, cursor.x + dx, cursor.y);
                    cursor.y += layout.getDescent() + layout.getLeading();
                }
            }
        }

    }
    
}