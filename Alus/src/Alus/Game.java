package Alus;

import java.awt.*;
import java.awt.image.BufferStrategy;

import Mappe.Mappa;
import States.*;

/**
 * Questa classe e' il motore centrale del gioco.
 * <br>I metodi di questa classe inizializzano il frame tramite la classe {@link Display},
 * creano l'oggetto {@code Mappa} e disegnano sul frame le rrisorse grafiche richieste 
 * dallo {@code State} in utilizzo.
 * <p>
 * Implementa l'interfaccia {@code Runneble} per utilizzare i Thread.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 * @see Runnable
 *
 */
public class Game implements Runnable {
	
	/**
	 * L'interfaccia grafica da visualizzare.
	 */
	public static Display display;
	public int width, height;
	
	/**
	 * Mappa attuale di riferimento.
	 */
	public static Mappa m;

	private Toolkit tk = Toolkit.getDefaultToolkit();
	
	private boolean running = false;
	private Thread thread;
	
	private BufferStrategy bs;
	private Graphics g;
	
	//States
	/**
	 * Uno degli {@code State} del gioco. 
	 */
	public static State gameState, battleState, menuState, scenaIniziale, creditiState, scenaFinale;
	public Game() {
		this.width = ((int) tk.getScreenSize().getWidth());
		this.height = ((int) tk.getScreenSize().getHeight());
		m = null;
	}
	
	private void init() {
		Assets.init();
		display = new Display();
		menuState = new MenuState();
		menuState.setState(menuState);
	}
	
	private void render() {
		bs = display.getCanvas().getBufferStrategy();
		if(bs == null) {
			display.getCanvas().createBufferStrategy(10);
			return;
		}
		g = bs.getDrawGraphics();
		//Clear Screen
		g.clearRect(0, 0, width, height);
		//Draw here!
		
		
		if(State.getState() != null) {
			State.getState().render(g);
		}
		
		
		//End Drawing!
		bs.show();
		g.dispose();
	}
	
	//disegna tutto cio che lo state attuale richiede a 60 frame/s
	// si occupa di cgiudere il programma richiamando un metodo apposito privato
	/**
	 * Disegna sullo schermo gli elementi richiesti dallo {@code State} attuale.
	 * Si occupa ionoltre di terminare il programma utilizzando un apposito metodo 
	 * di supporto dichiarato privato.
	 */
	/*public void run() {
		init();
		int fps = 60;
		double timePerTick = 1000000000 / fps;
		double delta = 0;
		long now;
		long lastTime = System.nanoTime();
		long timer = 0;
		while(running) {
			now = System.nanoTime();
			delta += (now - lastTime) / timePerTick;
			timer += now - lastTime;
			lastTime = now;
			if(delta >= 1) {
				render();
				delta--;
			}
			if(timer >= 1000000000) timer = 0;
		}
		
		stop();
	}*/
	public void run(){
		init();
	    int fps = 30;
	    /*double delay = 1000 / fps;
	    int ticks = 0;
	    while(true){
	        delay(ticks);
	        render();
	        ++ticks;
	    }*/
		long lastTick = System.currentTimeMillis();
		long ticksPerFrame = 1000 / fps;
		while (true) {
		    long nextTick = lastTick + ticksPerFrame;
		    long sleep = nextTick - System.currentTimeMillis();
		    if (sleep > 0) {
		        try {
		            Thread.sleep(sleep);
		        } catch (InterruptedException e) {
		            throw new RuntimeException(e.getMessage(), e);
		        }
		    }
		    lastTick = nextTick;
		    render();
		}
	}
	
	public void delay(int mil){
	    try{
	        Thread.sleep(mil);
	    }catch(Exception e){

	    }
	}
	
	//crea un nuovo tread e lo esegue
	/**
	 * Lancia il programma. Crea un nuovo thread passandogli come parametro 
	 * l'oggetto {@code this} e lo esegue. Cio' permette di eseguire il metodo {@link #run()}.
	 * 
	 */
	public synchronized void start() {
		if(running) return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	
	private synchronized void stop() {
		if(!running) return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}