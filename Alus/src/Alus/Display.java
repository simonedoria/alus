package Alus;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JTextField;

import Assets.Listeners.CustomMouseListener;
import System.TransparentPanel;
import System.TransparentTextField;

/**
 * In questa classe viene generato il frame del gioco. Essa estende {@code JFrame} e ne personalizza l'utilizzo.
 * Nessun altro elemento di tipo Component viene utilizzato in quanto viene tutto disegnato mediante Graphics,
 * eccetto che per un pannello ed un textBox inizializzato e mostrato quando serve.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */

public class Display extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Risorsa di tipo {@code Canvas}.
	 */
	public static Canvas canvas;
	
	//settaggi per dimensione monitor
	/**
	 * Variabile privata intera che rappresenta la larghezza dello schermo.
	 */
	private int width;
	/**
	 * Variabile privata intera che rappresenta la lunghezza dello schermo.
	 */
	private int height;
	/**
	 * Variabile privata di tipo {@code Toolkit} che serve per ottenere le dimensioni dello schermo.
	 */
	private Toolkit tk = Toolkit.getDefaultToolkit();
	
	//settaggi per cursore
	/**
	 * Variabile pubblica di tipo {@code TransparentPanel} che è un'estensione di {@code JPanel}.
	 */
	public TransparentPanel panel = new TransparentPanel();
	/**
	 * Variabile pubblica di tipo {@code TransparentTextField} che è un'estensione di {@code JTextField}.
	 */
	public TransparentTextField textbox = new TransparentTextField("");
	/**
	 * Costruttore della classe che ottiene le coordinate dello schermo ospitante e crea il frame su misura.
	 */
	public Display() {
		this.width = ((int) tk.getScreenSize().getWidth());
		this.height = ((int) tk.getScreenSize().getHeight());
		createDisplay();
	}
	
	private void createDisplay() {
		setTitle("Alus");
		setSize(width, height);
		setResizable(false);
		setLocationRelativeTo(null);
		setUndecorated(true);

		if(System.getProperty("os.name").startsWith("Win"))
            System.setProperty("sun.java2d.d3d","true");
		else {
            System.setProperty("sun.java2d.opengl", "true");
            System.setProperty("sun.java2d.d3d","true");
		}
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
		    cursorImg, new Point(0, 0), "blank cursor");
		setCursor(blankCursor);

		//pannello con textbox
		panel.setBounds((width-Assets.correctWidth(Assets.logo.getWidth()))/2, height-100, Assets.correctWidth(Assets.logo.getWidth()), 50);
		panel.setOpaque(false);
		panel.setLayout(null);
		panel.setBackground(new Color(213, 134, 145, 123));
		//panel.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		
		textbox.setText("Premi il tasto C e poi il tasto Tab <-->");
		textbox.setSize(Assets.correctWidth(Assets.logo.getWidth()), 50);
		textbox.setFocusable(true);
		textbox.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		textbox.setRequestFocusEnabled(true);
		textbox.setHorizontalAlignment(JTextField.CENTER);
		textbox.setFocusTraversalKeysEnabled(false);
		//textbox.addMouseListener(new InputListener(textbox));
		//textbox.getDocument().addDocumentListener(new InputListener(textbox));
		//panel.addKeyListener(new InputListener(textbox));
		panel.add(textbox);
		panel.setVisible(false);
		//pannello con textbox fine

		canvas = new Canvas();
		canvas.setPreferredSize(new Dimension(width, height));
		canvas.setMaximumSize(new Dimension(width, height));
		canvas.setMinimumSize(new Dimension(width, height));
		setVisible(true);
		
		canvas.addMouseListener(new CustomMouseListener(this, canvas, textbox));
		canvas.addMouseMotionListener(new CustomMouseListener(this, canvas, textbox));
		canvas.addKeyListener(new CustomMouseListener(this, canvas, textbox));
		
		addWindowListener(new WindowAdapter() {
	         public void windowClosing(WindowEvent windowEvent){
	            System.exit(0);
	         }        
	    });
		getContentPane().add(panel);
		add(canvas);
		pack();
		if(!System.getProperty("os.name").startsWith("Win")) {
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice gd = ge.getDefaultScreenDevice();
			setExtendedState(JFrame.MAXIMIZED_BOTH);
			gd.setFullScreenWindow(this);
		}
	}
	
	public Canvas getCanvas() {
		return canvas;
	}
	
}