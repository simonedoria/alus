package Alus;

import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import System.Audio;
import net.coobird.thumbnailator.Thumbnails;

/**
 * Contiene metodi statici per il caricamento delle risorse (come {@code Element}, {@code Audio}, ecc..) del gioco.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class Assets {
	
	//audio
	/**
	 * Risorsa di tipo {@code Audio}.
	 */
	public static Audio mainTheme, battleTheme, beachTheme, accampamentoTheme, monasteroTheme, prigioneTheme, mainTitle, crediti, dialogoFinale;
	
	//elementi menu iniziale
	/**
	 * Risorsa di tipo {@code Element}
	 */
	public static Element cursor, splash, sfondo, logo, newG, newGOver, load, loadOver, credits, creditsOver, esci, esciOver, stats, iconaAb, plus, boxStats, esciBoxStats, team, indietro, teamLogo;
	//elementi battaglia
	/**
	 * Risorsa di tipo {@code Element}
	 */
	public static Element bgBattle, boxTestoCentrale, barraManaSinistra, logoCentraleBattle, boxStatusS, boxStatusD;
	/**
	 * Risorsa di tipo {@code Element}
	 */
	public static Element barraLottaSinistra, barraLottaSinistra80, barraLottaSinistra60, barraLottaSinistra40, barraLottaSinistra20, barraLottaSinistra0;
	/**
	 * Risorsa di tipo {@code Element}
	 */
	public static Element barraLottaDestra, barraLottaDestra80, barraLottaDestra60, barraLottaDestra40, barraLottaDestra20, barraLottaDestra0;
	
	//elementi inventario
	/**
	 * Risorsa di tipo {@code Element}
	 */
	public static Element iconaInv, iconaEsci, boxDescrizione, boxInventario, button, frecciaDX, frecciaSX, button1, button2, button3, button4;
	//elementi scena iniziale
	/**
	 * Risorsa di tipo {@code Element}
	 */
	public static Element messaggio1, messaggio2, messaggio3, messaggioLogo;
	//elementi scena finale
	/**
	 * Risorsa di tipo {@code Element}
	 */
	public static Element m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12;
	//elementi carica partita
	/**
	 * Risorsa di tipo {@code Element}
	 */
	public static Element frecciaGiu, frecciaSu, boxCarica, list1, list2, list3, list4, esciCarica;
	
	/**
	 * Variabile statica intera che rappresenta la larghezza dello schermo.
	 */
	private static int width;
	/**
	 * Variabile statica intera che rappresenta la lunghezza dello schermo.
	 */
	private static int height;
	//Toolkit tk = Toolkit.getDefaultToolkit(); DA ELIMINARE PERCHE' INUTILE
	private static String path = "";
	/**
	 * Inizializza tutte le risorse grafiche e sonore.
	 */
	public static void init() {
		//inizializzo dimensioni schermo
		Toolkit tk = Toolkit.getDefaultToolkit();
		width = ((int) tk.getScreenSize().getWidth());
		height = ((int) tk.getScreenSize().getHeight());
		
		//richiamo il path del JAR
		File f1 = new File(System.getProperty("java.class.path"));
		File dir = f1.getAbsoluteFile().getParentFile();
		//path = dir.toString();
		//path += File.separator + "alusData" + File.separator + "immagini" + File.separator;
		path = "src/Alus/res/";
		//reisizing immagini
		try {
			//creazione nuove immagini con risoluzioni corrette
			Thumbnails.of(new File(path + "newg-original.png")).size(correctWidth(557), correctHeight(174)).toFile(path + "newg.png");
			Thumbnails.of(new File(path + "carica-original.png")).size(correctWidth(557), correctHeight(174)).toFile(path + "carica.png");
			Thumbnails.of(new File(path + "credits-original.png")).size(correctWidth(557), correctHeight(174)).toFile(path + "credits.png");
			Thumbnails.of(new File(path + "newghover-original.png")).size(correctWidth(557), correctHeight(174)).toFile(path + "newghover.png");
			Thumbnails.of(new File(path + "caricahover-original.png")).size(correctWidth(557), correctHeight(174)).toFile(path + "caricahover.png");
			Thumbnails.of(new File(path + "creditshover-original.png")).size(correctWidth(557), correctHeight(174)).toFile(path + "creditshover.png");
			Thumbnails.of(new File(path + "logo-original.png")).size(correctWidth(1102), correctHeight(481)).toFile(path + "logo.png");
			Thumbnails.of(new File(path + "sfondo-original.png")).size(width, height).toFile(path + "sfondo.png");
			Thumbnails.of(new File(path + "esci-original.png")).size(correctWidth(226), correctHeight(85)).toFile(path + "esci.png");
			Thumbnails.of(new File(path + "escihover-original.png")).size(correctWidth(226), correctHeight(85)).toFile(path + "escihover.png");
			Thumbnails.of(new File(path + "box-original.png")).size(correctWidth(1080), correctHeight(613)).toFile(path + "box.png");
			Thumbnails.of(new File(path + "stats-original.png")).size(correctWidth(64), correctHeight(219)).toFile(path + "stats.png");
			Thumbnails.of(new File(path + "abilita-original.png")).size(correctWidth(215), correctHeight(224)).toFile(path + "abilita.png");
			Thumbnails.of(new File(path + "boxStats-original.png")).size(correctWidth(501), correctHeight(244)).toFile(path + "boxStats.png");
			Thumbnails.of(new File(path + "plus-original.png")).size(correctWidth(36), correctHeight(34)).toFile(path + "plus.png");
			Thumbnails.of(new File(path + "teamLogo-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "teamLogo.png");
			//battleAssets
			Thumbnails.of(new File(path + "barraLotta/0-destra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/0-destra.png");
			Thumbnails.of(new File(path + "barraLotta/0-sinistra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/0-sinistra.png");
			Thumbnails.of(new File(path + "barraLotta/20-destra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/20-destra.png");
			Thumbnails.of(new File(path + "barraLotta/20-sinistra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/20-sinistra.png");
			Thumbnails.of(new File(path + "barraLotta/40-destra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/40-destra.png");
			Thumbnails.of(new File(path + "barraLotta/40-sinistra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/40-sinistra.png");
			Thumbnails.of(new File(path + "barraLotta/60-destra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/60-destra.png");
			Thumbnails.of(new File(path + "barraLotta/60-sinistra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/60-sinistra.png");
			Thumbnails.of(new File(path + "barraLotta/80-destra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/80-destra.png");
			Thumbnails.of(new File(path + "barraLotta/80-sinistra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/80-sinistra.png");
			Thumbnails.of(new File(path + "barraLotta/100-destra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/100-destra.png");
			Thumbnails.of(new File(path + "barraLotta/100-sinistra-original.png")).size(correctWidth(867), correctHeight(169)).toFile(path + "barraLotta/100-sinistra.png");
			Thumbnails.of(new File(path + "bgBattle-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "bgBattle.png");
			Thumbnails.of(new File(path + "barraLotta/barraMana-sinistra-original.png")).size(correctWidth(868), correctHeight(59)).toFile(path + "barraLotta/barraMana-sinistra.png");
			Thumbnails.of(new File(path + "barraLotta/logoCentraleBattle-original.png")).size(correctWidth(276), correctHeight(277)).toFile(path + "barraLotta/logoCentraleBattle.png");
			//fine battleAssets
			//assets scena iniziale
			Thumbnails.of(new File(path + "scenaIniziale/messaggio1-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaIniziale/messaggio1.png");
			Thumbnails.of(new File(path + "scenaIniziale/messaggio2-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaIniziale/messaggio2.png");
			Thumbnails.of(new File(path + "scenaIniziale/messaggio3-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaIniziale/messaggio3.png");
			Thumbnails.of(new File(path + "scenaIniziale/messaggioLogo-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaIniziale/messaggioLogo.png");
			//fine assets scena iniziale
			//assets scena finale
			Thumbnails.of(new File(path + "scenaFinale/1-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/1.png");
			Thumbnails.of(new File(path + "scenaFinale/2-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/2.png");
			Thumbnails.of(new File(path + "scenaFinale/3-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/3.png");
			Thumbnails.of(new File(path + "scenaFinale/4-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/4.png");
			Thumbnails.of(new File(path + "scenaFinale/5-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/5.png");
			Thumbnails.of(new File(path + "scenaFinale/6-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/6.png");
			Thumbnails.of(new File(path + "scenaFinale/7-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/7.png");
			Thumbnails.of(new File(path + "scenaFinale/8-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/8.png");
			Thumbnails.of(new File(path + "scenaFinale/9-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/9.png");
			Thumbnails.of(new File(path + "scenaFinale/10-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/10.png");
			Thumbnails.of(new File(path + "scenaFinale/11-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/11.png");
			Thumbnails.of(new File(path + "scenaFinale/12-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "scenaFinale/12.png");
			//fine assets scena finale
			//box status
			Thumbnails.of(new File(path + "barraLotta/status-original.png")).size(correctWidth(83), correctHeight(56)).toFile(path + "barraLotta/status.png");
			Thumbnails.of(new File(path + "barraLotta/status2-original.png")).size(correctWidth(83), correctHeight(56)).toFile(path + "barraLotta/status2.png");
			//fine box status
			//inventario
			Thumbnails.of(new File(path + "inventario/inventario-original.png")).size(correctWidth(215), correctHeight(224)).toFile(path + "inventario/inventario.png");
			Thumbnails.of(new File(path + "inventario/esci-inv-original.png")).size(correctWidth(56), correctHeight(56)).toFile(path + "inventario/esci-inv.png");
			Thumbnails.of(new File(path + "inventario/boxDescrizione-original.png")).size(correctWidth(1350), correctHeight(611)).toFile(path + "inventario/boxDescrizione.png");
			Thumbnails.of(new File(path + "inventario/boxInventario-original.png")).size(correctWidth(2001), correctHeight(1023)).toFile(path + "inventario/boxInventario.png");
			Thumbnails.of(new File(path + "inventario/button-original.png")).size(correctWidth(468), correctHeight(116)).toFile(path + "inventario/button.png");
			Thumbnails.of(new File(path + "inventario/frecciaDX-original.png")).size(correctWidth(165), correctHeight(93)).toFile(path + "inventario/frecciaDX.png");
			Thumbnails.of(new File(path + "inventario/frecciaSX-original.png")).size(correctWidth(165), correctHeight(93)).toFile(path + "inventario/frecciaSX.png");
			//fine inventario
			//carica partita
			Thumbnails.of(new File(path + "caricaPartita/frecciaGiu-original.png")).size(correctWidth(90), correctHeight(165)).toFile(path + "caricaPartita/frecciaGiu.png");
			Thumbnails.of(new File(path + "caricaPartita/frecciaSu-original.png")).size(correctWidth(90), correctHeight(165)).toFile(path + "caricaPartita/frecciaSu.png");
			Thumbnails.of(new File(path + "caricaPartita/boxCarica-original.png")).size(correctWidth(721), correctHeight(583)).toFile(path + "caricaPartita/boxCarica.png");
			Thumbnails.of(new File(path + "caricaPartita/strisciaLista-original.png")).size(correctWidth(666), correctHeight(124)).toFile(path + "caricaPartita/strisciaLista.png");
			//fine carica partita
			//credits
			Thumbnails.of(new File(path + "team-original.png")).size(correctWidth(2560), correctHeight(1440)).toFile(path + "team.png");
			Thumbnails.of(new File(path + "indietro-original.png")).size(correctWidth(165), correctHeight(74)).toFile(path + "indietro.png");
			//fine credits
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//inizializzo elementi carica partita
		boxCarica = new Element(ImageLoader.loadImage(path + "caricaPartita/boxCarica.png"), width/2 - correctWidth(721)/2, width/2 + correctWidth(721)/2, height/2 - correctHeight(583)/2, height/2 + correctHeight(583)/2, 721, 583);
		frecciaSu = new Element(ImageLoader.loadImage(path + "caricaPartita/frecciaSu.png"), boxCarica.getPosXb(), boxCarica.getPosXb() + correctWidth(90), boxCarica.getPosYa() + correctHeight(40), boxCarica.getPosYa() + correctHeight(165), 90, 165);
		frecciaGiu = new Element(ImageLoader.loadImage(path + "caricaPartita/frecciaGiu.png"), boxCarica.getPosXb(), boxCarica.getPosXb() + correctWidth(90), boxCarica.getPosYb() - correctHeight(165), boxCarica.getPosYb(), 90, 165);
		list1 = new Element(ImageLoader.loadImage(path + "caricaPartita/strisciaLista.png"), boxCarica.getPosXa() + correctWidth(28), boxCarica.getPosXa() + correctWidth(28) + correctWidth(666), boxCarica.getPosYa() + correctHeight(68), boxCarica.getPosYa() + correctHeight(68) + correctHeight(124), 666, 124);
		list2 = new Element(ImageLoader.loadImage(path + "caricaPartita/strisciaLista.png"), boxCarica.getPosXa() + correctWidth(28), boxCarica.getPosXa() + correctWidth(28) + correctWidth(666), boxCarica.getPosYa() + correctHeight(192), boxCarica.getPosYa() + correctHeight(192) + correctHeight(124), 666, 124);
		list3 = new Element(ImageLoader.loadImage(path + "caricaPartita/strisciaLista.png"), boxCarica.getPosXa() + correctWidth(28), boxCarica.getPosXa() + correctWidth(28) + correctWidth(666), boxCarica.getPosYa() + correctHeight(316), boxCarica.getPosYa() + correctHeight(316) + correctHeight(124), 666, 124);
		list4 = new Element(ImageLoader.loadImage(path + "caricaPartita/strisciaLista.png"), boxCarica.getPosXa() + correctWidth(28), boxCarica.getPosXa() + correctWidth(28) + correctWidth(666), boxCarica.getPosYa() + correctHeight(440), boxCarica.getPosYa() + correctHeight(440) + correctHeight(124), 666, 124);
		esciCarica = new Element(ImageLoader.loadImage(path + "inventario/esci-inv.png"), boxCarica.getPosXa() - correctWidth(20), boxCarica.getPosXb() + correctWidth(56) - correctWidth(20), boxCarica.getPosYa() + correctHeight(10), boxCarica.getPosYa() + correctHeight(10) + correctHeight(56), 56, 56);
		//fine inizializzazione elementi carica partita

		//inizializzo elementi immagini
		cursor = new Element(ImageLoader.loadImage(path + "newmouse.png"), 0, 32, 0, 32, 32, 32);
		sfondo = new Element(ImageLoader.loadImage(path + "sfondo.png"), 0, width, 0, height, width, height);
		teamLogo = new Element(ImageLoader.loadImage(path + "teamLogo.png"), 0, width, 0, height, width, height);
		splash = new Element(ImageLoader.loadImage(path + "splash.png"), 0, width, 0, height, width, height);
		logo =   new Element(ImageLoader.loadImage(path + "logo.png"), (width/2)-correctWidth(1102)/2, (width/2)+correctWidth(1102)/2, (height/100*5), (height/100*5) + correctHeight(481), 1102, 481);
		newG =   new Element(ImageLoader.loadImage(path + "newg.png"), (width/2)-correctWidth(557)/2, (width/2)+correctWidth(557)/2, (height/100)*55, (height/100)*55 + correctHeight(174), 557, 174);
		newGOver = new Element(ImageLoader.loadImage(path + "newghover.png"), (width/2)-correctWidth(557)/2, (width/2)+correctWidth(557)/2, (height/100)*55, (height/100)*55 + correctHeight(174), 557, 174);
		load =   new Element(ImageLoader.loadImage(path + "carica.png"), (width/2)-correctWidth(557)/2, (width/2)+correctWidth(557)/2, (height/100)*70, (height/100)*70 + correctHeight(174), 557, 174);
		loadOver = new Element(ImageLoader.loadImage(path + "caricahover.png"), (width/2)-correctWidth(557)/2, (width/2)+correctWidth(557)/2, (height/100)*70, (height/100)*70 + correctHeight(174), 557, 174);
		credits = new Element(ImageLoader.loadImage(path + "credits.png"), (width/2)-correctWidth(557)/2, (width/2)+correctWidth(557)/2, (height/100)*85, (height/100)*85 + correctHeight(174), 557, 174);
		creditsOver = new Element(ImageLoader.loadImage(path + "creditshover.png"), (width/2)-correctWidth(557)/2, (width/2)+correctWidth(557)/2, (height/100)*85, (height/100)*85 + correctHeight(174), 557, 174);
		esci = new Element(ImageLoader.loadImage(path + "esci.png"), width-correctWidth(226)-50, width-20, height-correctHeight(85)-40, height-40, 226, 85);
		esciOver = new Element(ImageLoader.loadImage(path + "escihover.png"), width-correctWidth(226)-50, width-20, height-correctHeight(85)-40, height-40, 226, 85);
		boxTestoCentrale = new Element(ImageLoader.loadImage(path + "box.png"), Assets.logo.getPosXa(), Assets.logo.getPosXa() + correctWidth(1080), Assets.logo.getPosYb(), Assets.logo.getPosXb() + correctHeight(613), 1080, 613);
		stats = new Element(ImageLoader.loadImage(path + "stats.png"), width - correctWidth(64) - correctWidth(400), width - correctWidth(400), correctHeight(100), correctHeight(100) + correctHeight(219), 64, 219);
		plus = new Element(ImageLoader.loadImage(path + "plus.png"), width - correctWidth(36) - correctWidth(390), width - correctWidth(390), stats.getPosYb() + correctHeight(20), stats.getPosYb() + correctHeight(20) + correctHeight(34), 36,34);
		boxStats = new Element(ImageLoader.loadImage(path + "boxStats.png"), width - correctWidth(501) - correctWidth(40), width - correctWidth(40), height/2 - correctHeight(244)/2, height/2 + correctHeight(244)/2, 501, 244);
		esciBoxStats = new Element(ImageLoader.loadImage(path + "inventario/esci-inv.png"), boxStats.getPosXa() - correctWidth(30), boxStats.getPosXa() + correctWidth(56) - correctWidth(30), boxStats.getPosYa() - correctHeight(40),boxStats.getPosYa() - correctHeight(40) + correctHeight(56), 56, 56);
		//battleAssets                                       
		bgBattle = new Element(ImageLoader.loadImage(path + "bgBattle.png"), 0, width, 0, height, width, height);
		barraLottaSinistra = new Element(ImageLoader.loadImage(path + "barraLotta/100-sinistra.png"), widthPercentage(10), widthPercentage(10) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		barraLottaSinistra80 = new Element(ImageLoader.loadImage(path + "barraLotta/80-sinistra.png"), widthPercentage(10), widthPercentage(10) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		barraLottaSinistra60 = new Element(ImageLoader.loadImage(path + "barraLotta/60-sinistra.png"), widthPercentage(10), widthPercentage(10) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		barraLottaSinistra40 = new Element(ImageLoader.loadImage(path + "barraLotta/40-sinistra.png"), widthPercentage(10), widthPercentage(10) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		barraLottaSinistra20 = new Element(ImageLoader.loadImage(path + "barraLotta/20-sinistra.png"), widthPercentage(10), widthPercentage(10) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		barraLottaSinistra0 = new Element(ImageLoader.loadImage(path + "barraLotta/0-sinistra.png"), widthPercentage(10), widthPercentage(10) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		
		barraManaSinistra = new Element(ImageLoader.loadImage(path + "barraLotta/barraMana-sinistra.png"), widthPercentage(10), widthPercentage(10) + correctWidth(867), heightPercentage(10) + correctHeight(barraLottaSinistra.getHeight()) + 2, heightPercentage(10) + correctHeight(168), 868, 59);
				
		logoCentraleBattle = new Element(ImageLoader.loadImage(path + "barraLotta/logoCentraleBattle.png"), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(276), heightPercentage(10) - (correctHeight(277)/2 - correctHeight(barraLottaSinistra.getHeight())/2), heightPercentage(10) + correctHeight(277), 276, 277);
				
		barraLottaDestra = new Element(ImageLoader.loadImage(path + "barraLotta/100-destra.png"), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		barraLottaDestra80 = new Element(ImageLoader.loadImage(path + "barraLotta/80-destra.png"), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		barraLottaDestra60 = new Element(ImageLoader.loadImage(path + "barraLotta/60-destra.png"), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		barraLottaDestra40 = new Element(ImageLoader.loadImage(path + "barraLotta/40-destra.png"), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		barraLottaDestra20 = new Element(ImageLoader.loadImage(path + "barraLotta/20-destra.png"), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
		barraLottaDestra0 = new Element(ImageLoader.loadImage(path + "barraLotta/0-destra.png"), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()), widthPercentage(10) + correctWidth(barraLottaSinistra.getWidth()) + correctWidth(logoCentraleBattle.getWidth()) + correctWidth(867), heightPercentage(10), heightPercentage(10) + correctHeight(168), 867, 168);
				
		boxStatusS = new Element(ImageLoader.loadImage(path + "barraLotta/status.png"), barraManaSinistra.getPosXa(), barraManaSinistra.getPosXa() + correctWidth(83), barraManaSinistra.getPosYb() + correctHeight(80), barraManaSinistra.getPosYb() + correctHeight(80) + correctHeight(56), 83, 56);
		boxStatusD = new Element(ImageLoader.loadImage(path + "barraLotta/status2.png"), barraLottaDestra.getPosXb() - correctWidth(83), barraLottaDestra.getPosXb(), barraManaSinistra.getPosYb() + correctHeight(80), barraManaSinistra.getPosYb() + correctHeight(80), 83, 56);
		//fine battleAssets
				
		//scenaIniziale
		messaggio1 = new Element(ImageLoader.loadImage(path + "scenaIniziale/messaggio1.png"), 0, width, 0, height, width, height);
		messaggio2 = new Element(ImageLoader.loadImage(path + "scenaIniziale/messaggio2.png"), 0, width, 0, height, width, height);
		messaggio3 = new Element(ImageLoader.loadImage(path + "scenaIniziale/messaggio3.png"), 0, width, 0, height, width, height);
		messaggioLogo = new Element(ImageLoader.loadImage(path + "scenaIniziale/messaggioLogo.png"), 0, width, 0, height, width, height);
		//fine scenaIniziale
		
		//scenaIniziale
		m1 = new Element(ImageLoader.loadImage(path + "scenaFinale/1.png"), 0, width, 0, height, width, height);
		m2 = new Element(ImageLoader.loadImage(path + "scenaFinale/2.png"), 0, width, 0, height, width, height);
		m3 = new Element(ImageLoader.loadImage(path + "scenaFinale/3.png"), 0, width, 0, height, width, height);
		m4 = new Element(ImageLoader.loadImage(path + "scenaFinale/4.png"), 0, width, 0, height, width, height);
		m5 = new Element(ImageLoader.loadImage(path + "scenaFinale/5.png"), 0, width, 0, height, width, height);
		m6 = new Element(ImageLoader.loadImage(path + "scenaFinale/6.png"), 0, width, 0, height, width, height);
		m7 = new Element(ImageLoader.loadImage(path + "scenaFinale/7.png"), 0, width, 0, height, width, height);
		m8 = new Element(ImageLoader.loadImage(path + "scenaFinale/8.png"), 0, width, 0, height, width, height);
		m9 = new Element(ImageLoader.loadImage(path + "scenaFinale/9.png"), 0, width, 0, height, width, height);
		m10 = new Element(ImageLoader.loadImage(path + "scenaFinale/10.png"), 0, width, 0, height, width, height);
		m11 = new Element(ImageLoader.loadImage(path + "scenaFinale/11.png"), 0, width, 0, height, width, height);
		m12 = new Element(ImageLoader.loadImage(path + "scenaFinale/12.png"), 0, width, 0, height, width, height);
		//fine scenaIniziale
				
		//inventario												                                                                                                                		
		iconaInv = new Element(ImageLoader.loadImage(path + "inventario/inventario.png"),width - correctWidth(215) - correctWidth(50), width - correctWidth(50), height - correctHeight(224) - correctHeight(100), height - correctHeight(100), 215, 224);
		iconaAb = new Element(ImageLoader.loadImage(path + "abilita.png"), width - correctWidth(430) - correctWidth(70), width - correctWidth(215) - correctWidth(70), height - correctHeight(224) - correctHeight(100), height - correctHeight(100), 215, 224);
		boxInventario = new Element(ImageLoader.loadImage(path + "inventario/boxInventario.png"), (width-correctWidth(2001))/2, (width-correctWidth(2001))/2 + correctWidth(2001), (height-correctHeight(1023))/2, (height-correctHeight(1023))/2 + correctHeight(1023), 2001, 1023);
		iconaEsci = new Element(ImageLoader.loadImage(path + "inventario/esci-inv.png"), boxInventario.getPosXb() - correctWidth(56) - correctWidth(30), boxInventario.getPosXb() - correctWidth(30), boxInventario.getPosYa() + correctHeight(30), boxInventario.getPosYa() + correctHeight(30) + correctHeight(56), 56, 56);
		button = new Element(ImageLoader.loadImage(path + "inventario/button.png"), boxInventario.getPosXa() + correctWidth(40), boxInventario.getPosXa() + correctWidth(40) + correctWidth(468), boxInventario.getPosYa() + correctHeight(300), boxInventario.getPosYb() + correctHeight(300) + correctHeight(116), 468, 116);
		button1 = new Element(ImageLoader.loadImage(path + "inventario/button.png"), boxInventario.getPosXa() + correctWidth(40), boxInventario.getPosXa() + correctWidth(40) + correctWidth(468), boxInventario.getPosYa() + correctHeight(300) + correctHeight(125), boxInventario.getPosYb() + correctHeight(300) + correctHeight(116) + correctHeight(125), 468, 116);
		button2 = new Element(ImageLoader.loadImage(path + "inventario/button.png"), boxInventario.getPosXa() + correctWidth(40), boxInventario.getPosXa() + correctWidth(40) + correctWidth(468), boxInventario.getPosYa() + correctHeight(300) + correctHeight(250), boxInventario.getPosYb() + correctHeight(300) + correctHeight(116) + correctHeight(250), 468, 116);
		button3 = new Element(ImageLoader.loadImage(path + "inventario/button.png"), boxInventario.getPosXa() + correctWidth(40), boxInventario.getPosXa() + correctWidth(40) + correctWidth(468), boxInventario.getPosYa() + correctHeight(300) + correctHeight(375), boxInventario.getPosYb() + correctHeight(300) + correctHeight(116) + correctHeight(375), 468, 116);
		button4 = new Element(ImageLoader.loadImage(path + "inventario/button.png"), boxInventario.getPosXa() + correctWidth(40), boxInventario.getPosXa() + correctWidth(40) + correctWidth(468), boxInventario.getPosYa() + correctHeight(300) + correctHeight(500), boxInventario.getPosYb() + correctHeight(300) + correctHeight(116) + correctHeight(500), 468, 116);
				
		boxDescrizione = new Element(ImageLoader.loadImage(path + "inventario/boxDescrizione.png"), button.getPosXb() + correctWidth(5), button.getPosXb() + correctWidth(5) + correctWidth(1350), button.getPosYa(), button.getPosYa() + correctHeight(611), 1350, 611);
		frecciaDX = new Element(ImageLoader.loadImage(path + "inventario/frecciaDX.png"), boxDescrizione.getPosXb() - correctWidth(165), boxDescrizione.getPosXb(), boxDescrizione.getPosYa() - correctHeight(93), boxDescrizione.getPosYa(), 165, 93);
		frecciaSX = new Element(ImageLoader.loadImage(path + "inventario/frecciaSX.png"), boxDescrizione.getPosXb() - correctWidth(165)*2, boxDescrizione.getPosXb() - correctWidth(165), boxDescrizione.getPosYa() - correctHeight(93), boxDescrizione.getPosYa(), 165, 93);
		//fine inventario
				
		//credits
		team = new Element(ImageLoader.loadImage(path + "team.png"), 0, width, 0, height, width, height);
		indietro = new Element(ImageLoader.loadImage(path + "indietro.png"), correctWidth(100), correctWidth(100) + correctWidth(165), correctHeight(100), correctHeight(100) + correctHeight(74), 165, 74);
		//fine credits
				
		//inizializzo musiche
		mainTheme = new Audio(path + "music/MainThemeAlus.mp3", false);
		battleTheme = new Audio(path + "music/Battle_Theme.mp3", true);
		beachTheme = new Audio(path + "music/Beach_Theme.mp3", true);
		accampamentoTheme = new Audio(path + "music/Accampamento_Theme.mp3", true);
		monasteroTheme = new Audio(path + "music/Monastero_Theme.mp3", true);
		prigioneTheme = new Audio(path + "music/Prison_Theme.mp3", true);
		mainTitle = new Audio(path + "music/MAIN_TITLE.mp3", false);
		crediti = new Audio(path + "music/CREDITI.mp3", false);
		dialogoFinale = new Audio(path + "music/dialogo_finale.mp3", false);
	}
	
	/**
	 * Corregge la larghezza dell'immagine adattandola allo schermo.
	 * @param larg Misura da correggere
	 * @return L'inetro corrispondente alla larghezza corretta
	 */
	public static int correctWidth(int larg) {
		return (width*larg)/2560;
	}
	/**
	 * Corregge l'altezza dell'immagine adattandola allo schermo.
	 * @param larg Misura da correggere
	 * @return L'inetro corrispondente all'altezza corretta
	 */
	public static int correctHeight(int alt) {
		return (height*alt)/1440;
	}
	
	/**
	 * Calcola la percentuale corrispondente alla larghezza dello schermo
	 * @param perc Percentuale da calcolare.
	 * @return Ritorna l'intero corrispondente  alla percentuale passata come parametro.
	 */
	public static int widthPercentage(int perc) {
		return (width/100)*perc;
	}
	
	/**
	 * Calcola la percentuale corrispondente all'altezza dello schermo
	 * @param perc Percentuale da calcolare.
	 * @return Ritorna l'intero corrispondente  alla percentuale passata come parametro.
	 */
	public static int heightPercentage(int perc) {
		return (height/100)*perc;
	}
	
	/**
	 * Mette in "stop" le risorse {@code Audio} tramite il metodo {@link Audio#stop()}. 
	 */
	public static void mute() {
		mainTheme.stop();
		battleTheme.stop();
		beachTheme.stop();
		accampamentoTheme.stop();
		monasteroTheme.stop();
		prigioneTheme.stop();
		mainTitle.stop();
		crediti.stop();
		dialogoFinale.stop();
	}
}
