package Combattimento;

import Alus.Game;
import Personaggi.*;
import System.RoutineSystem;

/**
 * E' una delle classi cardine del gioco.<br>
 * La lotta rappresenta il 50% di tutta la logica su cui e' basato il gioco.
 * All'interno della suddetta classe, viene gestito il combattimento tra il protagonista e gli eventuali avversari.
 * Implementa l'interfaccia {@code Runneble} per utilizzare i Thread.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 * @see Runnable
 *
 */

public class Lotta implements Runnable {
	
	private static String testoCentrale = " ";
	public static Personaggio p1, p2;
	private boolean gameover;
	
	private Thread thread = new Thread(this);
	
	public Lotta(boolean gameoverr) {
		p1 = Game.m.protagonista;
		p2 = Game.m.avversario;
		gameover = gameoverr;
	}
	/**
	 * Il metodo combattimento prende come parametri due oggetti di tipo {@code Personaggio}.<br>
	 * Appena viene richiamato determina chi deve iniziare. Mediante un metodo di supporto prensente in {@code RoutineCombattimento},<br>
	 * Subito dopo entra nel ciclo "infinito". La prima cosa che viene fatta nel ciclo infinito e' evocare gli {@code Status}.<br>
	 * A seguire, un controllo preventivo sugli attuali punti vita dei due duellanti, nel caso in cui uno dei due fosse a zero, interrompe il ciclo.<br>
	 * Un flag determina di chi è il turno, se si tratta del proprio turno, si può scegliere di attaccare utilizzando le {@code Abilita} che si ha imparato.<br>
	 * L'avversario al contrario risponde all'attacco utilizzando abilità prestabilite mixandole usando un'intelligenza artificiale.<br>
	 * @see RoutineCombattimento
	 */
	private void combattimento(Personaggio p1, Personaggio p2) {
		//FLAG CHE GESTISCE CHI INIZIA
		int flag = RoutineCombattimento.whoStart(p1, p2);
		//FINE FLAG CHE GESTISCE CHI INIZIA
		
		while(true) { //inizio il ciclo "infinito" di combattimento
			RoutineCombattimento.evocaStatus(p1,p2);
			//INIZIO CONTROLLO SU CHI HA VINTO
			if(p1.getPv()<=0) {
				setTestoCentrale("Hai perso!");
				RoutineSystem.dormi(2000);
				Game.m.returnBattle();
				break;
			}
			if(p2.getPv()<=0) { 
				setTestoCentrale("Il vincitore e' "+p1.getNome());
				RoutineCombattimento.ricompensa(p1, p2);
				p1.getStatusAttivi().clear();
				break;
			}
			//FINE CONTROLLO SU CHI HA VINTO
			//INIZIO CONTROLLO SUI TURNI DI COMBATTIMENTO
			if(flag==0) {
				p1.setPm(p1.getPm() +2);
				setTestoCentrale("E' il tuo turno!");
				RoutineCombattimento.scegliAbilita(p1, p2);
				flag=1;
			} else {
				p2.setPm(p2.getPm() +2);
				setTestoCentrale("E' il turno di " + p2.getNome());
				RoutineSystem.dormi(1000);
				RoutineCombattimento.rispostaAvversario(p2, p1);
				flag=0;
			}
			//FINE CONTROLLO SUI TURNI DI COMBATTIMENTO
		}
	}
	/**
	 * Restituisce l'ultima print effettuata sul box centrale nel {@code BattleState}.
	 */
	public String getTestoCentrale() {
		return testoCentrale;
	}
	/**
	 * Printa una nuova stringa sul box centrale nel {@code BattleState}.
	 */
	public void setTestoCentrale(String testoCentral) {
		testoCentrale = testoCentral;
	}
	/**
	 * Fa partire il {@code Thread}
	 */
	public synchronized void start() {
		thread.start();
	}
	/**
	 * Esegue il metodo Combattimento.
	 */
	@Override
	public void run() {
		combattimento(p1, p2);
	}
	/**
	 * Restituisce l'esito della battaglia. True se si ha vinto, False se si ha perso.
	 */
	public boolean getGameover() {
		return gameover;
	}
}