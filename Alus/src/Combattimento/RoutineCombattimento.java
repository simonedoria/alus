package Combattimento;

import java.util.Random;

import Abilita.Abilita;
import Alus.Game;
import Personaggi.*;
import Status.Status;
import System.RoutineSystem;

/**
 * Contiene una serie di metodi statici che permettono di effettuare calcoli sulle varie statistiche dei personaggi e condizionarne le azioni.
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 * @see Runnable
 *
 */
public class RoutineCombattimento {
	/**
	 * Prende come parametro un oggetto di tipo {@code Personaggio}.<br>
	 * Genera un numero random fra 1 e 100 e se il suddetto numero risulta uguale o minore al valore di probabilita' di riuscita del "colpo critico" per la fortuna del Personaggio<br>
	 * allora restituisce True. Altrimenti False.
	 */
	public static boolean critico(Personaggio p1) {
		Random rn=new Random();
		double random=rn.nextInt(100)+1;
		if(random<=(p1.getCrit()*p1.getFort())){
			Game.m.lotta.setTestoCentrale("Critico!");
			RoutineSystem.dormi(500);
			return true;
		}
		return false;
	}
	//#########################################################################################
	/**
	 * Prende come parametro un oggetto di tipo {@code Personaggio} ed un valore numerico di tipo {@code double}.<br>
	 * Genera un numero random fra 1 e 100 e se il suddetto numero risulta uguale o minore al valore di probabilita' di riuscita del "colpo critico" per la fortuna del Personaggio<br>
	 * allora restituisce True. Altrimenti False.
	 * Questa versione al contrario del metodo Critico normale, permette di settare un critico sul momento. E non per forza usare quello di base del personaggio.
	 */
	public static boolean critico2(Personaggio p1, double crit) {
		Random rn=new Random();
		double random=rn.nextInt(100)+1;
		if(random<=(crit*p1.getFort())){
			Game.m.lotta.setTestoCentrale("Critico!");
			RoutineSystem.dormi(500);
			return true; 
		}
		return false;
	}
	//#########################################################################################
	/**
	 * Prende come parametri due oggetti di tipo {@code Personaggio}.<br>
	 * Controlla le destrezze dei due personaggi, chi ha destrezza maggiore inizia.
	 * Restituisce un {@code int} compreso tra 0 e 1.
	 */
	public static int whoStart(Personaggio p1, Personaggio p2) {
		//DECIDO CHI INIZIA PRIMA
		int flag;
		if(p1.getDes()>p2.getDes()) flag=0;
		else flag=1;
		RoutineSystem.dormi(1000);
		//FINE DECISIONE DI CHI INIZIA PRIMA
		return flag;
	}
	
	//#########################################################################################
	/**
	 * Prende come parametri due oggetti di tipo {@code Personaggio}.<br>
	 * E' un metodo molto importante in quanto crea un ciclo che non si interrompe finche' il giocatore non digita un particolare tipo di attacco che ha imparato.
	 * Ovviamente il parametro viene controllato nella lista delle {@code Abilita} imparate.
	 */
	public static void scegliAbilita(Personaggio p1, Personaggio p2) {
		Game.m.setCmdLotta(" ");
		while(true) {
			for(Abilita ab : p1.getAbilita()) {
				if(RoutineSystem.parseString(Game.m.getCmdLotta(), ab.getNome())) {
					if(ab.attiva(p1, p2)) return;
				}
			}
		}
	}

	//#########################################################################################
	/**
	 * Prende come parametri due oggetti di tipo {@code Personaggio}.<br>
	 * Quando la battaglia finisce con la vittoria, il protagonista acquisisce gli oggetti presenti nell'inventario del suo avversario.<br>
	 * Successivamente viene mostrato a video un messaggio con cio' che si e' guadagnato.
	 */
	public static void ricompensa(Personaggio p1, Personaggio p2) {
		p1.setGold(p1.getGold()+p2.getGold());
		p1.setExp(p1.getExp()+p2.getExp());
		//ogni stuff dell'avversario viene aggiunto agli staff del protagonista
		for(int i=0; i<p2.getInventario().size(); i++) {
			p1.addStuff(p2.getInventario().get(i));
		}
		StringBuilder sb = new StringBuilder();
		sb.append(p1.getNome()+" ha guadagnato "+Math.round(p2.getExp())+" punti di esperienza e "+Math.round(p2.getGold())+" monete d'oro.\n");
		for(int i=0; i<p2.getInventario().size(); i++) {
			if(i==0) sb.append("Hai guadagnato:\n");
			sb.append(p2.getInventario().get(i).getNome());
			sb.append("\n");
		}
		Game.m.lotta.setTestoCentrale(sb.toString());
	}
	//#########################################################################################
	/**
	 * Prende come parametri due oggetti di tipo {@code Personaggio}.<br>
	 * Richiama la risposta artificiale specifica degli avversari. In questo caso {@code Guardia}, e' l'unico avversario. Per ora.
	 */
	public static void rispostaAvversario(Personaggio p1, Personaggio p2) {
		
		if(p1 instanceof Personaggi.Guardia) {
			((Guardia) p1).richiamaAbilita(p2);
		}
	}
	//#########################################################################################
	/**
	 * Prende come parametri due oggetti di tipo {@code double} e {@code int}.<br>
	 * Metodo che corregge il danno inflitto da un personaggio.<br>
	 * Restituisce un double.
	 */
	public static double correzioneDanno(double danno, int var){
		if(danno <= 0)return 1;
		Random rn = new Random();
		double random = rn.nextInt(var*2)-var+1;
		danno = Math.round(danno+(danno*random/100));
		return danno;
	}
	/**
	 * Prende come parametri due oggetti di tipo {@code Personaggio}.<br>
	 * Evoca gli status e ne scatena gli effetti su entrambi i personaggi, ad ogni turno.
	 */
	public static void evocaStatus(Personaggio p1, Personaggio p2){
		for(Status st : p1.getStatusAttivi())
			st.esegui(p1);
		for(Status st : p2.getStatusAttivi())
			st.esegui(p2);
	}
	/**
	 * Piccola accortezza stilistica che mostra il susseguirsi di tre puntini sospensivi prima della stringa di attacco mostrata a schermo.
	 */
	public static void waitDots() {
		StringBuilder dots = new StringBuilder();
		dots.append(".");
		Game.m.lotta.setTestoCentrale(dots.toString());
		for(int i=0; i<3; i++) {
			RoutineSystem.dormi(300);
			dots.append(".");
			Game.m.lotta.setTestoCentrale(dots.toString());
		}
	}
}

