package System;

import java.awt.*;

import javax.swing.JTextField;

/**
 * Questa classe estende {@code JTextField} e ridisegna il componente.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class TransparentTextField extends JTextField {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int larg;
    private int fontSize;
	private Toolkit tk = Toolkit.getDefaultToolkit();
	/**
	 * @param text
	 * Setta il testo centrale e ridisegna il componente con dei parametri precisi.
	 */
    public TransparentTextField(String text) {
    	super(text);
    	this.larg = ((int) tk.getScreenSize().getWidth());
        setOpaque(false);
        setForeground(Color.WHITE);
        setBackground(new Color(0,0,0,255));
        setCaretColor(new Color(255,255,255,250));
        setSelectionColor(new Color(0x80,0x80,0xff,96));
        fontSize = (this.larg*15)/1366; //il 15 a 1366 è corretto
        setFont(new Font("Arial", Font.BOLD, fontSize));
    }
    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(getBackground());
        g.fillRect(0, 0, getWidth()-1, getHeight()-1);
        super.paintComponent(g);
    }
   
}
