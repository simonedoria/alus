package System;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * Questa classe estende {@code JPanel} e ridisegna il componente.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class TransparentPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * COSTRUTTORE
	 */
	public TransparentPanel() {
        setOpaque(false);
        setBackground(new Color(0,0,0,0));
    }
	
	@Override
    protected void paintComponent(Graphics g) {
        g.setColor(getBackground());
        g.clearRect(0, 0, getWidth(), getHeight());
        g.fillRect(0,0, getWidth(),getHeight());
        super.paintComponent(g);
    }
}
