package System;

import java.io.*;

import Abilita.*;
import Mappe.*;
import Personaggi.*;
import Status.*;
import Stuff.*;
/**
 * Questa classe gestisce i salvataggi.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class MemoryCard {
	private static Personaggio p;
	private static StatoDelGioco st;
	private static String nm;
	
	private static String fetch(String nomefile) throws IOException {
		File f1 = new File(System.getProperty("java.class.path"));
		File dir = f1.getAbsoluteFile().getParentFile();
		String path = dir.toString();
		path += File.separator + "alusData" + File.separator + "salvataggi" + File.separator;
		String filePath = path+nomefile+".alus";
		RandomAccessFile in=new RandomAccessFile(filePath, "r");
		StringBuilder sb = new StringBuilder();
		byte x;
		for(;;) {
			try {
				x=in.readByte();
				sb.append((char) x);
			}catch(EOFException e) {break;}
		}
		in.close();
		return sb.toString();
	}
	/**
	 * @param data
	 * @param nomefile
	 * @throws IOException
	 * Effettua il salvataggio di una stringa all'interno di un file convertendola in {@code Byte} utilizzando la classe {@code RandomAccessFile}.
	 */
	public static void salva(String data, String nomefile) throws IOException {
		String nomefileOriginal = nomefile;
		File f1 = new File(System.getProperty("java.class.path"));
		File dir = f1.getAbsoluteFile().getParentFile();
		String path = dir.toString();
		path += File.separator + "alusData" + File.separator + "salvataggi" + File.separator;
		String filePath = path+nomefile+".alus";
		int cont = 1;
		File f = new File(filePath);
		do {
			if(f.exists()) {
				nomefileOriginal+="-"+cont;
				filePath = path+nomefileOriginal+".alus";
				f = new File(filePath);
				cont++;
				nomefileOriginal = nomefile;
			} else break;
		} while(true);
		byte[] a = data.getBytes();
		RandomAccessFile out=new RandomAccessFile(filePath, "rw");
		for(int i=0; i<a.length; i++) out.write(a[i]);
		out.close();
	}
	/**
	 * @param nomefile
	 * @throws IOException
	 * Analizza il file di salvataggio e setta il {@code Personaggio} e lo {@code StatusDelGioco} in base ai parametri forniti.
	 */
	public static void carica(String nomefile) throws IOException {
		String dati = fetch(nomefile);
		String [] data;
		String [] prot;
		String [] stat;
		data = dati.split("\\@");
		//il nome della mappa si trova a data[2]
		nm = data[2];
		//i dati del personaggio sono in data[0]
		prot = data[0].split("\\|", -1);
		//i dati dello stato dei gioco sono in data[1]
		stat = data[1].split("\\s", -1);
		
		//inizio ad analizzare il personaggio
		//i dati del costruttore del personaggio sono in prot[0]
		String [] pers = prot[0].split("\\s");
		p = new Personaggio(pers[0], Double.parseDouble(pers[1]), Double.parseDouble(pers[2]), Double.parseDouble(pers[3]), Double.parseDouble(pers[4]), Double.parseDouble(pers[5]), Double.parseDouble(pers[6]), Double.parseDouble(pers[7]), Double.parseDouble(pers[8]), Double.parseDouble(pers[9]), Double.parseDouble(pers[10]), Double.parseDouble(pers[11]), Double.parseDouble(pers[12]), Double.parseDouble(pers[13]), Double.parseDouble(pers[14]), Double.parseDouble(pers[15]), Double.parseDouble(pers[16]), Double.parseDouble(pers[17]), Double.parseDouble(pers[18]), Double.parseDouble(pers[19]), Double.parseDouble(pers[20]));
		
		//i dati delle abilita' del personaggio sono in prot[1]
		String[] ab = prot[1].split("\\s");
		applicaAbilita(p, ab);
		
		//i dati dell'inventario del personaggio sono in prot[2]
		String[] in = prot[2].split("\\s");
		applicaInventario(p, in);
		
		//i dati degli status inapplicabili del personaggio sono in prot[3]
		String[] si = prot[3].split("\\s");
		applicaStatusInapplicabili(p, si);
		
		//il dato relativo ad arma è in prot[4]
		String ar = prot[4];
		applicaArma(p, ar);
		
		//il dato relativo ad armaScudo è in prot[5]
		String as = prot[5];
		applicaArmaScudo(p, as);
		
		//il dato relativo ad armatura è in prot[6]
		String at = prot[6];
		applicaArmatura(p, at);
		
		//il dato relativo ad accessorio è in prot[7]
		String ac = prot[7];
		applicaAccessorio(p, ac);
		
		//applico lo status del gioco a stat
		st = new StatoDelGioco(bool(stat[0]), bool(stat[1]), bool(stat[2]), bool(stat[3]), bool(stat[4]), bool(stat[5]), bool(stat[6]), bool(stat[7]), bool(stat[8]), bool(stat[9]), bool(stat[10]), bool(stat[11])); 
	}
	private static boolean bool(String str) {
		if(str.equals("1")) return true;
		return false;
	}
	private static void applicaAbilita(Personaggio p, String [] ab) {
		for(int i=0; i<ab.length; i++) {
			switch(ab[i]) {
				case "attacca":
					p.addAbilita(new Attacca());
					break;
				case "afurtivo":
					p.addAbilita(new AttaccoFurtivo());
					break;
				case "apoderoso":
					p.addAbilita(new AttaccoPoderoso());
					break;
				case "difendi":
					p.addAbilita(new Difendi());
					break;
			}
		}
	}
	private static void applicaInventario(Personaggio p, String [] in) {
		for(int i=0; i<in.length; i++) {
			switch(in[i]) {
				case "armguacar":
					p.addStuff(new ArmaturaDaGuardiaCarceraria());
					break;
				case "daga":
					p.addStuff(new Daga());
					break;
				case "pcurativa":
					p.addStuff(new PozioneCurativa());
					break;
				case "spadacorta":
					p.addStuff(new SpadaCorta());
					break;
				case "cristmisterioso":
					p.addStuff(new CristalloMisterioso());
					break;
			}
		}
	}
	private static void applicaStatusInapplicabili(Personaggio p, String [] si) {
		for(int i=0; i<si.length; i++) {
			switch(si[i]) {
				case "posdif":
					p.addStatusInapplicabili(new PosizioneDifensiva(p));
					break;
				case "sanguinante":
					p.addStatusInapplicabili(new Sanguinante(p));
					break;
			}
		}
	}
	private static void applicaArma(Personaggio p, String ar) {
		switch(ar) {
			case "daga":
				p.setArma(new Daga());
				break;
			case "spadacorta":
				p.setArma(new SpadaCorta());
				break;
		}
	}
	private static void applicaArmaScudo(Personaggio p, String as) {
		switch(as) {
			case "daga":
				p.setArmaScudo(new Daga());
				break;
			case "spadacorta":
				p.setArmaScudo(new SpadaCorta());
				break;
		}
	}
	private static void applicaArmatura(Personaggio p, String at) {
		switch(at) {
			case "armguacar":
				p.setArmatura(new ArmaturaDaGuardiaCarceraria());
				break;
		}
	}
	private static void applicaAccessorio(Personaggio p, String ac) {
		switch(ac) {
			//non sono state creati ancora accessori
		}
	}
	/**
	 * @return La {@code Mappa} settata e pronta per essere eseguita.
	 */
	public static Mappa getMap() {
		Mappa m = null;
		switch(nm) {
			case "mappa1":
				m = new Mappa1(p, st);
				break;
		}
		return m;
	}
}
