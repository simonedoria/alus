package System;

import java.util.Random;
/**
 * Questa classe contiene una serie di metodi public e static piu' o meno importanti.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class RoutineSystem {
	
	private static String OS = null;
	/**
	 * @param ms
	 * Interrompe il {@code Thread} da cui viene richiamato per un tempo tale fornito in millisecondi.
	 */
	public static void dormi(int ms) {
		try {
				Thread.sleep(ms);
			} catch(InterruptedException ex) {
   				Thread.currentThread().interrupt();
			}
	}
	//#########################################################################################
	/**
	 * @param cmd
	 * @param str
	 * @return Ritorna True se la stringa passata come primo parametro ha almeno un match nel vararg passato come secondo parametro.
	 */
	public static boolean parseString(String cmd, String ... str) {
		for(int i=0; i<str.length; i++) {
			if(str[i].toUpperCase().equals(cmd.toUpperCase())) return true;
		}
		return false;
	}
	//##########################################################################################
	/**
	 * @param perc
	 * @return Restituisce True se la percentuale passata come parametro e' minore o uguale ad un numero random generato tra 0 e 100.
	 */
	public static boolean prob(double perc) {
		Random rn=new Random();
		double random=rn.nextInt(100)+1;
		if(random<=perc) return true; 
		return false;
	}
	//##########################################################################################
	/**
	 * @param d
	 * @return Restituisce una {@code String} di un {@code double} senza il .0 finale.
	 */
	public static String rimuoviZero(double d) {
		return String.valueOf(d).replaceAll("[0]*$", "").replaceAll(".$", "");
	}
	//##########################################################################################
	/**
	 * @return Restituisce il nome del sistema operativo.
	 */
	public static String getOsName() {
	    if(OS == null) { OS = System.getProperty("os.name"); }
	    return OS;
	}
	/**
	 * @return Restituisce True se si tratta di Windows. Altrimenti False.
	 */
	public static boolean isWindows() {
	      return getOsName().startsWith("Windows");
	}
}
