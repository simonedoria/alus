package System;

import java.io.*;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class Audio implements Runnable {
	private String fileLocation;
	private Player music;
	private Thread thread;
	private boolean playing;
	private FileInputStream buff;

	public Audio(String fileName, boolean playing) {
		File f1 = new File(System.getProperty("java.class.path"));
		//File dir = f1.getAbsoluteFile().getParentFile();
		String path = "";
		//path += File.separator + "alusData" + File.separator + "audio" + File.separator;
		//path = "src/Alus/res/";
		this.playing = playing;
		this.fileLocation = path + fileName;
		try {
			buff = new FileInputStream(fileLocation);
			music = new Player(buff);
		} catch (FileNotFoundException | JavaLayerException e) {
			e.printStackTrace();
		}
	}

	   
	public void play() {
        try {
			music.play();
		} catch (JavaLayerException e) {
			e.printStackTrace();
		}
	}
	    
	        
	public void close() {
		try {
			music.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public void run() {
		try {
            do {
                buff = new FileInputStream(fileLocation);
                music = new Player(buff);
                music.play();
            } while (playing);
        } catch (Exception ioe) {}
		music.close();
    }
		
	public synchronized void start() {
		if(playing) return;
		playing = true;
		thread = new Thread(this);
		thread.start();
	}
		
	public synchronized void stop() {
		if(!playing) return;
			playing = false;
			try {
				this.finalize();
			} catch (Throwable e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				close();
			}catch(NullPointerException e) {
				System.out.println("Errore in STOP: "+e);
			}
	}
}