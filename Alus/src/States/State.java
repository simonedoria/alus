package States;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.List;

import Alus.*;
import Assets.Listeners.CustomMouseListener;
import Stuff.*;
import System.RoutineSystem;

/**
 * In questa classe di tipo astratto vengono gestite le varie "stanze" in cui va in scena il gioco.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public abstract class State {

	private static State currentState = null;
	
	protected int width, height;
	protected Toolkit tk = Toolkit.getDefaultToolkit();
	
	//Settaggi font
	protected Font fontTitle = new Font("Arial", Font.BOLD, correctFontSize(30));
	protected Font fontDescr = new Font("Arial", Font.PLAIN, correctFontSize(25));
	protected Font fontBoxStats = new Font("Arial", Font.BOLD, correctFontSize(15));
	
	/**
	 * Le seguenti variabili servono per gestire il fading degli elementi a schermo e del testo a comparsa al cambio della print.
	 */
	public static int fading;
	public int fadingTesto;
	
	/**
	 * Le seguenti variabili public e protected servono per gestire l'apparizione dell'inventario.
	 */
	public boolean isActive1 = false;
	public boolean isActive2 = false;
	public boolean isActive3 = false;
	public boolean isActiveLeftArrow = false;
	public boolean isActiveRightArrow = false;
	public boolean isActiveCarica = false;
	public int numSalv;
	protected boolean firstTime = true;
	public int numPage = 0;
	public int page = 0;
	public int cont = 0;
	public int numToCont;
	public int offsetButtonY = 0;
	public int incrementoDesc = 0;
	public int tot;
	protected String testoAggiuntivo;
	
	/**
	 * Le seguenti variabili static di tipo intero servono per gestire i tempi di apparizione e di fade delle slide nelle varie cut-scene create.
	 */
	protected static int timing1, timing2, timing3, timing4, timing5, timing6, timing7, timing8, timing9, timing10, timing11, timing12, timing13, timing14, timing15, timing16, timing17, timing18, timing19, timing20, timing21, timing22, timing23, timing24, timing25, timing26, timing27;
	//fine impostazioni scenaIniziale

	public boolean isActiveCredits;

	public boolean isActiveDownArrow;

	public List<String> listaSalvataggi;

	public boolean isActiveUpArrow;
	
	/**
	 * Il seguente metodo modifica lo {@code State} da cui attingere il metodo render.
	 * @see Game
	 */
	public void setState(State state) {
		fading = 255;
		currentState = state;
		if(currentState instanceof ScenaIniziale) {
			timing1 = 400; //tempo iniziale prima che appaia la prima scritta
			timing2 = 900; //durata della prima scritta
			timing3 = 1050;//partenza fadeOut + sostituzione scritta con la seconda
			timing4 = 1200;//durata fadein
			timing5 = 1280;//partenza fadeOut + sostituzione scritta con la terza
			timing6 = 1700;//durata terza scritta
			timing7 = 2150;//inizio logo
			timing8 = 3000;//inizio fadein logo
			timing9 = 3350;//fine. ci spostiamo nel gameState
		} else if(currentState instanceof CreditiState) {
			timing1 = 400; //tempo iniziale prima che appaia il logo
			timing2 = 1100; //durata del logo
			timing3 = 1350; //partenza fadeOut + sostituzione logo con scritta
			timing4 = 6650; //durata totale
		} else if(currentState instanceof ScenaFinale) {
			timing1 = 240; //tempo iniziale prima che appaia la prima scritta
			timing2 = 840; //durata della prima scritta
			timing3 = 1360;//partenza fadeOut + sostituzione scritta con la seconda
			timing4 = 1800;//durata fadein
			timing5 = 2460;//partenza fadeOut + sostituzione scritta con la terza
			timing6 = 2550;//durata terza scritta
			timing7 = 2640;
			timing8 = 2740;
			timing9 = 2820;
			timing10 = 3000;
			timing11 = 3400;
			timing12 = 3780;
			timing13 = 3840;
			timing14 = 4140;
			timing15 = 5040;
			timing16 = 5520;
			timing17 = 5760;
			timing18 = 6720;
			timing19 = 7380;
			timing20 = 7740;
			timing21 = 7800;
			timing22 = 8520;
			timing23 = 8880;
			timing24 = 9480;
			timing25 = 9720;
			timing26 = 10440;
			timing27 = 11160;
		}
	}
	/**
	 * @return Lo {@code State} attuale.
	 */
	public static State getState() {
		return currentState;
	}
	
	/**
	 * Renderizza cio' che viene settato mediante il parametro di tipo {@Graphics}.
	 */
	public abstract void render(Graphics g);
	
	/**
	 * @return La corretta size del {@code Font} in base alla risoluzione dello schermo.
	 */
	protected int correctFontSize(int size) {
		this.width = ((int) tk.getScreenSize().getWidth());
		return (width*size)/1366;
	}
	
	protected void inventarioOggetti(Graphics g) {
		//box di sfondo
		g.drawImage(Assets.boxInventario.getImage(), Assets.boxInventario.getPosXa(), Assets.boxInventario.getPosYa(), Assets.correctWidth(Assets.boxInventario.getWidth()), Assets.correctHeight(Assets.boxInventario.getHeight()), null);
		g.drawImage(Assets.iconaEsci.getImage(), Assets.iconaEsci.getPosXa(), Assets.iconaEsci.getPosYa(), Assets.correctWidth(Assets.iconaEsci.getWidth()), Assets.correctHeight(Assets.iconaEsci.getHeight()), null);
		if(Game.m.protagonista.getInventario().size() == 0) return;
		tot = Game.m.protagonista.getInventario().size(); //numero effettivo totale di oggetti attivi
		//paginazione
		//effettuo questi controlli solo la prima volta (per questo utilizzo il booleano firstTime)
		if(firstTime) {
			//incremento il numero di pagine almeno di 1 se il totale di valori è maggiore di 5
			if(tot > 5) {
				numPage++;
			}
			//comincio ad incrementare dal secondo valore in poi per multipli di 5 per scoprire il numero di pagine
			while(true) {
				if(tot-5 > 5) {
					numPage++;
					tot-=5;
				}
				else break;
			}
			firstTime = false;
		}
		//verifico quando devo mostrare le frecce
		if((page==0 && numPage!=0) || (page>0 && numPage>page)) {
			g.drawImage(Assets.frecciaDX.getImage(), Assets.frecciaDX.getPosXa(), Assets.frecciaDX.getPosYa(), Assets.correctWidth(Assets.frecciaDX.getWidth()), Assets.correctHeight(Assets.frecciaDX.getHeight()), null); 
			isActiveRightArrow = true;
		} else {
			isActiveRightArrow = false;
		}
		if(page!=0 && numPage>=page) {
			g.drawImage(Assets.frecciaSX.getImage(), Assets.frecciaSX.getPosXa(), Assets.frecciaSX.getPosYa(), Assets.correctWidth(Assets.frecciaSX.getWidth()), Assets.correctHeight(Assets.frecciaSX.getHeight()), null); 
			isActiveLeftArrow = true;
		} else {
			isActiveLeftArrow = false;
		}
		cont = page * 5;
		//nel caso in cui ci trovassimo nell'ultima pagina, ne mancano il resto della divisione del totale per 5
		if(page==numPage && tot%5!=0) numToCont = tot%5 + cont;
		//altrimenti si incrementa sempre di 5 (anche per l'ultima pagina)
		else numToCont = 5 + cont;
		//fine paginazione
		
		//il ciclo che mostra i risultati max 5 per pagina
		for(int i = cont; i<numToCont; i++) {
			g.drawImage(Assets.button.getImage(), Assets.button.getPosXa(), Assets.button.getPosYa() + offsetButtonY, Assets.correctWidth(Assets.button.getWidth()), Assets.correctHeight(Assets.button.getHeight()), null);
			TextContainer caption = new TextContainer(Game.m.protagonista.getInventario().get(i).getSigla(), Assets.button.getPosXa() + Assets.correctWidth(50), Assets.button.getPosYa() + Assets.correctHeight(30) + offsetButtonY, Assets.correctWidth(Assets.button.getWidth()) - Assets.correctWidth(20), Assets.correctHeight(Assets.button.getHeight()) - Assets.correctHeight(30), fontDescr, new Color(255,255,255,255));
			caption.paint(g);
			offsetButtonY += Assets.correctHeight(125);
		}
		offsetButtonY  = 0; //serve per garantire la prima posizione al primo oggetto della pagina successiva (azzerando l'offset verticale di posizionamento)
		
		//controllo sui pulsanti
		//pulsante 1
		if(Assets.button.isDetected(CustomMouseListener.mc)) {
			incrementoDesc = cont;
		}
		//pulsante 2
		if(Assets.button1.isDetected(CustomMouseListener.mc)) {
			if(numToCont-cont >= 2) incrementoDesc = cont + 1;
		}
		//pulsante 3
		if(Assets.button2.isDetected(CustomMouseListener.mc)) {
			if(numToCont-cont >= 3) incrementoDesc = cont + 2;
		}
		//pulsante 4
		if(Assets.button3.isDetected(CustomMouseListener.mc)) {
			if(numToCont-cont >= 4) incrementoDesc = cont + 3;	
		}
		//pulsante 5
		if(Assets.button4.isDetected(CustomMouseListener.mc)) {
			if(numToCont-cont >= 5) incrementoDesc = cont + 4;
		}
		//fine controllo sui pulsanti
		
		//controlli sull'equipaggiamento attivo/non attivo
		if(Game.m.protagonista.getInventario().get(incrementoDesc) instanceof StuffEquipaggiabile) {
			if(Game.m.protagonista.getInventario().get(incrementoDesc) instanceof Arma) {
				if(Game.m.protagonista.getArma() == null) testoAggiuntivo = "";
				else if(Game.m.protagonista.getArma().getNome().equals(Game.m.protagonista.getInventario().get(incrementoDesc).getNome()) || (Game.m.protagonista.getArmaScudo() != null && Game.m.protagonista.getArmaScudo().getNome().equals(Game.m.protagonista.getInventario().get(incrementoDesc).getNome()))) {
					testoAggiuntivo = " [Equipaggiato]";
				} else testoAggiuntivo = "";
			}
			if(Game.m.protagonista.getInventario().get(incrementoDesc) instanceof Scudo) {
				if(Game.m.protagonista.getArmaScudo() == null) testoAggiuntivo = "";
				else if(Game.m.protagonista.getArmaScudo().getNome().equals(Game.m.protagonista.getInventario().get(incrementoDesc).getNome())) {
					testoAggiuntivo = " [Equipaggiato]";
				} else testoAggiuntivo = "";
			}
			if(Game.m.protagonista.getInventario().get(incrementoDesc) instanceof Armatura) {
				if(Game.m.protagonista.getArmatura() == null) testoAggiuntivo = "";
				else if(Game.m.protagonista.getArmatura().getNome().equals(Game.m.protagonista.getInventario().get(incrementoDesc).getNome())) {
					testoAggiuntivo = " [Equipaggiato]";
				} else testoAggiuntivo = "";
			}
			if(Game.m.protagonista.getInventario().get(incrementoDesc) instanceof Accessorio) {
				if(Game.m.protagonista.getAccessorio() == null) testoAggiuntivo = "";
				else if(Game.m.protagonista.getAccessorio().getNome().equals(Game.m.protagonista.getInventario().get(incrementoDesc).getNome())) {
					testoAggiuntivo = " [Equipaggiato]";
				} else testoAggiuntivo = "";
			}
		} else testoAggiuntivo = "";
		
		//box descrizione con info
		g.drawImage(Assets.boxDescrizione.getImage(), Assets.boxDescrizione.getPosXa(), Assets.boxDescrizione.getPosYa(), Assets.correctWidth(Assets.boxDescrizione.getWidth()), Assets.correctHeight(Assets.boxDescrizione.getHeight()), null);
		TextContainer title = new TextContainer(Game.m.protagonista.getInventario().get(incrementoDesc).getNome() + " (" + Game.m.protagonista.getInventario().get(incrementoDesc).getQnt() + ")" + testoAggiuntivo, Assets.boxDescrizione.getPosXa() + Assets.correctWidth(100), Assets.boxDescrizione.getPosYa() + Assets.correctHeight(100), Assets.correctWidth(Assets.boxDescrizione.getWidth()) - Assets.correctWidth(100), Assets.correctHeight(Assets.boxDescrizione.getHeight()) - Assets.correctHeight(100), fontTitle, new Color(255,255,255,255));
		title.paint(g);
		TextContainer descr = new TextContainer(Game.m.protagonista.getInventario().get(incrementoDesc).getDescrizione(), Assets.boxDescrizione.getPosXa() + Assets.correctWidth(100), Assets.boxDescrizione.getPosYa() + Assets.correctHeight(300), Assets.correctWidth(Assets.boxDescrizione.getWidth()) - Assets.correctWidth(100), Assets.correctHeight(Assets.boxDescrizione.getHeight()) - Assets.correctHeight(100), fontDescr, new Color(255,255,255,255));
		descr.paint(g);
	}
	
	protected void inventarioAbilita(Graphics g) {
		//box di sfondo
		g.drawImage(Assets.boxInventario.getImage(), Assets.boxInventario.getPosXa(), Assets.boxInventario.getPosYa(), Assets.correctWidth(Assets.boxInventario.getWidth()), Assets.correctHeight(Assets.boxInventario.getHeight()), null);
		g.drawImage(Assets.iconaEsci.getImage(), Assets.iconaEsci.getPosXa(), Assets.iconaEsci.getPosYa(), Assets.correctWidth(Assets.iconaEsci.getWidth()), Assets.correctHeight(Assets.iconaEsci.getHeight()), null);
		tot = Game.m.protagonista.getAbilita().size(); //numero effettivo totale di oggetti attivi
		if(tot==0) return;
		//paginazione
		//effettuo questi controlli solo la prima volta (per questo utilizzo il booleano firstTime)
		if(firstTime) {
			//incremento il numero di pagine almeno di 1 se il totale di valori è maggiore di 5
			if(tot > 5) {
				numPage++;
			}
			//comincio ad incrementare dal secondo valore in poi per multipli di 5 per scoprire il numero di pagine
			while(true) {
				if(tot-5 > 5) {
					numPage++;
					tot-=5;
				}
				else break;
			}
			firstTime = false;
		}
		//verifico quando devo mostrare le frecce
		if((page==0 && numPage!=0) || (page>0 && numPage>page)) {
			g.drawImage(Assets.frecciaDX.getImage(), Assets.frecciaDX.getPosXa(), Assets.frecciaDX.getPosYa(), Assets.correctWidth(Assets.frecciaDX.getWidth()), Assets.correctHeight(Assets.frecciaDX.getHeight()), null); 
			isActiveRightArrow = true;
		} else {
			isActiveRightArrow = false;
		}
		if(page!=0 && numPage>=page) {
			g.drawImage(Assets.frecciaSX.getImage(), Assets.frecciaSX.getPosXa(), Assets.frecciaSX.getPosYa(), Assets.correctWidth(Assets.frecciaSX.getWidth()), Assets.correctHeight(Assets.frecciaSX.getHeight()), null); 
			isActiveLeftArrow = true;
		} else {
			isActiveLeftArrow = false;
		}
		cont = page * 5;
		//nel caso in cui ci trovassimo nell'ultima pagina, ne mancano il resto della divisione del totale per 5
		if(page==numPage && tot%5!=0) numToCont = tot%5 + cont;
		//altrimenti si incrementa sempre di 5 (anche per l'ultima pagina)
		else numToCont = 5 + cont;
		//fine paginazione
		
		//il ciclo che mostra i risultati max 5 per pagina
		for(int i = cont; i<numToCont; i++) {
			g.drawImage(Assets.button.getImage(), Assets.button.getPosXa(), Assets.button.getPosYa() + offsetButtonY, Assets.correctWidth(Assets.button.getWidth()), Assets.correctHeight(Assets.button.getHeight()), null);
			TextContainer caption = new TextContainer(Game.m.protagonista.getAbilita().get(i).getSigla(), Assets.button.getPosXa() + Assets.correctWidth(50), Assets.button.getPosYa() + Assets.correctHeight(30) + offsetButtonY, Assets.correctWidth(Assets.button.getWidth()) - Assets.correctWidth(20), Assets.correctHeight(Assets.button.getHeight()) - Assets.correctHeight(30), fontDescr, new Color(255,255,255,255));
			caption.paint(g);
			offsetButtonY += Assets.correctHeight(125);
		}
		offsetButtonY  = 0; //serve per garantire la prima posizione al primo oggetto della pagina successiva (azzerando l'offset verticale di posizionamento)
		
		//controllo sui pulsanti
		//pulsante 1
		if(Assets.button.isDetected(CustomMouseListener.mc)) {
			incrementoDesc = cont;
		}
		//pulsante 2
		if(Assets.button1.isDetected(CustomMouseListener.mc)) {
			if(numToCont-cont >= 2) incrementoDesc = cont + 1;
		}
		//pulsante 3
		if(Assets.button2.isDetected(CustomMouseListener.mc)) {
			if(numToCont-cont >= 3) incrementoDesc = cont + 2;
		}
		//pulsante 4
		if(Assets.button3.isDetected(CustomMouseListener.mc)) {
			if(numToCont-cont >= 4) incrementoDesc = cont + 3;	
		}
		//pulsante 5
		if(Assets.button4.isDetected(CustomMouseListener.mc)) {
			if(numToCont-cont >= 5) incrementoDesc = cont + 4;
		}
		//fine controllo sui pulsanti
		
		//parte di destra
		g.drawImage(Assets.boxDescrizione.getImage(), Assets.boxDescrizione.getPosXa(), Assets.boxDescrizione.getPosYa(), Assets.correctWidth(Assets.boxDescrizione.getWidth()), Assets.correctHeight(Assets.boxDescrizione.getHeight()), null);
		TextContainer title = new TextContainer(Game.m.protagonista.getAbilita().get(incrementoDesc).getNome(), Assets.boxDescrizione.getPosXa() + Assets.correctWidth(100), Assets.boxDescrizione.getPosYa() + Assets.correctHeight(100), Assets.correctWidth(Assets.boxDescrizione.getWidth()) - Assets.correctWidth(100), Assets.correctHeight(Assets.boxDescrizione.getHeight()) - Assets.correctHeight(100), fontTitle, new Color(255,255,255,255));
		title.paint(g);
		TextContainer descr = new TextContainer(Game.m.protagonista.getAbilita().get(incrementoDesc).getDescrizione(), Assets.boxDescrizione.getPosXa() + Assets.correctWidth(100), Assets.boxDescrizione.getPosYa() + Assets.correctHeight(200), Assets.correctWidth(Assets.boxDescrizione.getWidth()) - Assets.correctWidth(100), Assets.correctHeight(Assets.boxDescrizione.getHeight()) - Assets.correctHeight(100), fontDescr, new Color(255,255,255,255));
		descr.paint(g);	
	}
	protected void stats(Graphics g) {
		g.drawImage(Assets.stats.getImage(), Assets.stats.getPosXa(), Assets.stats.getPosYa(), Assets.correctWidth(Assets.stats.getWidth()), Assets.correctHeight(Assets.stats.getHeight()), null);
		g.drawImage(Assets.plus.getImage(), Assets.plus.getPosXa(), Assets.plus.getPosYa(), Assets.correctWidth(Assets.plus.getWidth()), Assets.correctHeight(Assets.plus.getHeight()), null);
		TextContainer pv = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getPv()), Assets.stats.getPosXb() + Assets.correctWidth(10), Assets.stats.getPosYa(), Assets.correctWidth(50), Assets.correctHeight(50), fontDescr, new Color(255, 255, 255, 255)); 
		pv.paint(g);
		TextContainer gold = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getGold()), Assets.stats.getPosXb() + Assets.correctWidth(10), Assets.stats.getPosYa() + Assets.correctHeight(78), Assets.correctWidth(300), Assets.correctHeight(50), fontDescr, new Color(255, 255, 255, 255)); 
		gold.paint(g);
		TextContainer exp = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getExp()), Assets.stats.getPosXb() + Assets.correctWidth(10), Assets.stats.getPosYa() + Assets.correctHeight(162), Assets.correctWidth(300), Assets.correctHeight(50), fontDescr, new Color(255, 255, 255, 255)); 
		exp.paint(g);
	}
	protected void statsAvanzate(Graphics g) {
		g.drawImage(Assets.boxStats.getImage(), Assets.boxStats.getPosXa(), Assets.boxStats.getPosYa(), Assets.correctWidth(Assets.boxStats.getWidth()), Assets.correctHeight(Assets.boxStats.getHeight()), null);
		g.drawImage(Assets.esciBoxStats.getImage(), Assets.esciBoxStats.getPosXa(), Assets.esciBoxStats.getPosYa(), Assets.correctWidth(Assets.esciBoxStats.getWidth()), Assets.correctHeight(Assets.esciBoxStats.getHeight()), null);
		TextContainer box1 = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getPv()) + "/" + RoutineSystem.rimuoviZero(Game.m.protagonista.getPvMax()), Assets.boxStats.getPosXa() + Assets.correctWidth(78), Assets.boxStats.getPosYa() + Assets.correctHeight(17), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box1.paint(g);
		TextContainer box2 = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getPm()) + "/" + RoutineSystem.rimuoviZero(Game.m.protagonista.getPmMax()), Assets.boxStats.getPosXa() + Assets.correctWidth(240), Assets.boxStats.getPosYa() + Assets.correctHeight(17), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box2.paint(g);
		TextContainer box3 = new TextContainer("+"+RoutineSystem.rimuoviZero(Game.m.protagonista.getForz()) + " +" + RoutineSystem.rimuoviZero(Game.m.protagonista.getBonusforz()), Assets.boxStats.getPosXa() + Assets.correctWidth(410), Assets.boxStats.getPosYa() + Assets.correctHeight(17), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box3.paint(g);
		TextContainer box4 = new TextContainer("+"+RoutineSystem.rimuoviZero(Game.m.protagonista.getDif()) + " +" + RoutineSystem.rimuoviZero(Game.m.protagonista.getBonusdif()), Assets.boxStats.getPosXa() + Assets.correctWidth(78), Assets.boxStats.getPosYa() + Assets.correctHeight(80), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box4.paint(g);
		TextContainer box5 = new TextContainer("+"+RoutineSystem.rimuoviZero(Game.m.protagonista.getDes()) + " +" + RoutineSystem.rimuoviZero(Game.m.protagonista.getBonusdes()), Assets.boxStats.getPosXa() + Assets.correctWidth(240), Assets.boxStats.getPosYa() + Assets.correctHeight(80), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box5.paint(g);
		TextContainer box6 = new TextContainer("+"+RoutineSystem.rimuoviZero(Game.m.protagonista.getAmag()) + " +" + RoutineSystem.rimuoviZero(Game.m.protagonista.getBonusamag()), Assets.boxStats.getPosXa() + Assets.correctWidth(410), Assets.boxStats.getPosYa() + Assets.correctHeight(80), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box6.paint(g);
		TextContainer box7 = new TextContainer("+"+RoutineSystem.rimuoviZero(Game.m.protagonista.getDmag()) + " +" + RoutineSystem.rimuoviZero(Game.m.protagonista.getBonusdmag()), Assets.boxStats.getPosXa() + Assets.correctWidth(78), Assets.boxStats.getPosYa() + Assets.correctHeight(137), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box7.paint(g);
		TextContainer box8 = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getFort()), Assets.boxStats.getPosXa() + Assets.correctWidth(240), Assets.boxStats.getPosYa() + Assets.correctHeight(137), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box8.paint(g);
		TextContainer box9 = new TextContainer("+"+RoutineSystem.rimuoviZero(Game.m.protagonista.getCrit()) + " +" + RoutineSystem.rimuoviZero(Game.m.protagonista.getBonuscrit()), Assets.boxStats.getPosXa() + Assets.correctWidth(410), Assets.boxStats.getPosYa() + Assets.correctHeight(137), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box9.paint(g);
		TextContainer box10 = new TextContainer(""+Game.m.protagonista.getDcrit(), Assets.boxStats.getPosXa() + Assets.correctWidth(78), Assets.boxStats.getPosYa() + Assets.correctHeight(197), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box10.paint(g);
		TextContainer box11 = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getExp()), Assets.boxStats.getPosXa() + Assets.correctWidth(240), Assets.boxStats.getPosYa() + Assets.correctHeight(197), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box11.paint(g);
		TextContainer box12 = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getGold()), Assets.boxStats.getPosXa() + Assets.correctWidth(410), Assets.boxStats.getPosYa() + Assets.correctHeight(197), Assets.correctWidth(100), Assets.correctHeight(50), fontBoxStats, new Color(0, 0, 0, 255)); 
		box12.paint(g);
	}
}
