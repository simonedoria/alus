package States;

import Alus.*;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Questa classe che estende {@code State} viene renderizzata la cut-scene finale.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class ScenaFinale extends State {
	
	private boolean messaggioLogo = true, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12;
	/**
 	* COSTRUTTORE <br>
 	* Inizializza larghezza e altezza dello schermo.
 	*/
	public ScenaFinale() {
		this.width = ((int) tk.getScreenSize().getWidth());
		this.height = ((int) tk.getScreenSize().getHeight());
	}
	/**
 	* Metodo che prende come parametro un oggetto di tipo {@code Graphics} e se ne serve per disegnare nel canvas all'interno del {@code Display}. 
 	*/
	@Override
	public void render(Graphics g) {
		if(timing1 > 0) timing1--;
		if(timing2 > 0) timing2--;
		if(timing3 > 0) timing3--;
		if(timing4 > 0) timing4--;
		if(timing5 > 0) timing5--;
		if(timing6 > 0) timing6--;
		if(timing7 > 0) timing7--;
		if(timing8 > 0) timing8--;
		if(timing9 > 0) timing9--;
		if(timing10 > 0) timing10--;
		if(timing11 > 0) timing11--;
		if(timing12 > 0) timing12--;
		if(timing13 > 0) timing13--;
		if(timing14 > 0) timing14--;
		if(timing15 > 0) timing15--;
		if(timing16 > 0) timing16--;
		if(timing17 > 0) timing17--;
		if(timing18 > 0) timing18--;
		if(timing19 > 0) timing19--;
		if(timing20 > 0) timing20--;
		if(timing21 > 0) timing21--;
		if(timing22 > 0) timing22--;
		if(timing23 > 0) timing23--;
		if(timing24 > 0) timing24--;
		if(timing25 > 0) timing25--;
		if(timing26 > 0) timing26--;
		if(timing27 > 0) timing27--;
		
		if(messaggioLogo) g.drawImage(Assets.messaggioLogo.getImage(), 0, 0, width, height, null);
		if(m1) g.drawImage(Assets.m1.getImage(), 0, 0, width, height, null);
		if(m2) g.drawImage(Assets.m2.getImage(), 0, 0, width, height, null);
		if(m3) g.drawImage(Assets.m3.getImage(), 0, 0, width, height, null);
		if(m4) g.drawImage(Assets.m4.getImage(), 0, 0, width, height, null);
		if(m5) g.drawImage(Assets.m5.getImage(), 0, 0, width, height, null);
		if(m6) g.drawImage(Assets.m6.getImage(), 0, 0, width, height, null);
		if(m7) g.drawImage(Assets.m7.getImage(), 0, 0, width, height, null);
		if(m8) g.drawImage(Assets.m8.getImage(), 0, 0, width, height, null);
		if(m9) g.drawImage(Assets.m9.getImage(), 0, 0, width, height, null);
		if(m10) g.drawImage(Assets.m10.getImage(), 0, 0, width, height, null);
		if(m11) g.drawImage(Assets.m11.getImage(), 0, 0, width, height, null);
		if(m12) g.drawImage(Assets.m12.getImage(), 0, 0, width, height, null);
		
		if(timing1==0) {
			if(fading>0) fading-=5;
		}
		if(timing2==0) {
			timing1=-1;
			messaggioLogo=true;
			if(fading<255) fading+=15;
		}
		if(timing3==0) {
			timing2=-1;
			m1=true;
			if(fading>0) fading-=5;
		}
		if(timing4==0) {
			timing3=-1;
			if(fading<255) fading+=15;
		}
		if(timing5==0) {
			timing4=-1;
			m2=true;
			if(fading>0) fading-=5;
		}
		if(timing6==0) {
			timing5=-1;
			if(fading<255) fading+=15;
		}
		if(timing7==0) {
			timing6=-1;
			m3=true;
			if(fading>0) fading-=5;
		}
		if(timing8==0) {
			timing7=-1;
			if(fading<255) fading+=15;
		}
		if(timing9==0) {
			timing8=-1;
			m4=true;
			if(fading>0) fading-=5;
		}
		if(timing10==0) {
			timing9=-1;
			if(fading<255) fading+=15;
		}
		if(timing11==0) {
			timing10=-1;
			m5=true;
			if(fading>0) fading-=5;
		}
		if(timing12==0) {
			timing11=-1;
			if(fading<255) fading+=15;
		}
		if(timing13==0) {
			timing12=-1;
			m6=true;
			if(fading>0) fading-=5;
		}
		if(timing14==0) {
			timing13=-1;
			if(fading<255) fading+=15;
		}
		if(timing15==0) {
			timing14=-1;
			m7=true;
			if(fading>0) fading-=5;
		}
		if(timing16==0) {
			timing15=-1;
			if(fading<255) fading+=15;
		}
		if(timing17==0) {
			timing16=-1;
			m8=true;
			if(fading>0) fading-=5;
		}
		if(timing18==0) {
			timing17=-1;
			if(fading<255) fading+=15;
		}
		if(timing19==0) {
			timing18=-1;
			m9=true;
			if(fading>0) fading-=5;
		}
		if(timing20==0) {
			timing19=-1;
			if(fading<255) fading+=15;
		}
		if(timing21==0) {
			timing20=-1;
			m10=true;
			if(fading>0) fading-=5;
		}
		if(timing22==0) {
			timing21=-1;
			if(fading<255) fading+=15;
		}
		if(timing23==0) {
			timing22=-1;
			m11=true;
			if(fading>0) fading-=5;
		}
		if(timing24==0) {
			timing23=-1;
			if(fading<255) fading+=15;
		}
		if(timing25==0) {
			timing24=-1;
			m12=true;
			if(fading>0) fading-=5;
		}
		if(timing26==0) {
			timing25=-1;
			if(fading<255) fading+=15;
		}
		if(timing27==0) {
			timing26=-1;
			Assets.mute();
			Game.creditiState = new CreditiState();
			State.getState().setState(Game.creditiState);
			Assets.crediti.start();
		}
		g.setColor(new Color(0,0,0,fading));
		g.fillRect(0, 0, width, height);
	}

}













