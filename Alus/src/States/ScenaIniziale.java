package States;

import Alus.*;
import Mappe.Mappa1;

import java.awt.Color;
import java.awt.Graphics;


/**
 * Questa classe che estende {@code State} viene renderizzata la cut-scene iniziale.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class ScenaIniziale extends State {
	
	private boolean messaggio1 = true, messaggio2, messaggio3, messaggioLogo;
	/**
 	* COSTRUTTORE <br>
 	* Inizializza larghezza e altezza dello schermo.
 	*/
	public ScenaIniziale() {
		this.width = ((int) tk.getScreenSize().getWidth());
		this.height = ((int) tk.getScreenSize().getHeight());
	}
	
	/**
 	* Metodo che prende come parametro un oggetto di tipo {@code Graphics} e se ne serve per disegnare nel canvas all'interno del {@code Display}. 
 	*/
	@Override
	public void render(Graphics g) {
		if(timing1 > 0) timing1--;
		if(timing2 > 0) timing2--;
		if(timing3 > 0) timing3--;
		if(timing4 > 0) timing4--;
		if(timing5 > 0) timing5--;
		if(timing6 > 0) timing6--;
		if(timing7 > 0) timing7--;
		if(timing8 > 0) timing8--;
		if(timing9 > 0) timing9--;
		
		if(messaggio1) g.drawImage(Assets.messaggio1.getImage(), 0, 0, width, height, null);
		if(messaggio2) g.drawImage(Assets.messaggio2.getImage(), 0, 0, width, height, null);
		if(messaggio3) g.drawImage(Assets.messaggio3.getImage(), 0, 0, width, height, null);
		if(messaggioLogo) g.drawImage(Assets.messaggioLogo.getImage(), 0, 0, width, height, null);
		if(timing1==0) {
			if(fading>0) fading-=5;
		}
		if(timing2==0) {
			timing1=-1;
			messaggio1=true;
			if(fading<255) fading+=5;
		}
		if(timing3==0) {
			timing2=-1;
			messaggio2=true;
			if(fading>0) fading-=5;
		}
		if(timing4==0) {
			timing3=-1;
			if(fading<255) fading+=5;
		}
		if(timing5==0) {
			timing4=-1;
			messaggio3=true;
			if(fading>0) fading-=5;
		}
		if(timing6==0) {
			timing5=-1;
			if(fading<255) fading+=5;
		}
		if(timing7==0) {
			timing6=-1;
			messaggioLogo=true;
			if(fading>0) fading--;
		}
		if(timing8==0) {
			timing7=-1;
			if(fading<255) fading+=5;
		}
		if(timing9==0) {
			Assets.mute();
			Assets.prigioneTheme.start();
			Game.m = new Mappa1();
			Game.gameState = new GameState();
			State.getState().setState(Game.gameState);
			Game.display.panel.setVisible(true);
		}
		g.setColor(new Color(0,0,0,fading));
		g.fillRect(0, 0, width, height);
	}

}
