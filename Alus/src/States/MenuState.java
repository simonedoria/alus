package States;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import Alus.*;
import Assets.Listeners.CustomMouseListener;
import System.MemoryCard;
import System.RoutineSystem;
/**
 * Questa classe che estende {@code State} viene renderizzato il menu iniziale insieme ai settaggi di carica partita.
 * @see MemoryCard
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class MenuState extends State {
	//settaggi box caricaPartita
	private Font fontSalvataggio = new Font("Arial", Font.PLAIN, correctFontSize(25));
	/**
 	* COSTRUTTORE <br>
 	* Inizializza larghezza e altezza dello schermo.<br>
 	* Inizializza la lista dei salvataggi.<br>
 	* Inizializza e fa partite il mainTheme iniziale.
 	* @see Audio
 	*/
	public MenuState() {
		this.width = ((int) tk.getScreenSize().getWidth());
		this.height = ((int) tk.getScreenSize().getHeight());
		listaSalvataggi=new ArrayList<>();
		try {
			listaSalvataggi();
		} catch (IOException e) {
			e.printStackTrace();
		}
		initAudioLoop();
	}
	
	private void initAudioLoop() {
		Assets.mute();
		Assets.mainTheme.start();
	}
	/**
 	* Metodo che prende come parametro un oggetto di tipo {@code Graphics} e se ne serve per disegnare nel canvas all'interno del {@code Display}. 
 	*/
	@Override
	public void render(Graphics g) {
		g.drawImage(Assets.sfondo.getImage(), 0, 0, width, height, null); //sfondo che si adatta alla finestra
		g.drawImage(Assets.logo.getImage(),    Assets.logo.getPosXa(),    Assets.logo.getPosYa(), 	 Assets.correctWidth(Assets.logo.getWidth()), 	  Assets.correctHeight(Assets.logo.getHeight()), 	null);
		if(!isActiveCarica && !isActiveCredits) redrawHover(g);
		if(isActiveCredits) credits(g);
		if((fading!=0 && !isActiveCarica)) fading--;
		g.setColor(new Color(0,0,0,fading));
		g.fillRect(0, 0, width, height);
		if(isActiveCarica) caricaPartita(g);
		g.drawImage(Assets.cursor.getImage(), CustomMouseListener.x, CustomMouseListener.y, 32, 32,  null);
	}
	private void redrawHover(Graphics g) {
		if(CustomMouseListener.me == null) {
			g.drawImage(Assets.esci.getImage(), Assets.esci.getPosXa(), Assets.esci.getPosYa(), Assets.correctWidth(Assets.esci.getWidth()), Assets.correctHeight(Assets.esci.getHeight()), null);
			g.drawImage(Assets.newG.getImage(),    Assets.newG.getPosXa(),    Assets.newG.getPosYa(), 	 Assets.correctWidth(Assets.newG.getWidth()), 	  Assets.correctHeight(Assets.newG.getHeight()), 	null);
			g.drawImage(Assets.load.getImage(),    Assets.load.getPosXa(),    Assets.load.getPosYa(), 	 Assets.correctWidth(Assets.load.getWidth()), 	  Assets.correctHeight(Assets.load.getHeight()),  	null);
			g.drawImage(Assets.credits.getImage(), Assets.credits.getPosXa(), Assets.credits.getPosYa(), Assets.correctWidth(Assets.credits.getWidth()), Assets.correctHeight(Assets.credits.getHeight()),null);
			return;
		}
		//pulsante nuova
		if(Assets.newG.isDetected(CustomMouseListener.me)) { 
			g.drawImage(Assets.newGOver.getImage(),    Assets.newG.getPosXa(),    Assets.newG.getPosYa(), 	 Assets.correctWidth(Assets.newG.getWidth()), 	  Assets.correctHeight(Assets.newG.getHeight()), 	null);
			g.drawImage(Assets.load.getImage(),    Assets.load.getPosXa(),    Assets.load.getPosYa(), 	 Assets.correctWidth(Assets.load.getWidth()), 	  Assets.correctHeight(Assets.load.getHeight()),  	null);
			g.drawImage(Assets.credits.getImage(), Assets.credits.getPosXa(), Assets.credits.getPosYa(), Assets.correctWidth(Assets.credits.getWidth()), Assets.correctHeight(Assets.credits.getHeight()),null);
			g.drawImage(Assets.esci.getImage(), Assets.esci.getPosXa(), Assets.esci.getPosYa(), Assets.correctWidth(Assets.esci.getWidth()), Assets.correctHeight(Assets.esci.getHeight()), null);
		}
		//pulsante load
		else if(Assets.load.isDetected(CustomMouseListener.me)) { 
			g.drawImage(Assets.loadOver.getImage(),    Assets.load.getPosXa(),    Assets.load.getPosYa(), 	 Assets.correctWidth(Assets.load.getWidth()), 	  Assets.correctHeight(Assets.load.getHeight()),  	null); 
			g.drawImage(Assets.newG.getImage(),    Assets.newG.getPosXa(),    Assets.newG.getPosYa(), 	 Assets.correctWidth(Assets.newG.getWidth()), 	  Assets.correctHeight(Assets.newG.getHeight()), 	null);
			g.drawImage(Assets.credits.getImage(), Assets.credits.getPosXa(), Assets.credits.getPosYa(), Assets.correctWidth(Assets.credits.getWidth()), Assets.correctHeight(Assets.credits.getHeight()),null);
			g.drawImage(Assets.esci.getImage(), Assets.esci.getPosXa(), Assets.esci.getPosYa(), Assets.correctWidth(Assets.esci.getWidth()), Assets.correctHeight(Assets.esci.getHeight()), null);
		}
		//pulsante credits
		else if(Assets.credits.isDetected(CustomMouseListener.me)) {
			g.drawImage(Assets.creditsOver.getImage(), Assets.credits.getPosXa(), Assets.credits.getPosYa(), Assets.correctWidth(Assets.credits.getWidth()), Assets.correctHeight(Assets.credits.getHeight()),null); 
			g.drawImage(Assets.newG.getImage(),    Assets.newG.getPosXa(),    Assets.newG.getPosYa(), 	 Assets.correctWidth(Assets.newG.getWidth()), 	  Assets.correctHeight(Assets.newG.getHeight()), 	null);
			g.drawImage(Assets.load.getImage(),    Assets.load.getPosXa(),    Assets.load.getPosYa(), 	 Assets.correctWidth(Assets.load.getWidth()), 	  Assets.correctHeight(Assets.load.getHeight()),  	null);
			g.drawImage(Assets.esci.getImage(), Assets.esci.getPosXa(), Assets.esci.getPosYa(), Assets.correctWidth(Assets.esci.getWidth()), Assets.correctHeight(Assets.esci.getHeight()), null);
		}
		//pulsante esci
		else if(Assets.esci.isDetected(CustomMouseListener.me)) {
			g.drawImage(Assets.esciOver.getImage(), Assets.esci.getPosXa(), Assets.esci.getPosYa(), Assets.correctWidth(Assets.esci.getWidth()), Assets.correctHeight(Assets.esci.getHeight()), null);
			g.drawImage(Assets.newG.getImage(),    Assets.newG.getPosXa(),    Assets.newG.getPosYa(), 	 Assets.correctWidth(Assets.newG.getWidth()), 	  Assets.correctHeight(Assets.newG.getHeight()), 	null);
			g.drawImage(Assets.load.getImage(),    Assets.load.getPosXa(),    Assets.load.getPosYa(), 	 Assets.correctWidth(Assets.load.getWidth()), 	  Assets.correctHeight(Assets.load.getHeight()),  	null);
			g.drawImage(Assets.credits.getImage(), Assets.credits.getPosXa(), Assets.credits.getPosYa(), Assets.correctWidth(Assets.credits.getWidth()), Assets.correctHeight(Assets.credits.getHeight()),null);
		} else {
			g.drawImage(Assets.esci.getImage(), Assets.esci.getPosXa(), Assets.esci.getPosYa(), Assets.correctWidth(Assets.esci.getWidth()), Assets.correctHeight(Assets.esci.getHeight()), null);
			g.drawImage(Assets.newG.getImage(),    Assets.newG.getPosXa(),    Assets.newG.getPosYa(), 	 Assets.correctWidth(Assets.newG.getWidth()), 	  Assets.correctHeight(Assets.newG.getHeight()), 	null);
			g.drawImage(Assets.load.getImage(),    Assets.load.getPosXa(),    Assets.load.getPosYa(), 	 Assets.correctWidth(Assets.load.getWidth()), 	  Assets.correctHeight(Assets.load.getHeight()),  	null);
			g.drawImage(Assets.credits.getImage(), Assets.credits.getPosXa(), Assets.credits.getPosYa(), Assets.correctWidth(Assets.credits.getWidth()), Assets.correctHeight(Assets.credits.getHeight()),null);
		}
	}
	private void caricaPartita(Graphics g) {
		//fine controllo sui pulsanti
		g.drawImage(Assets.boxCarica.getImage(), Assets.boxCarica.getPosXa(), Assets.boxCarica.getPosYa(), Assets.correctWidth(Assets.boxCarica.getWidth()), Assets.correctHeight(Assets.boxCarica.getHeight()), null);
		g.drawImage(Assets.esciCarica.getImage(), Assets.esciCarica.getPosXa(), Assets.esciCarica.getPosYa(), Assets.correctWidth(Assets.esciCarica.getWidth()), Assets.correctHeight(Assets.esciCarica.getHeight()), null);
			
		tot = listaSalvataggi.size();
		if(tot==0) return;
		//paginazione
		//effettuo questi controlli solo la prima volta (per questo utilizzo il booleano firstTime)
		if(firstTime) {
			//incremento il numero di pagine almeno di 1 se il totale di valori è maggiore di 4
			if(tot > 4) {
				numPage++;
			}
			//comincio ad incrementare dal secondo valore in poi per multipli di 4 per scoprire il numero di pagine
			while(true) {
				if(tot-4 > 4) {
					numPage++;
					tot-=4;
				}
				else break;
			}
			firstTime = false;
		}
		//verifico quando devo mostrare le frecce
		if((page==0 && numPage!=0) || (page>0 && numPage>page)) {
			g.drawImage(Assets.frecciaGiu.getImage(), Assets.frecciaGiu.getPosXa(), Assets.frecciaGiu.getPosYa(), Assets.correctWidth(Assets.frecciaGiu.getWidth()), Assets.correctHeight(Assets.frecciaGiu.getHeight()), null);
			isActiveDownArrow = true;
		} else {
			isActiveDownArrow = false;
		}
		if(page!=0 && numPage>=page) {
			g.drawImage(Assets.frecciaSu.getImage(), Assets.frecciaSu.getPosXa(), Assets.frecciaSu.getPosYa(), Assets.correctWidth(Assets.frecciaSu.getWidth()), Assets.correctHeight(Assets.frecciaSu.getHeight()), null);
			isActiveUpArrow = true;
		} else {
			isActiveUpArrow = false;
		}
		cont = page * 4;
		//nel caso in cui ci trovassimo nell'ultima pagina, ne mancano il resto della divisione del totale per 4
		if(page==numPage && tot%4!=0) numToCont = tot%4 + cont;
		//altrimenti si incrementa sempre di 5 (anche per l'ultima pagina)
		else numToCont = 4 + cont;
		//fine paginazione
		//il ciclo che mostra i risultati max 5 per pagina
		for(int i = cont; i<numToCont; i++) {
			g.drawImage(Assets.list1.getImage(), Assets.list1.getPosXa(), Assets.list1.getPosYa() + offsetButtonY, Assets.correctWidth(Assets.list1.getWidth()), Assets.correctHeight(Assets.list1.getHeight()), null);
			TextContainer caption = new TextContainer(listaSalvataggi.get(i), Assets.list1.getPosXa() + Assets.correctWidth(50), Assets.list1.getPosYa() + Assets.correctHeight(30) + offsetButtonY, Assets.correctWidth(Assets.boxCarica.getWidth()) + Assets.correctWidth(100), Assets.correctHeight(Assets.list1.getHeight()) - Assets.correctHeight(30), fontSalvataggio, new Color(255,255,255,255));
			caption.paint(g);
			offsetButtonY += Assets.correctHeight(124);
		}
		offsetButtonY  = 0; //serve per garantire la prima posizione al primo oggetto della pagina successiva (azzerando l'offset verticale di posizionamento)
	}
	private List<String> listaSalvataggi() throws IOException {
		//richiamo il path del JAR
		File f1 = new File(System.getProperty("java.class.path"));
		File dir = f1.getAbsoluteFile().getParentFile();
		String path = "";
		path += "src/Alus/res/salvataggi";
		if(RoutineSystem.isWindows()) {
			listaSalvataggi=new ArrayList<>();
			Files.walk(Paths.get(path)).forEach(filePath -> {
			    if (Files.isRegularFile(filePath)) {
			    	String [] sub = filePath.toString().split("\\\\");
			        String[] nome = sub[sub.length-1].split("\\.");
			        listaSalvataggi.add(nome[0]);
			    }
			});
		} else {
			listaSalvataggi=new ArrayList<>();
			Files.walk(Paths.get(path)).forEach(filePath -> {
			    if (Files.isRegularFile(filePath)) {
			    	String [] sub = filePath.toString().split("\\/");
			        String[] nome = sub[sub.length-1].split("\\.");
			        listaSalvataggi.add(nome[0]);
			    }
			});
		}
		
		return listaSalvataggi;
	}
	private void credits(Graphics g) {
		g.drawImage(Assets.team.getImage(), Assets.team.getPosXa(), Assets.team.getPosYa(), width, height, null);
		g.drawImage(Assets.indietro.getImage(), Assets.indietro.getPosXa(), Assets.indietro.getPosYa(), Assets.correctWidth(Assets.indietro.getWidth()), Assets.correctHeight(Assets.indietro.getHeight()), null);
	}
}
