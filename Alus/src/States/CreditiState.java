package States;

import Alus.*;
import java.awt.Color;
import java.awt.Graphics;

/**
 * Questa classe che estende {@code State} viene renderizzata la cut-scene finale dei crediti.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class CreditiState extends State {
	
	private boolean messaggioLogo = true, messaggioTeam;
	/**
 	* COSTRUTTORE <br>
 	* Inizializza larghezza e altezza dello schermo.
 	*/
	public CreditiState() {
		this.width = ((int) tk.getScreenSize().getWidth());
		this.height = ((int) tk.getScreenSize().getHeight());
	}
	/**
 	* Metodo che prende come parametro un oggetto di tipo {@code Graphics} e se ne serve per disegnare nel canvas all'interno del {@code Display}. 
 	*/
	@Override
	public void render(Graphics g) {
		if(timing1 > 0) timing1--;
		if(timing2 > 0) timing2--;
		if(timing3 > 0) timing3--;
		if(timing4 > 0) timing4--;
		
		if(messaggioLogo) g.drawImage(Assets.teamLogo.getImage(), 0, 0, width, height, null);
		if(messaggioTeam) g.drawImage(Assets.team.getImage(), 0, 0, width, height, null);
		
		if(timing1==0) {
			if(fading>0) fading-=5;
		}
		if(timing2==0) {
			timing1=-1;
			if(fading<255) fading+=5;
		}
		if(timing3==0) {
			timing2=-1;
			messaggioTeam=true;
			if(fading>0) fading-=5;
		}
		if(timing4==0) {
			Game.menuState = new MenuState();
			State.getState().setState(Game.menuState);
		}
		g.setColor(new Color(0,0,0,fading));
		g.fillRect(0, 0, width, height);
	}

}
