package States;

import java.awt.*;

import Alus.*;
import Assets.Listeners.CustomMouseListener;
import Mappe.*;

/**
 * Questa classe che estende {@code State} viene renderizzato il {@code GameState} insieme alla {@code Mappa}.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class GameState extends State {
	
	//set textOutput 
	private TextContainer textOutput;
	private Font fontTextOutput = new Font("MS Sans Serif", Font.PLAIN, correctFontSize(25));
	//fine set TextOutput
	
	/**
 	* COSTRUTTORE SENZA PARAMETRI<br>
 	* Inizializza larghezza e altezza dello schermo e fa partite la {@code Mappa}.
 	*/
	public GameState() {
		this.width = ((int) tk.getScreenSize().getWidth());
		this.height = ((int) tk.getScreenSize().getHeight());
		Game.m.play();
	}
	/**
 	* COSTRUTTORE CON PARAMETRI<br>
 	* Inizializza larghezza e altezza dello schermo e fa partire la {@code Mappa} passata per parametro.
 	*/
	public GameState(Mappa ma) {
		this.width = ((int) tk.getScreenSize().getWidth());
		this.height = ((int) tk.getScreenSize().getHeight());
		Game.m = ma;
		Game.m.play();
	}
	/**
 	* Metodo che prende come parametro un oggetto di tipo {@code Graphics} e se ne serve per disegnare nel canvas all'interno del {@code Display}. 
 	*/
	@Override
	public void render(Graphics g) {
		g.drawImage(Assets.sfondo.getImage(), 0, 0, width, height, null); //sfondo che si adatta alla finestra
		if(fadingTesto!=255) fadingTesto+=5;
		g.drawImage(Assets.boxTestoCentrale.getImage(), Assets.boxTestoCentrale.getPosXa(), Assets.boxTestoCentrale.getPosYa(), Assets.correctWidth(Assets.boxTestoCentrale.getWidth()), Assets.correctHeight(Assets.boxTestoCentrale.getHeight()), null);
		textOutput = new TextContainer(Game.m.getOutput(), Assets.boxTestoCentrale.getPosXa() + Assets.correctWidth(20), Assets.boxTestoCentrale.getPosYa()+20, Assets.correctWidth(Assets.boxTestoCentrale.getWidth()) - Assets.correctWidth(20), Assets.correctHeight(Assets.boxTestoCentrale.getHeight())*3, fontTextOutput, new Color(255,255,255,fadingTesto));
		textOutput.paint(g);
		stats(g);
		g.drawImage(Assets.iconaAb.getImage(), Assets.iconaAb.getPosXa(), Assets.iconaAb.getPosYa(), Assets.correctWidth(Assets.iconaAb.getWidth()), Assets.correctHeight(Assets.iconaAb.getHeight()), null);
		g.drawImage(Assets.iconaInv.getImage(), Assets.iconaInv.getPosXa(), Assets.iconaInv.getPosYa(), Assets.correctWidth(Assets.iconaInv.getWidth()), Assets.correctHeight(Assets.iconaInv.getHeight()), null);
		//gestione fading totale
		if(isActive3) statsAvanzate(g);
		if((fading!=0 && !isActive1) && (fading!=0 && !isActive2)) fading--;
		g.setColor(new Color(0,0,0,fading));
		g.fillRect(0, 0, width, height);
		if(isActive1) inventarioOggetti(g);
		if(isActive2) inventarioAbilita(g);
		//fine gestione fading totale
		g.drawImage(Assets.cursor.getImage(), CustomMouseListener.x, CustomMouseListener.y, 32, 32,  null);
	}
	
	
}
