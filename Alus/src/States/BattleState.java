package States;

import java.awt.*;

import Alus.*;
import Assets.Listeners.CustomMouseListener;
import System.RoutineSystem;
/**
 * Questa classe che estende {@code State} viene renderizzata la battaglia.
 * 
 * @author Giorgio De Giorgio
 * @author Simone Doria
 * @author Andrea Fabiano
 * @author Gian Marco Gulla'
 * @author Tommaso Ruga
 * @version 1.0
 *
 */
public class BattleState extends State {
	
	//set testoCentraleBattle
	TextContainer testoCentraleBattle;
	Font fontTextOutput = new Font("Arial", Font.PLAIN, correctFontSize(25));
	Font fontTestoCentraleBattle = new Font("Arial", Font.PLAIN, correctFontSize(25));
	//fine set testoCentraleBattle
	
	//settaggio font
	private Font fontStats = new Font("Arial", Font.BOLD, correctFontSize(20));
	private Font fontMana = new Font("Arial", Font.BOLD, correctFontSize(15));
	private Font fontStatus = new Font("Arial", Font.BOLD, correctFontSize(16));	
	
	private TextContainer pvSinistra;
	private TextContainer manaSinistra;
	private TextContainer pvDestra;
	
	//settaggi status
	private TextContainer statusText;
	private int offsetXa = 0;
	private int offsetYa = 0;
	private int offsetXb = 0;
	private int offsetYb = 0;
	/**
 	* COSTRUTTORE <br>
 	* Inizializza larghezza e altezza dello schermo.
 	*/
	public BattleState() {
		this.width = ((int) tk.getScreenSize().getWidth());
		this.height = ((int) tk.getScreenSize().getHeight());
	}
	/**
 	* Metodo che prende come parametro un oggetto di tipo {@code Graphics} e se ne serve per disegnare nel canvas all'interno del {@code Display}. 
 	*/
	@Override
	public void render(Graphics g) {
		//sfondo
		g.drawImage(Assets.bgBattle.getImage(), 0, 0, width, height, null);
		
		barreLotta(g);
		statusAttivi(g);
		
		//parte centrale
		g.drawImage(Assets.logoCentraleBattle.getImage(), Assets.logoCentraleBattle.getPosXa(), Assets.logoCentraleBattle.getPosYa(), Assets.correctWidth(Assets.logoCentraleBattle.getWidth()), Assets.correctHeight(Assets.logoCentraleBattle.getHeight()), null);

		//testo centrale output battaglia
		g.drawImage(Assets.boxTestoCentrale.getImage(), Assets.boxTestoCentrale.getPosXa(), Assets.boxTestoCentrale.getPosYa(), Assets.correctWidth(Assets.boxTestoCentrale.getWidth()), Assets.correctHeight(Assets.boxTestoCentrale.getHeight()), null);
		testoCentraleBattle = new TextContainer(Game.m.lotta.getTestoCentrale(), Assets.boxTestoCentrale.getPosXa() + Assets.correctWidth(20), Assets.boxTestoCentrale.getPosYa()+20, Assets.correctWidth(Assets.boxTestoCentrale.getWidth()) - Assets.correctWidth(20), Assets.correctHeight(Assets.boxTestoCentrale.getHeight())*3, fontTestoCentraleBattle, new Color(255,255,255,255));
		testoCentraleBattle.paint(g);
		g.drawImage(Assets.iconaAb.getImage(), Assets.iconaAb.getPosXa(), Assets.iconaAb.getPosYa(), Assets.correctWidth(Assets.iconaAb.getWidth()), Assets.correctHeight(Assets.iconaAb.getHeight()), null);
		g.drawImage(Assets.iconaInv.getImage(), Assets.iconaInv.getPosXa(), Assets.iconaInv.getPosYa(), Assets.correctWidth(Assets.iconaInv.getWidth()), Assets.correctHeight(Assets.iconaInv.getHeight()), null);
		//fading
		if((fading!=0 && !isActive1) && (fading!=0 && !isActive2)) fading--;
		g.setColor(new Color(0,0,0,fading));
		g.fillRect(0, 0, width, height);
		//settaggi inventario
		if(isActive1) inventarioOggetti(g);
		if(isActive2) inventarioAbilita(g);
		//cursore
		g.drawImage(Assets.cursor.getImage(), CustomMouseListener.x, CustomMouseListener.y, 32, 32,  null);
	}
	
	private void statusAttivi(Graphics g) {
		//verifico se devo andare a capo
		for(int i = 0; i<Game.m.protagonista.getStatusAttivi().size(); i++) {
			if(Assets.correctWidth(Assets.barraManaSinistra.getWidth()) - offsetXa < Assets.correctWidth(Assets.boxStatusS.getWidth())) {
				offsetYa += Assets.correctHeight(60);
				offsetXa = 0;
			}
			g.drawImage(Assets.boxStatusS.getImage(), Assets.boxStatusS.getPosXa() + offsetXa, Assets.boxStatusS.getPosYa() + offsetYa, Assets.correctWidth(Assets.boxStatusS.getWidth()), Assets.correctHeight(Assets.boxStatusS.getHeight()), null);
			statusText = new TextContainer(Game.m.protagonista.getStatusAttivi().get(i).getSigla(), Assets.boxStatusS.getPosXa() + Assets.correctWidth(10) + offsetXa, Assets.boxStatusS.getPosYa() + Assets.correctHeight(10) + offsetYa, Assets.correctWidth(Assets.boxStatusS.getWidth()) - Assets.correctWidth(5),Assets.correctHeight(Assets.boxStatusS.getHeight())  - Assets.correctHeight(5), fontStatus, Color.WHITE);
			statusText.paint(g);
			offsetXa += Assets.correctWidth(85);
		}
		//resetto gli offsets
		offsetXa = 0;
		offsetYa = 0;
		//come sopra questo vale per la parte destra
		for(int i = 0; i<Game.m.avversario.getStatusAttivi().size(); i++) {
			if(Assets.correctWidth(Assets.barraLottaDestra.getWidth()) - offsetXb < Assets.correctWidth(Assets.boxStatusD.getWidth())) {
				offsetYb += Assets.correctHeight(60);
				offsetXb = 0;
			}
			g.drawImage(Assets.boxStatusD.getImage(), Assets.boxStatusD.getPosXa() - offsetXb, Assets.boxStatusD.getPosYa() + offsetYb, Assets.correctWidth(Assets.boxStatusD.getWidth()), Assets.correctHeight(Assets.boxStatusD.getHeight()), null);
			statusText = new TextContainer(Game.m.avversario.getStatusAttivi().get(i).getSigla(), Assets.boxStatusD.getPosXa() + Assets.correctWidth(10) - offsetXb, Assets.boxStatusD.getPosYa() + Assets.correctHeight(10) + offsetYb, Assets.correctWidth(Assets.boxStatusD.getWidth()) - Assets.correctWidth(5),Assets.correctHeight(Assets.boxStatusD.getHeight())  - Assets.correctHeight(5), fontStatus, Color.WHITE);
			statusText.paint(g);
			offsetXb += Assets.correctWidth(85);
		}
		offsetXb = 0;
		offsetYb = 0;
		
	}
	
	private void barreLotta(Graphics g) {
		//parte sinistra
		if(checkPercentuale(Game.m.protagonista.getPvMax(), Game.m.protagonista.getPv(), 80, 100)) {
			
			g.drawImage(Assets.barraLottaSinistra.getImage(), Assets.barraLottaSinistra.getPosXa(), Assets.barraLottaSinistra.getPosYa(), Assets.correctWidth(Assets.barraLottaSinistra.getWidth()), Assets.correctHeight(Assets.barraLottaSinistra.getHeight()), null);

		} if(checkPercentuale(Game.m.protagonista.getPvMax(), Game.m.protagonista.getPv(), 60, 79)) {
			
			g.drawImage(Assets.barraLottaSinistra80.getImage(), Assets.barraLottaSinistra.getPosXa(), Assets.barraLottaSinistra.getPosYa(), Assets.correctWidth(Assets.barraLottaSinistra.getWidth()), Assets.correctHeight(Assets.barraLottaSinistra.getHeight()), null);
			
		} if(checkPercentuale(Game.m.protagonista.getPvMax(), Game.m.protagonista.getPv(), 40, 59)) {
			
			g.drawImage(Assets.barraLottaSinistra60.getImage(), Assets.barraLottaSinistra.getPosXa(), Assets.barraLottaSinistra.getPosYa(), Assets.correctWidth(Assets.barraLottaSinistra.getWidth()), Assets.correctHeight(Assets.barraLottaSinistra.getHeight()), null);
			
		} if(checkPercentuale(Game.m.protagonista.getPvMax(), Game.m.protagonista.getPv(), 20, 39)) {
			
			g.drawImage(Assets.barraLottaSinistra40.getImage(), Assets.barraLottaSinistra.getPosXa(), Assets.barraLottaSinistra.getPosYa(), Assets.correctWidth(Assets.barraLottaSinistra.getWidth()), Assets.correctHeight(Assets.barraLottaSinistra.getHeight()), null);
			
		} if(checkPercentuale(Game.m.protagonista.getPvMax(), Game.m.protagonista.getPv(), 1, 19)) {
			
			g.drawImage(Assets.barraLottaSinistra20.getImage(), Assets.barraLottaSinistra.getPosXa(), Assets.barraLottaSinistra.getPosYa(), Assets.correctWidth(Assets.barraLottaSinistra.getWidth()), Assets.correctHeight(Assets.barraLottaSinistra.getHeight()), null);
		
		} if(checkPercentuale(Game.m.protagonista.getPvMax(), Game.m.protagonista.getPv(), 0, 0)) {
			
			g.drawImage(Assets.barraLottaSinistra0.getImage(), Assets.barraLottaSinistra.getPosXa(), Assets.barraLottaSinistra.getPosYa(), Assets.correctWidth(Assets.barraLottaSinistra.getWidth()), Assets.correctHeight(Assets.barraLottaSinistra.getHeight()), null);
		
		}
		//parte destra
		if(checkPercentuale(Game.m.avversario.getPvMax(), Game.m.avversario.getPv(), 80, 100)) {
			
			g.drawImage(Assets.barraLottaDestra.getImage(), Assets.barraLottaDestra.getPosXa(), Assets.barraLottaDestra.getPosYa(), Assets.correctWidth(Assets.barraLottaDestra.getWidth()), Assets.correctHeight(Assets.barraLottaDestra.getHeight()), null);

		} if(checkPercentuale(Game.m.avversario.getPvMax(), Game.m.avversario.getPv(), 60, 79)) {
			
			g.drawImage(Assets.barraLottaDestra80.getImage(), Assets.barraLottaDestra.getPosXa(), Assets.barraLottaDestra.getPosYa(), Assets.correctWidth(Assets.barraLottaDestra.getWidth()), Assets.correctHeight(Assets.barraLottaDestra.getHeight()), null);
			
		} if(checkPercentuale(Game.m.avversario.getPvMax(), Game.m.avversario.getPv(), 40, 59)) {
			
			g.drawImage(Assets.barraLottaDestra60.getImage(), Assets.barraLottaDestra.getPosXa(), Assets.barraLottaDestra.getPosYa(), Assets.correctWidth(Assets.barraLottaDestra.getWidth()), Assets.correctHeight(Assets.barraLottaDestra.getHeight()), null);
			
		} if(checkPercentuale(Game.m.avversario.getPvMax(), Game.m.avversario.getPv(), 20, 39)) {
			
			g.drawImage(Assets.barraLottaDestra40.getImage(), Assets.barraLottaDestra.getPosXa(), Assets.barraLottaDestra.getPosYa(), Assets.correctWidth(Assets.barraLottaDestra.getWidth()), Assets.correctHeight(Assets.barraLottaDestra.getHeight()), null);
			
		} if(checkPercentuale(Game.m.avversario.getPvMax(), Game.m.avversario.getPv(), 1, 19)) {
			
			g.drawImage(Assets.barraLottaDestra20.getImage(), Assets.barraLottaDestra.getPosXa(), Assets.barraLottaDestra.getPosYa(), Assets.correctWidth(Assets.barraLottaDestra.getWidth()), Assets.correctHeight(Assets.barraLottaDestra.getHeight()), null);
		
		} if(checkPercentuale(Game.m.avversario.getPvMax(), Game.m.avversario.getPv(), 0, 0)) {
			
			g.drawImage(Assets.barraLottaDestra0.getImage(), Assets.barraLottaDestra.getPosXa(), Assets.barraLottaDestra.getPosYa(), Assets.correctWidth(Assets.barraLottaDestra.getWidth()), Assets.correctHeight(Assets.barraLottaDestra.getHeight()), null);
		
		}
		if(Game.m.protagonista.getPv() >= 10) pvSinistra = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getPv()), Assets.widthPercentage(10) + Assets.correctWidth(60), Assets.heightPercentage(10) + Assets.correctHeight(Assets.barraLottaSinistra.getHeight()/3), Assets.barraLottaSinistra.getPosYa(), correctFontSize(20),fontStats, Color.WHITE);
		else pvSinistra = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getPv()), Assets.widthPercentage(10) + Assets.correctWidth(75), Assets.heightPercentage(10) + Assets.correctHeight(Assets.barraLottaSinistra.getHeight()/3), Assets.barraLottaSinistra.getPosYa(), correctFontSize(20),fontStats, Color.WHITE);
		pvSinistra.paint(g);
		g.drawImage(Assets.barraManaSinistra.getImage(), Assets.barraManaSinistra.getPosXa(), Assets.barraManaSinistra.getPosYa(), Assets.correctWidth(Assets.barraManaSinistra.getWidth()), Assets.correctHeight(Assets.barraManaSinistra.getHeight()), null);
		manaSinistra = new TextContainer(RoutineSystem.rimuoviZero(Game.m.protagonista.getPm()), Assets.widthPercentage(10) + Assets.correctWidth(Assets.barraManaSinistra.getWidth()/18), Assets.heightPercentage(10) + Assets.correctHeight(Assets.barraLottaSinistra.getHeight()) + Assets.correctHeight(Assets.barraManaSinistra.getHeight()/3), Assets.barraManaSinistra.getPosYa(), correctFontSize(15),fontMana, Color.WHITE);
		manaSinistra.paint(g);
		//parte destra
		if(Game.m.avversario.getPv() >= 10) pvDestra = new TextContainer(RoutineSystem.rimuoviZero(Game.m.avversario.getPv()), Assets.barraLottaDestra.getPosXb() - Assets.correctWidth(100), Assets.heightPercentage(10) + Assets.correctHeight(Assets.barraLottaDestra.getHeight()/3), Assets.barraLottaDestra.getPosYa(), correctFontSize(20),fontStats, Color.WHITE);
		else pvDestra = new TextContainer(RoutineSystem.rimuoviZero(Game.m.avversario.getPv()), Assets.barraLottaDestra.getPosXb() - Assets.correctWidth(90), Assets.heightPercentage(10) + Assets.correctHeight(Assets.barraLottaDestra.getHeight()/3), Assets.barraLottaDestra.getPosYa(), correctFontSize(20),fontStats, Color.WHITE);
		pvDestra.paint(g);
	}
	private boolean checkPercentuale(double tot, double par, int per1, int per2) {
		int valorePer1 = (int) ((tot/100) * per1);
		int valorePer2 = (int) ((tot/100) * per2);
		if(par >= valorePer1 && par <= valorePer2) return true;
		return false;
	}
}
